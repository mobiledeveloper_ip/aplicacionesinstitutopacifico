<?php
class vencimientos_tributarios
{

    const NOMBRE_TABLA = "noticias";
  //  const REVISTA = "alias";
/*    const PRIMER_NOMBRE = "primerNombre";
    const PRIMER_APELLIDO = "primerApellido";
    const TELEFONO = "telefono";
    const CORREO = "correo";
    const ID_USUARIO = "idUsuario";*/

    const CODIGO_EXITO = 1;
    const ESTADO_EXITO = 1;
    const ESTADO_ERROR = 2;
    const ESTADO_ERROR_BD = 3;
    const ESTADO_ERROR_PARAMETROS = 4;
    const ESTADO_NO_ENCONTRADO = 5;
	
	public function postVencimientos(){
		$idUsuario = usuarios::autorizar();
		$body = file_get_contents('php://input');
		$data = json_decode($body);
		$vt_categoria = $data->vt_categoria;
		
		$vt_dia = $data->vt_dia;
		$vt_mes = (empty($data->vt_mes)) ? date('m') : $data->vt_mes;
		$vt_anio = (empty($data->vt_anio)) ? date('Y') : $data->vt_anio;
		$vt_detalle = $data->vt_detalle;
		return self::vencimientotributario($vt_categoria, $vt_dia, $vt_mes, $vt_anio, $vt_detalle);		
	}
	
	private function vencimientotributario($vt_categoria, $vt_dia, $vt_mes, $vt_anio, $vt_detalle){
		try{
			//if(empty($vt_categoria)) throw new ExcepcionApi(self::ESTADO_ERROR, "No tengo una categoría para verificar.");
			//if($vt_dia=='') throw new ExcepcionApi(self::ESTADO_ERROR, "No tengo un día para verificar.");
			if($vt_mes=='') throw new ExcepcionApi(self::ESTADO_ERROR, "No tengo un mes para verificar.");
			if($vt_anio=='') throw new ExcepcionApi(self::ESTADO_ERROR, "No tengo una año para verificar.");
			
			$comando = "SELECT idvencimiento, nombrecategoria, detalle, fechavencimiento
FROM siip_vencimientocategoria svc, siip_vencimientotributario svt 
WHERE svt.id_vencimientocategoria = svc.idvencimientocategoria
and svc.estado = 1 ";
								
			if($vt_dia!='') $comando.=" and DAY(fechavencimiento) = '".$vt_dia."' ";
			if($vt_mes!='') $comando.=" and MONTH(fechavencimiento) = '".$vt_mes."' ";
			if($vt_anio!='') $comando.=" and YEAR(fechavencimiento) = '".$vt_anio."' ";
			if($vt_categoria!='') $comando.=" and id_vencimientocategoria = '".$vt_categoria."'";
			if($vt_detalle!='') $comando.=" and detalle like '%".$vt_detalle."%'";
			
			$comando.=" order by fechavencimiento asc";
			
			if($vt_mes!='' && $vt_anio!='' && $vt_categoria!='' && $vt_detalle!='') $comando.=" LIMIT 1";
			
			$sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
			
			//echo $comando." => ".$vt_dia." => ".$vt_mes." => ".$vt_anio;
			
			if ($sentencia->execute()) {
				http_response_code(200);
				return
					[
						"estado" => self::ESTADO_EXITO,
						"datos" => $sentencia->fetchAll(PDO::FETCH_ASSOC)
					];
			} else
				throw new ExcepcionApi(self::ESTADO_ERROR, "Se ha producido un error la consulta.");
			
		} catch (PDOException $e) {
            throw new ExcepcionApi(self::ESTADO_ERROR_BD, $e->getMessage());
        }	
	}
	
	
	
	public function postVencimientosCategorias(){
		$idUsuario = usuarios::autorizar();
		return self::vencimientocategoria();		
	}
	
	private function vencimientocategoria(){
		try{
			$comando = "SELECT idvencimientocategoria, nombrecategoria FROM siip_vencimientocategoria where estado = 1";
			$sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
			
			if ($sentencia->execute()) {
				http_response_code(200);
				return
					[
						"estado" => self::ESTADO_EXITO,
						"datos" => $sentencia->fetchAll(PDO::FETCH_ASSOC)
					];
			} else
				throw new ExcepcionApi(self::ESTADO_ERROR, "Se ha producido un error la consulta.");
			
		} catch (PDOException $e) {
            throw new ExcepcionApi(self::ESTADO_ERROR_BD, $e->getMessage());
        }	
	}
	
}