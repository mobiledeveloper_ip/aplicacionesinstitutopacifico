<?php
class session
{
	private $bd, $vista;

	public function __construct(){
        $this->bd = ConexionBD::obtenerInstancia()->obtenerBD();
		$this->vista = new VistaJson();
    }

	public function sign(){
		//echo "sdgds";
		try {
			$body = file_get_contents('php://input');
			$data = json_decode($body);

			$user_data_object = $data->user_data_object;
								/*
								"about_membership":"Miembro del periodo 2017–2018",
								"about_user_name":"Usuario invitado",
								"user_address":"direccion del usuario",
								"user_email":"nuevousuraio@gmail.com",
								"user_id":"guest",
								"user_password":"123456",
								"user_phone_number":"987654321"
								*/

			$compressed = (isset($data->compressed)) ? $data->compressed : false;
			if($compressed) $this->vista = new VistaGzip();

			$request = $data->request;
			//,"request”:"sign_in" "sign_up"
			$revista = $data->revista;
			//"revista":"AE”

			if(empty($request)) throw new ExcepcionApi(ESTADO_ERROR, "No se encontró un request para verificar");

			if($request=='sign_up') $array = self::sign_up($user_data_object, $revista);

			if($request=='sign_in') $array = self::sign_in($user_data_object, $revista);

			if($request=='I_FORGOT_MY_DATA'){
				$array_sign_in = self::sign_in($user_data_object, $revista, false);
				$Idtareaenvioemail = self::send_message_recovery($array_sign_in, $revista);
				if(empty($Idtareaenvioemail)) throw new ExcepcionApi(ESTADO_ERROR, "No se logró realizar el envío");

				$array = [
				 'estado' => ESTADO_EXITO,
				 'bad_request_message' => ''
				];
			}

			return $array;

		}catch(ExcepcionApi $e){

			$array = [
					"estado" => $e->estado,
					'bad_request_message' => $e->message
					];

		}

		$this->vista->imprimir($array);

	}

	private function html_mensaje($revista, $tipo='confirmation'){
		if($revista=='ae'){
			$htmllogo_header = '<!-- EMPRESARIAL -->
					<a href="http://aempresarial.com" target="_blank"><img src="http://institutopacifico.com.pe/images/logo_empresarial.png" height="40px" alt="" border="0"></a>';
			$link = 'http://aempresarial.com';
		}
		elseif($revista=='ag'){
			$htmllogo_header = '<!-- GUBERNAMENTAL -->
					<a href="http://agubernamental.org/" target="_blank"><img src="http://institutopacifico.com.pe/images/logo_gubernamental.png" height="45px" alt="" border="0"></a>';
			$link = 'http://agubernamental.org';
		}
		elseif($revista=='ac'){
			$htmllogo_header = '<!-- CIVIL -->
					<a href="http://actualidadcivil.com.pe/" target="_blank"><img src="http://institutopacifico.com.pe/images/logo_acivil.png" height="55px" alt="" border="0"></a>';
			$link = 'http://actualidadcivil.com.pe';
		}
		elseif($revista=='ap'){
			$htmllogo_header = '<!-- PENAL-->
					<a href="http://actualidadpenal.com.pe/" target="_blank"><img src="http://institutopacifico.com.pe/images/logo_apenal.png" height="55px" alt="" border="0"></a>';
			$link = 'http://actualidadpenal.com.pe';
		}
		else{
			$htmllogo_header = '<a href="http://aempresarial.com" target="_blank"><img src="http://institutopacifico.com.pe/images/logo_empresarial.png" height="40px" alt="" border="0"></a>';
			$link = 'http://aempresarial.com';
		}

		if($tipo=='confirmation')
		{
			$html = '<!DOCTYPE html>
					<html lang="en">
					<head>
						<meta charset="UTF-8">
						<title>Instituto Pacífico</title>
					</head>
					<body style="background-color: #dddddd; margin: 0; padding: 0">
						<div style="padding-top: 20px;">
							<div style="width: 700px; margin: 0 auto 0 auto; background-color: #ffffff; border: solid 3px #cccccc; ">
								<table width="700px" align="center" bgcolor="#ffffff" cellpadding="15" border="0" cellspacing="0" >
									<tr bgcolor="#f2f2f2">
										<td>'.$htmllogo_header.'</td>
										<td align="right"><a href="http://institutopacifico.com.pe" target="_blank"><img src="http://institutopacifico.com.pe/images/logo_instituto_pacifico.png" height="60px" alt="" border="0"></a></td>
									</tr>

									<tr bgcolor="#ffffff">
										<td colspan="2">
											<div style="font-family: Arial; font-size: 15px; line-height:18pt; color: #333 ">
												<p>Hola, <span>#nombre#</span></p>
												<h2 style="font-size: 20px; margin-top: 0px;">
													Gracias por configurar tu correo electrónico.
												</h2>

												<p>El correo electrónico y la contraseña que acabas de configurar te servirán para ingresar a nuestra plataforma web y  móvil (Android e  iOS), te pedimos que conserves el usuario y la contraseña para ingresar a nuestros servicios.</p>

												<p>Inicia sesión y disfruta de todos los beneficios exclusivos que tenemos para ti.</p>

												<p><a href="'.$link.'" style="background-color:#408fd4; border: solid 1px #3176b2; padding: 10px 30px; text-decoration: none; color: #ffffff ">Ingresar</a></p>

												<br>

												<p>Atte. <br><br> Dpto. de Innovación y Sistemas<br>Instituto Pacífico SAC</p>

												<p></p>
											</div>
										</td>
									</tr>

									<tr bgcolor="#19507e">
										<td colspan="2">
											<div style="background-color: #19507e; color: #ffffff">
												<p style="font-family: Arial; font-size: 14px; margin: 3px 0">&copy; 2017 - Instituto Pacífico S.A.C.</p>
											</div>
										</td>
									</tr>

								</table>
							</div>
						</div>
					</body>
					</html>';
		}
		elseif($tipo=='restore')
		{
			$html = '<!DOCTYPE html>
					<html lang="en">
					<head>
						<meta charset="UTF-8">
						<title>Instituto Pacífico</title>
					</head>
					<body style="background-color: #dddddd; margin: 0; padding: 0">
						<div style="padding-top: 20px;">
							<div style="width: 700px; margin: 0 auto 0 auto; background-color: #ffffff; border: solid 3px #cccccc; ">
								<table width="700px" align="center" bgcolor="#ffffff" cellpadding="15" border="0" cellspacing="0" >
									<tr bgcolor="#f2f2f2">
										<td>'.$htmllogo_header.'</td>

										<td align="right"><a href="http://institutopacifico.com.pe" target="_blank"><img src="http://institutopacifico.com.pe/images/logo_instituto_pacifico.png" height="60px" alt="" border="0"></a></td>
									</tr>

									<tr bgcolor="#ffffff">
										<td colspan="2">
											<div style="font-family: Arial; font-size: 15px; line-height:18pt; color: #333 ">
												<p>Hola <span>#nombre#</span></p>
												<h2 style="font-size: 20px; margin-top: 0px;">
													Vamos a restablecer tu contraseña
												</h2>

												<p>Hemos recibido una solicitud para restablecer la contraseña de tu cuenta Actualidad Empresarial.</p>

												<!--p>Para restablecer tu contraseña deberás ingresar este código, de modo que podamos verificar que eres tú la persona que ha realizado la solicitud.</p>

												<p>El código es <b>VwfJ8heV4wZBvv1</b></p-->

												<p>Haz clic en el botón que está abajo para continuar con el proceso de restablecer la contraseña.</p>
												<p><a href="#linkrestore#" style="background-color:#408fd4; border: solid 1px #3176b2; padding: 10px 30px; text-decoration: none; color: #ffffff ">Restablecer</a></p>

												<p>Gracias</p>

												<p>Atte. <br><br> Dpto. de Innovación y Sistemas<br>Instituto Pacífico SAC</p>

												<p></p>
											</div>
										</td>
									</tr>

									<tr bgcolor="#19507e">
										<td colspan="2">
											<div style="background-color: #19507e; color: #ffffff">
												<p style="font-family: Arial; font-size: 14px; margin: 3px 0">&copy; 2017 - Instituto Pacífico S.A.C.</p>
											</div>
										</td>
									</tr>

								</table>
							</div>
						</div>
					</body>
					</html>';
		}
		elseif($tipo=='sendValidateInformation')
		{
		$html = '<!DOCTYPE html>
					<html lang="en">
					<head>
						<meta charset="UTF-8">
						<title>Instituto Pacífico</title>
					</head>
					<body style="background-color: #dddddd; margin: 0; padding: 0">
						<div style="padding-top: 20px;">
							<div style="width: 700px; margin: 0 auto 0 auto; background-color: #ffffff; border: solid 3px #cccccc; ">
								<table width="700px" align="center" bgcolor="#ffffff" cellpadding="15" border="0" cellspacing="0" >
									<tr bgcolor="#f2f2f2">
										<td>'.$htmllogo_header.'</td>
										<td align="right"><a href="http://institutopacifico.com.pe" target="_blank"><img src="http://institutopacifico.com.pe/images/logo_instituto_pacifico.png" height="60px" alt="" border="0"></a></td>
									</tr>

									<tr bgcolor="#ffffff">
										<td colspan="2">
											<div style="font-family: Arial; font-size: 15px; line-height:18pt; color: #333 ">
												<p>Hola <span>#nombre#</span></p>
												<h2 style="font-size: 20px; margin-top: 0px;">
												El suscriptor #contacto# con DNI/RUC #dniruc# y teléfono #telefono# quiere validar su información.</h2>

												<p>Gracias</p>

												<p>Atte. <br><br> Dpto. de Innovación y Sistemas<br>Instituto Pacífico SAC</p>

											</div>
										</td>
									</tr>

									<tr bgcolor="#19507e">
										<td colspan="2">
											<div style="background-color: #19507e; color: #ffffff">
												<p style="font-family: Arial; font-size: 14px; margin: 3px 0">&copy; 2017 - Instituto Pacífico S.A.C.</p>
											</div>
										</td>
									</tr>

								</table>
							</div>
						</div>
					</body>
					</html>';
		}
		elseif($tipo=='confirmation-restore')
		{
			$html = '<!DOCTYPE html>
					<html lang="en">
					<head>
						<meta charset="UTF-8">
						<title>Instituto Pacífico</title>
					</head>
					<body style="background-color: #dddddd; margin: 0; padding: 0">
						<div style="padding-top: 20px;">
							<div style="width: 700px; margin: 0 auto 0 auto; background-color: #ffffff; border: solid 3px #cccccc; ">
								<table width="700px" align="center" bgcolor="#ffffff" cellpadding="15" border="0" cellspacing="0" >
									<tr bgcolor="#f2f2f2">
										<td>'.$htmllogo_header.'</td>
										<td align="right"><a href="http://institutopacifico.com.pe" target="_blank"><img src="http://institutopacifico.com.pe/images/logo_instituto_pacifico.png" height="60px" alt="" border="0"></a></td>
									</tr>

									<tr bgcolor="#ffffff">
										<td colspan="2">
											<div style="font-family: Arial; font-size: 15px; line-height:18pt; color: #333 ">
												<p>Hola <span>#nombre#</span></p>
												<h2 style="font-size: 20px; margin-top: 0px;">Tu contraseña se ha restablecido</h2>

												<p>Has cambiado la contraseña de tu suscripción satisfactoriamente.</p><!--#NombreDeRevista# -->

												<p>Gracias</p>

												<p>Atte. <br><br> Dpto. de Innovación y Sistemas<br>Instituto Pacífico SAC</p>

											</div>
										</td>
									</tr>

									<tr bgcolor="#19507e">
										<td colspan="2">
											<div style="background-color: #19507e; color: #ffffff">
												<p style="font-family: Arial; font-size: 14px; margin: 3px 0">&copy; 2017 - Instituto Pacífico S.A.C.</p>
											</div>
										</td>
									</tr>

								</table>
							</div>
						</div>
					</body>
					</html>';
		}
		return $html;
	}

	private function set_tokenrestablecer($token, $IdContactoLogin){
		$sql = "UPDATE siip_contacto_login SET tokenrestablecer = ?, tokenfechareg = now() WHERE idcontacto_login = ?";
		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(1, $token, PDO::PARAM_STR);
		$sentencia->bindParam(2, $IdContactoLogin, PDO::PARAM_INT);
		$sentencia->execute();
		$numaffected = $sentencia->rowCount();
		return $numaffected;
	}

	private function set_tokenVrestablecer($token, $idloginvisitante){
		$sql = "UPDATE siip_login_visitante SET tokenrestablecer = ?, tokenfechareg = now() WHERE idloginvisitante = ?";
		//echo $sql." => ".$token." => ".$idloginvisitante;
		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(1, $token, PDO::PARAM_STR);
		$sentencia->bindParam(2, $idloginvisitante, PDO::PARAM_INT);
		$sentencia->execute();
		$numaffected = $sentencia->rowCount();
		return $numaffected;
	}

	private function send_message_recovery($array, $revista){

		$estado = $array["estado"];
		$bad_request_message = $array["bad_request_message"];
		if(empty($estado)) throw new ExcepcionApi($estado, $bad_request_message);

		$message = self::html_mensaje($revista, 'restore');
		$user = $array["user_data_object"];
		$var["from_address"] = 'webmaster@institutopacifico.com.pe';
		$var["from_name"] = utf8_encode('Instituto Pacífico S.A.C.');
		$var["email"] = $user->user_email;
		$var["ccemail"] = NULL;
		$var["name"] = utf8_encode($user->about_user_name);
		$message = str_replace("#nombre#", $user->about_user_name, $message);

		//CREANDO EL TOKEN PARA VALIDAR EL LINK DE RESTAURAR
		$token= __SECRET_KEY__;
		$token.= $user->user_id;
		$token.= $user->user_email;
		$token.= $user->user_password;
		$token.= $user->user_password;
		$token.= fecha_hora(2);
		$token = md5($token);

		//$user->user_type;
		if($user->user_type=='V'){ $regToken = self::set_tokenVrestablecer($token, $user->user_id); }
		else{ $regToken = self::set_tokenrestablecer($token, $user->user_id); }

		if(empty($regToken))  throw new ExcepcionApi(ESTADO_ERROR, "No se pudo realizar la tarea que solicitada. Por favor intentalo más tarde.");

		$linkrestore = "http://institutopacifico.pe/login/restore-pass?rev=".$revista."&url=/&token=".$token;//."&type=".$user->user_type;

		$message = str_replace("#linkrestore#", $linkrestore, $message);
		$var["subject"] = 'Cambio de tu contraseña.';
		$var["altbody"] = 'Para ver el mensaje , por favor, utilice un visor de HTML de correo electronico compatible!';
		$var["message"] = utf8_encode($message);
		$var["priority"] = 1;
		//if(empty($idenvio)) throw new ExcepcionApi(ESTADO_ERROR, "No pude realizar la tarea que necesitas. Inténtalo más tarde.");

		$sql = "INSERT INTO `siip_tareaenvioemail`(`priority`, `From_email`, `From_name`, `Address_email`, `CCAddress_email`, `Address_name`, `Subject`, `AltBody`, `MsgHTML`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$sentencia = $this->bd->prepare($sql);
		//echo $sql;
		$sentencia->bindParam(1, $var["priority"], PDO::PARAM_INT);
		$sentencia->bindParam(2, $var["from_address"], PDO::PARAM_STR);
		$sentencia->bindParam(3, $var["from_name"], PDO::PARAM_STR);
		$sentencia->bindParam(4, $var["email"], PDO::PARAM_STR);
		$sentencia->bindParam(5, $var["ccemail"], PDO::PARAM_STR);
		$sentencia->bindParam(6, $var["name"], PDO::PARAM_STR);
		$sentencia->bindParam(7, $var["subject"], PDO::PARAM_STR);
		$sentencia->bindParam(8, $var["altbody"], PDO::PARAM_STR);
		$sentencia->bindParam(9, $var["message"], PDO::PARAM_STR);
		$sentencia->execute();
		$idtareaenvioemail = $this->bd->lastInsertId('idtareaenvioemail');
		return $idtareaenvioemail;
	}

	public function get_sign_in($user_data_object, $revista='AE', $validPass=true, $md5=true){
		return self::sign_in($user_data_object, $revista, $validPass, $md5);
	}

	private function sign_in($user_data_object, $revista='AE', $validPass=true, $md5=true){
		$tipo_login = NULL;
		$bad_request_message = NULL;
		$status_signin = ESTADO_EXITO;
		$rows = NULL;

		try {
			$email = $user_data_object->user_email;
			$password = ($md5) ? md5($user_data_object->user_password) : $user_data_object->user_password;
			//echo $email."\n".$password;
			//PRIMERO HACEMOS LA BUSQUEDA EN SUSCRIPTORES
			$sql = "SELECT
					CONCAT('Miembro del periodo ', YEAR(desde), ' - ', YEAR(hasta)) as about_membership,
					IF(scli.tipo_persona='N',
					   concat(scli.ape_paterno, ' ', scli.ape_materno, ' ', scli.nombres_razonsocial),
					   sc.contacto) as 'about_user_name',
					scli.direccion_fiscal as 'about_address',
					sc.telefono as 'about_phone_number',
					scl.email as 'user_email',
					idcontacto_login as 'user_id',
					scl.password as 'user_password',
					'S' as 'user_type', sr.alias as revista
					from
					instiuo7_siip1.siip_revista sr,
					instiuo7_siip1.siip_suscripcion ss,
					instiuo7_siip1.siip_cliente scli,
                    instiuo7_siip1.siip_contacto sc,
					instiuo7_siip1.siip_contacto_login scl
					where
					ss.id_revista = sr.idrevista and
					ss.id_cliente = scli.idcliente and
                    scli.idcliente = sc.id_cliente and
                    sc.idcontacto = scl.id_contacto and
					scl.email = :email and
					scl.estado = 1";// and
					//echo $sql." => ".$email." => ".$password;

			$sentencia = $this->bd->prepare($sql);
			$sentencia->bindParam(":email", $email, PDO::PARAM_STR);

			if(!$sentencia->execute()) throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");
			$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);

			if(!empty($rows)){
				$tipo_login = 'S';
				$rev_encontrada = false;
				foreach($rows as $row){
					if(strtoupper($row["revista"]) == strtoupper($revista)){
						$rev_encontrada = true;
						$rows = $row;
					}

					if($validPass && $rev_encontrada){
						if($row["user_password"] != $password){
							throw new ExcepcionApi(ESTADO_ERROR, "La contraseña no es correcta");
						}
					}
				}


				if(!$rev_encontrada){
					throw new ExcepcionApi(ESTADO_ERROR, "El usuario no tiene acceso para esta revista");
				}
			}

			//echo "paso";
			//Y SI NO SE ENCUENTRA EL REGISTRO HACEMOS LA BUSQUEDA EN VISITANTES
			if(empty($rows)){
				$sql = "SELECT
					'Invitado' as 'about_membership', nombre as 'about_user_name', direccion as 'about_address', telefono as 'about_phone_number',
					email as 'user_email', idloginvisitante as 'user_id', token as 'user_password', 'V' as 'user_type'
					from instiuo7_siip1.siip_login_visitante
					where
					email = :email
					and estado=1";

				//echo "\n\n".$sql." => ".$email." => ".$password;

				$sentencia = $this->bd->prepare($sql);
				$sentencia->bindParam(":email", $email, PDO::PARAM_STR);

				if(!$sentencia->execute()) throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");
				$rows = $sentencia->fetch(PDO::FETCH_ASSOC);
				$tipo_login = 'V';

				if($validPass && !empty($rows)){
					if($rows["user_password"] != $password){
						throw new ExcepcionApi(ESTADO_ERROR, "La contraseña no es correcta");
					}
				}
			}

			if(empty($rows)) throw new ExcepcionApi(ESTADO_ERROR, "No tenemos un registro");

			$user_data_object = (object) [
								"about_membership" => $rows["about_membership"],
								"about_user_name" => $rows["about_user_name"],
								"about_address" => $rows["about_address"]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ,
								"about_phone_number" => $rows["about_phone_number"],
								"user_email" => $rows["user_email"],
								"user_id" => $rows["user_id"],
								"user_password" => $rows["user_password"],
								"user_type" => $tipo_login
								];

			//http_response_code(200);

		} catch (ExcepcionApi $e) {
			$status_signin = $e->getStatus();
			$bad_request_message = $e->getMessage();

		}
		//print_r($user_data_object);
		$array = [
				 'user_data_object' => $user_data_object,
				 'estado' => $status_signin,
				 'bad_request_message' => $bad_request_message
				 ];

		//print_r($array);
		return $array;

	}

	private function sign_up($user_data_object, $revista='AE'){

		$bad_request_message = NULL;
		$status_signup = ESTADO_EXITO;
		$rows = NULL;

		try {
			$array_valid = $this->sign_in($user_data_object, $revista, false);
			if(empty($array_valid)) throw new ExcepcionApi(ESTADO_ERROR, "Falló la verificación");
			$user_id = $array_valid["user_data_object"]->user_id;
			if(!empty($user_id)) throw new ExcepcionApi(ESTADO_ERROR, "El correo ya existe");

			$status = 1;
			$name = $user_data_object->about_user_name;
			$address = $user_data_object->about_address;
			$phone = $user_data_object->about_phone_number;
			$type = $user_data_object->user_type;
			$email = $user_data_object->user_email;
			$password = $user_data_object->user_password;
			if(empty($password)) throw new ExcepcionApi(ESTADO_ERROR, "La contraseña no puede ser vacía");

			$password_md5 = md5($password);

			//HACEMOS EL REGISTRO EN VISITANTES
			$sql = "INSERT INTO siip_login_visitante
					(`idloginvisitante`, `estado`, `email`, `nombre`, `telefono`, `direccion`, `fch_registro`, `token`)
					VALUES
					(NULL, ?, ?, ?, ?, ?, NULL, ?)";

			$sentencia = $this->bd->prepare($sql);
			$sentencia->bindParam(1, $status, PDO::PARAM_INT);
			$sentencia->bindParam(2, $email, PDO::PARAM_STR);
			$sentencia->bindParam(3, $name, PDO::PARAM_STR);
			$sentencia->bindParam(4, $phone, PDO::PARAM_STR);
			$sentencia->bindParam(5, $address, PDO::PARAM_STR);
			$sentencia->bindParam(6, $password_md5, PDO::PARAM_STR);
			if(!$sentencia->execute()) throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");

			$IdLoginVisitante = $this->bd->lastInsertId('idloginvisitante');

			if(empty($IdLoginVisitante)) throw new ExcepcionApi(ESTADO_ERROR, "No se logró registrar");

			http_response_code(200);

			$user_data_object = [
								"about_membership" => "Invitado",
								"about_user_name" => $name,
								"about_address" => $address,
								"about_phone_number" => $phone,
								"user_email" => $email,
								"user_id" => $IdLoginVisitante,
								"user_password" => $password_md5
								];
$bad_request_message = "OK"
		} catch (ExcepcionApi $e) {
			$status_signup = $e->getStatus();
			$bad_request_message = $e->getMessage();

		}

		$array = [
				'user_data_object' => $user_data_object,
				'estado' => $status_signup,
					'bad_request_message' => $bad_request_message
				];

		return $array;
	}

}
