<?php
class noticiasae1
{
	private $bd, $vista;
	public function __construct(){
        $this->bd = ConexionBD::obtenerInstancia()->obtenerBD();
		$this->vista = new VistaJson();
    }
	/* public function boletindiario($tipo){
		//echo "sdgds";
		$body = file_get_contents('php://input');
		$data = json_decode($body);

		$referenceobject = $data->reference_object;
		$userid = $data->user_id;
		$revista = $data->revista;
		$step = $data->step;
		$request = $data->request;

		$array = ($tipo=='legal') ?
				 self::noticias_legal_all($request, $step, $referenceobject, $userid, $revista) :
				 self::noticias_all($request, $step, $referenceobject, $userid, $revista);

		return	[
					"estado" => ESTADO_EXITO,
					"datos" => $array
				];
	}*/

	public function get_data($recurso){
		try{
			//echo "sdgds";
			$body = file_get_contents('php://input');
			$data = json_decode($body);

			//estableciendo variables default
			$userid = NULL;
			$tipologin = NULL;

			//valores de variables enviada
			$referenceobject = $data->reference_object;
			$user_data_object = $data->user_data_object;
			$revista = $data->revista;
			$step = $data->step;
			$request = $data->request;

			$compressed = (isset($data->compressed)) ? $data->compressed : false;
			if($compressed) $this->vista = new VistaGzip();
	//		$string_additional_parameter =  ? : ;

			//VERIFICANDO DATOS DEL USUARIO ENVIADO
			{
				$session = new session;
				$array_session = $session->get_sign_in($user_data_object, $revista, true, false);
				$estado_session = $array_session["estado"];

				if(isset($data->string_additional_parameter) && $data->string_additional_parameter == 'SHOW_MY_STARRED_ARTICLES'){
					if($estado_session == 0 || empty($estado_session)) throw new ExcepcionApi(ESTADO_ERROR, "Usuario no encontrado.");
				}

				//if($estado_session == 0 || empty($estado_session)) throw new ExcepcionApi(ESTADO_ERROR, "No encuentro al usuario");
				if($estado_session != 0 && !empty($estado_session)){
					$array_session_details = $array_session["user_data_object"];
					$about_membership = $array_session_details->about_membership;
					//SET VALORES DEL USUARIO
					$userid = $array_session_details->user_id;
					$tipologin = (strtoupper($about_membership) == strtoupper('Invitado')) ? 'V' : 'S';
				}
			}

			//BUSCANDO EL PARAMETRO  -  SHOW_MY_STARRED_ARTICLES
			if(isset($data->string_additional_parameter)){
				$parameter = $data->string_additional_parameter;

				if($parameter == 'SHOW_MY_STARRED_ARTICLES'){
					$tipoitem = ($recurso=='normas_legales') ? 'R' : 'N';
					$array_data = self::get_favoritos($tipoitem, $userid, $tipologin, $request, $step, $referenceobject, $revista);
				}else{
					$array_data = ($recurso=='normas_legales') ?
								 self::normas_legales($request, $step, $referenceobject, $userid, $tipologin, $revista, $parameter) :
								 self::noticias_all($request, $step, $referenceobject, $userid, $tipologin, $revista, $parameter);
				}
			}
			else{
				$array_data = ($recurso=='normas_legales') ?
							 self::normas_legales($request, $step, $referenceobject, $userid, $tipologin, $revista) :
							 self::noticias_all($request, $step, $referenceobject, $userid, $tipologin, $revista);
			}
			//print_r($array_data);

			$array = [
						"estado" => ESTADO_EXITO,
						"datos" => $array_data
					];

		}
		catch (ExcepcionApi $e) {
			$status_favorito = $e->getStatus();
			$bad_request_message = $e->getMessage();

			$array = [
				 'estado' => $status_favorito,
				 'bad_request_message' => $bad_request_message
				 ];
		}

		//return $array;
		$this->vista->imprimir($array);
	}

	private function noticias_all($request, $step, $referenceobject, $userid, $tipologin, $revista='AE', $busqueda = NULL){


        try {
			{
				$comando ="SELECT
							a.id_noti as boletin_articles_id,
							if(a.relacion=1, 'Noticias',
								if(a.relacion=2, 'Comentarios Legales',
									if(a.relacion=3, 'Obligaciones',
										if(a.relacion=4, 'Tips Empresariales',
											if(a.relacion=5, 'Artículos Destacados',
												if(a.relacion=6, 'Jurisprudencia de la Semana', 'Otros')))))) as boletin_articles_category,
							IF((select iditemfavorito from instiuo7_siip1.siip_favoritos_item sfv where sfv.idlogin='".$userid."' and sfv.tipologin='".$tipologin."' and sfv.iditem = a.id_noti and sfv.tipoitem = 'N' and sfv.revista = '".$revista."') IS NULL, 'false', 'true') as boletin_articles_is_starred,

							IFNULL((select date_added from instiuo7_siip1.siip_favoritos_item sfv where sfv.idlogin='".$userid."' and sfv.tipologin='".$tipologin."' and sfv.iditem = a.id_noti and sfv.tipoitem = 'N' and sfv.revista = '".$revista."'), '') as boletin_articles_date_when_article_is_starred,

							IF(a.imagen='', NULL, CONCAT('http://aempresarial.com/web/boletin/',a.imagen)) as boletin_articles_image_url,

							CONCAT(fecha_mod, ' ', hora_mod) as boletin_articles_date_added,

							REPLACE(a.texto, '../web/downmailrev.php', 'http://aempresarial.com/web/downmailrev.php') as boletin_articles_description,

							a.titulo as boletin_articles_title,

							concat('http://aempresarial.com/web/informativo.php?id=', a.id_noti) as boletin_articles_url, ";

				$comando.=' if(LOCATE( "downmailrev.php?id=", a.texto ),
							CONCAT( "http://aempresarial.com/web/",
							REPLACE( SUBSTRING( a.texto,
							LOCATE( "downmailrev.php?id=", a.texto ) ,
							LOCATE(\'"><\', SUBSTRING( a.texto, LOCATE(\'downmailrev.php?id=\', a.texto ) ))) ,  \'\',  "" ) )
							,CONCAT("https://institutopacifico.com.pe/apiv1/boletin_diario/pdf/", a.id_noti)) AS boletin_articles_pdf';

				$comando.=" FROM instiuo7_aempresa.noticias a
							LEFT JOIN instiuo7_aempresa.sumillas_nl b ON a.id_norma = b.id
							WHERE a.tipo=0";

			}

			if($request == 'get_data'){
				///print_r($referenceobject->boletin_articles_id);
				$articles_array = $referenceobject->boletin_articles_id;
				$prefix = $articles_List = '';
				if(!empty($articles_array)){
					foreach ($articles_array as $articulo_id){
						$articles_List .= $prefix . "'" . $articulo_id . "'";
						$prefix = ', ';
					}
					$comando.= " and a.id_noti IN (".$articles_List.")";
				}
			}
			else{
				$fecha = $referenceobject->boletin_articles_date_added;

				if(!empty($fecha)){
					if($request == 'more_data') $comando.= " and CONCAT(fecha_mod, ' ', hora_mod)<'".$fecha."' ";
					if($request == 'refresh') $comando.= " and CONCAT(fecha_mod, ' ', hora_mod)>'".$fecha."' ";
				}
			}

			if(!empty($busqueda)){
				$cadena = NULL;
				$fechaini_g = NULL;
				$fechafin_g = NULL;
				$daterange = false;
				$palabras = explode(" ", $busqueda);

				if(!empty($palabras)){
					foreach($palabras as $palabra){
						$pos = strpos($palabra, "daterange:");
						if ($pos !== false) {
							$daterange = true;
							$rangofechas = substr($palabra, ($pos + 10));
							//echo "\n : rangofechas: ".$rangofechas;
							if(strpos($rangofechas, "-") !== false){
								list($fechaini_g, $fechafin_g) = explode("-", $rangofechas);
							}
							else $fechaini_g = $rangofechas;
						}elseif(strpos($palabra, ":") === false){
							$cadena.= $palabra." ";
						}
					}
				}

				if($daterange){
					if(!is_null($fechaini_g)){
						$fecha_ini_ = inc_invertir_fecha_gregoriana(JDToGregorian($fechaini_g), "-");
						$comando.=" and CONCAT(snl.fecha, ' ', snl.hora)>=".$fecha_ini_;
					}
					if(!is_null($fechafin_g)){
						$fecha_fin_ = inc_invertir_fecha_gregoriana(JDToGregorian($fechaini_g), "-");
						$comando.=" and CONCAT(snl.fecha, ' ', snl.hora)<=".$fecha_fin_;
					}

				}

				$cadena = trim($cadena);
				$comando.=" and (a.titulo like '%".$cadena."%' or a.texto like '%".$cadena."%') ";

			}

			$limit = empty($step) ? '5' : $step;

			if($revista == 'AE') $comando.=" AND a.FLG_AEMPRESARIAL =  '1' ";
			if($revista == 'AG') $comando.=" AND a.FLG_AGUBERNAMENTAL =  '1' ";

			if(!empty($busqueda)){
				$comando.=" and (a.titulo like '%".$busqueda."%' or a.texto like '%".$busqueda."%' or a.obs like '%".$busqueda."%') ";
			}

			$comando.= " ORDER BY CONCAT(fecha_mod, ' ', hora_mod) desc ";
			$comando.= " LIMIT ".$limit;
			//echo $comando;

			$sentencia = $this->bd->prepare($comando);

			if ($sentencia->execute()) {
				http_response_code(200);
				$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);

				$array = [];
				if(!empty($rows)){
					foreach($rows as $reg => $item){
						$idpdf = md5(__SECRET_KEY__.$item["boletin_articles_id"]);
						$boletin_articles_description_md_url =	'https://institutopacifico.com.pe/apiv1/boletin_diario/md/'.$idpdf;
						$boletin_articles_pdf =	'https://institutopacifico.com.pe/apiv1/boletin_diario/pdf/'.$idpdf;
						$description = substr($item["boletin_articles_description"],0,250);

						$array_data = array(
										//"boletin_articles_id" => $item["boletin_articles_id"],
										"boletin_articles_is_starred" => $item["boletin_articles_is_starred"],
										"boletin_articles_date_when_article_is_starred" => $item["boletin_articles_date_when_article_is_starred"],
										"boletin_articles_category" => $item["boletin_articles_category"],
										"boletin_articles_image_url" => $item["boletin_articles_image_url"],
										"boletin_articles_date_added" => $item["boletin_articles_date_added"],
										"boletin_articles_description" => html_entity_decode(strip_tags($description)),
										"boletin_articles_description_md_url" => $boletin_articles_description_md_url,
										"boletin_articles_title" => $item["boletin_articles_title"],
										"boletin_articles_url" => $item["boletin_articles_url"],
										"boletin_articles_pdf" => $boletin_articles_pdf
										);

						array_push($array, $array_data);

					}
				}

				return $array;

            } else
                throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");

        }
		catch (PDOException $e) {
            throw new ExcepcionApi(ESTADO_ERROR, $e->getMessage());
        }


	}

	private function normas_legales($request, $step, $referenceobject, $userid, $tipologin, $revista='AE', $busqueda = NULL){
        try {
			{
				$comando ="	SELECT
							snl.id AS 'boletin_articles_id',
							op.nombre AS 'boletin_articles_organismo',
							enl.nombre AS 'boletin_articles_entidad',
							IF((select iditemfavorito from instiuo7_siip1.siip_favoritos_item sfv where sfv.idlogin='".$userid."' and sfv.tipologin='".$tipologin."' and sfv.iditem = snl.id and sfv.tipoitem = 'R' and sfv.revista = '".$revista."') IS NULL, 'false', 'true') as 'boletin_articles_is_starred',

							IFNULL((select date_added from instiuo7_siip1.siip_favoritos_item sfv where sfv.idlogin='".$userid."' and sfv.tipologin='".$tipologin."' and sfv.iditem = snl.id and sfv.tipoitem = 'R' and sfv.revista = '".$revista."'), '') as boletin_articles_date_when_article_is_starred,

							'' as 'boletin_articles_image_url',
							CONCAT(snl.fecha, ' ', snl.hora) as 'boletin_articles_date_added',
							snl.texto as 'boletin_articles_description',
							snl.titulo as 'boletin_articles_title',
							CONCAT(IF(('".$revista."'='AG'), 'http://www.agubernamental.org/web/solicitud_nl.php?id=', 'http://www.aempresarial.com/web/solicitud_nl.php?id='), snl.id) as 'boletin_articles_url',
							CONCAT('https://institutopacifico.com.pe/apiv1/normas_legales/pdf/', snl.id) as 'boletin_articles_pdf'
							FROM instiuo7_aempresa.sumillas_nl snl, instiuo7_aempresa.org_principal op, instiuo7_aempresa.entidad_nl enl
							where snl.cod_ent_prim = op.id and snl.cod_ent_sec = enl.id";
			}

			if($request == 'get_data'){
				///print_r($referenceobject->boletin_articles_id);
				$articles_array = $referenceobject->boletin_articles_id;
				$prefix = $articles_List = '';
				if(!empty($articles_array)){
					foreach ($articles_array as $articulo_id){
						$articles_List .= $prefix . "'" . $articulo_id . "'";
						$prefix = ', ';
					}
					$comando.= " and snl.id IN (".$articles_List.")";
				}
			}
				//$comando.= " and snl.id = '".$idnoticia."' ";

			if($revista == 'AE') $comando.=" AND snl.FLG_AEMPRESARIAL =  '1' ";
			if($revista == 'AG') $comando.=" AND snl.FLG_AGUBERNAMENTAL =  '1' ";

			$limit = empty($step) ? '5' : $step;

			$fecha = $referenceobject->boletin_articles_date_added;

			if(!empty($fecha)){
				if($request == 'more_data') $comando.= " and CONCAT(snl.fecha, ' ', snl.hora)<'".$fecha."' ";
				if($request == 'refresh') $comando.= " and CONCAT(snl.fecha, ' ', snl.hora)>'".$fecha."' ";
			}

			if(!empty($busqueda)){
				$cadena = NULL;
				$fechaini_g = NULL;
				$fechafin_g = NULL;
				$daterange = false;
				$palabras = explode(" ", $busqueda);

				if(!empty($palabras)){
					foreach($palabras as $palabra){
						$pos = strpos($palabra, "daterange:");
						if ($pos !== false) {
							$daterange = true;
							$rangofechas = substr($palabra, ($pos + 10));
							//echo "\n : rangofechas: ".$rangofechas;
							if(strpos($rangofechas, "-") !== false){
								list($fechaini_g, $fechafin_g) = explode("-", $rangofechas);
							}
							else $fechaini_g = $rangofechas;
						}elseif(strpos($palabra, ":") === false){
							$cadena.= $palabra." ";
						}
					}
				}

				if($daterange){
					if(!is_null($fechaini_g)){
						$fecha_ini_ = inc_invertir_fecha_gregoriana(JDToGregorian($fechaini_g), "-");
						$comando.=" and CONCAT(snl.fecha, ' ', snl.hora)>=".$fecha_ini_;
					}
					if(!is_null($fechafin_g)){
						$fecha_fin_ = inc_invertir_fecha_gregoriana(JDToGregorian($fechaini_g), "-");
						$comando.=" and CONCAT(snl.fecha, ' ', snl.hora)<=".$fecha_fin_;
					}

				}

				$cadena = trim($cadena);
				$comando.=" and (snl.titulo like '%".$cadena."%' or snl.texto like '%".$cadena."%') ";

			}

			//$comando.= " ORDER BY op.orden ASC, CONCAT(snl.fecha, ' ', snl.hora) DESC, snl.id DESC";
			$comando.= " ORDER BY CONCAT(snl.fecha, ' ', snl.hora) DESC";
			//CONCAT(snl.fecha, ' ', snl.hora) DESC, snl.id DESC, op.orden ASC" ;
			$comando.= " LIMIT ".$limit;

			//echo $comando;
			$sentencia = $this->bd->prepare($comando);

			if ($sentencia->execute()) {
				http_response_code(200);
				$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);
				$array = [];

				if(!empty($rows)){
					foreach($rows as $reg => $item){
						$idpdf = md5(__SECRET_KEY__.$item["boletin_articles_id"]);
						$boletin_articles_description_md_url = 'https://institutopacifico.com.pe/apiv1/normas_legales/md/'.$idpdf;
						$boletin_articles_pdf =	'https://institutopacifico.com.pe/apiv1/normas_legales/pdf/'.$idpdf;
						$description = substr($item["boletin_articles_description"],0, 260);

						$array_data = array(
										  // "boletin_articles_id" => $item["boletin_articles_id"],
										   "boletin_articles_is_starred" => $item["boletin_articles_is_starred"],
										   "boletin_articles_date_when_article_is_starred" => $item["boletin_articles_date_when_article_is_starred"],
										   "boletin_articles_category" => NULL,
										   "boletin_articles_organismo" => $item["boletin_articles_organismo"],
										   "boletin_articles_entidad" => $item["boletin_articles_entidad"],
										   "boletin_articles_image_url" => $item["boletin_articles_image_url"],
										   "boletin_articles_date_added" => $item["boletin_articles_date_added"],
										   "boletin_articles_description" => html_entity_decode(strip_tags($description)),
										   "boletin_articles_description_md_url" => $boletin_articles_description_md_url,
										   "boletin_articles_title" => $item["boletin_articles_title"],
										   "boletin_articles_url" => $item["boletin_articles_url"],
										   "boletin_articles_pdf" => $boletin_articles_pdf
										 );

						array_push($array, $array_data);

					}

				}

				return $array;

            } else
                throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");

        }
		catch (PDOException $e) {
            throw new ExcepcionApi(ESTADO_ERROR, $e->getMessage());
        }
	}

	public function add_favorito(){
		try {
			$body = file_get_contents('php://input');
			$data = json_decode($body);

			//$referenceobject = $data->list_boletin_item_view_object_boletin_starred_articles;
			$referenceobject = $data->reference_object;
			$user_data_object = $data->user_data_object;
			$revista = $data->revista;
			$request = $data->request;

			//VERIFICANDO EL USUARIO
			$session = new session;
			$array_session = $session->get_sign_in($user_data_object, $revista, true, false);
			$estado_session = $array_session["estado"];

			if($estado_session == 0 || empty($estado_session)) throw new ExcepcionApi(ESTADO_ERROR, "No encuentro al usuario");

			$array_session_details = $array_session["user_data_object"];
			$about_membership = $array_session_details->about_membership;

			//SET VALORES DEL USUARIO
			$idlogin = $array_session_details->user_id;
			$tipologin = (strtoupper($about_membership) == strtoupper('Invitado')) ? 'V' : 'S';

			if($request=='save_starred_articles') $array = self::insert_favorito($referenceobject, $idlogin, $tipologin, $revista);
		}
		catch (ExcepcionApi $e) {
			$array = [
				 	'estado' => $e->getStatus(),
				 	'bad_request_message' => $e->getMessage()
				 	];
		}

		return $array;
	}

	public function favorites($recurso){
		try {
			$tipoitem = ($recurso=='boletin_diario_starred_articles') ? 'N' : 'R';
			$body = file_get_contents('php://input');
			$data = json_decode($body);

			//$referenceobject = $data->list_boletin_item_view_object_boletin_starred_articles;
			$user_data_object = $data->user_data_object;
			$revista = $data->revista;

			$compressed = (isset($data->compressed)) ? $data->compressed : false;
			if($compressed) $this->vista = new VistaGzip();

			//VERIFICANDO EL USUARIO
			$session = new session;
			$array_session = $session->get_sign_in($user_data_object, $revista, true, false);
			$estado_session = $array_session["estado"];

			if($estado_session == 0 || empty($estado_session)) throw new ExcepcionApi(ESTADO_ERROR, "No encuentro al usuario");

			$array_session_details = $array_session["user_data_object"];
			$about_membership = $array_session_details->about_membership;

			//SET VALORES DEL USUARIO
			$idlogin = $array_session_details->user_id;
			$tipologin = (strtoupper($about_membership) == strtoupper('Invitado')) ? 'V' : 'S';

			$array = self::get_favoritos($tipoitem, $idlogin, $tipologin, $revista);

			return	[
					"estado" => ESTADO_EXITO,
					"datos" => $array
				];
		}
		catch (ExcepcionApi $e) {
			$array = [
				 	'estado' => $e->getStatus(),
				 	'bad_request_message' => $e->getMessage()
				 	];
		}

		$this->vista($array);
	}
	private function get_favoritos($tipoitem, $idlogin, $tipologin, $request, $step, $referenceobject, $revista='AE'){
		$status_favorito = ESTADO_EXITO;
		$bad_request_message = NULL;
		$array_data = $array_data_id = $array_data_date_added = [];

		try {
			$comando = "SELECT * FROM `siip_favoritos_item` WHERE idlogin = :idlogin and tipologin = :tipologin
					and revista = :revista and tipoitem = :tipoitem ";

			if(!empty($referenceobject)){
				$fecha = $referenceobject->boletin_articles_date_when_article_is_starred;
				if(!empty($fecha)){
					if($request == 'more_data') $comando.= " and date_added < '".$fecha."' ";
					if($request == 'refresh') $comando.= " and date_added > '".$fecha."' ";
				}
			}

			$sentencia = $this->bd->prepare($comando);
			$sentencia->bindParam(":idlogin", $idlogin, PDO::PARAM_INT);
			$sentencia->bindParam(":tipologin", $tipologin, PDO::PARAM_STR);
			$sentencia->bindParam(":revista", $revista, PDO::PARAM_STR);
			$sentencia->bindParam(":tipoitem", $tipoitem, PDO::PARAM_STR);
			$sentencia->execute();
			$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);
			if(!empty($rows)){
				foreach($rows as $reg){
					$array_data_id[] = $reg["iditem"];
					//$tipoitem = $reg["tipoitem"];
					//$referenceobject = (object)["boletin_articles_id" => $iditem];
				}

				$referenceobject = (object)["boletin_articles_id" => $array_data_id];

				if($tipoitem=='N'){
					//GET NOTICIA
					$array = self::noticias_all('get_data', $step, $referenceobject, $idlogin, $tipologin, $revista);
				}elseif($tipoitem=='R'){
					//GET NORMAS
					$array = self::normas_legales('get_data', $step, $referenceobject, $idlogin, $tipologin, $revista);
				}
				//print_r($array);
				//array_push($array_data, $array);
			}

			return $array;
		}
		catch (ExcepcionApi $e) {
			$status_favorito = $e->getStatus();
			$bad_request_message = $e->getMessage();

			return $array = [
				 'estado' => $status_favorito,
				 'bad_request_message' => $bad_request_message
				 ];
		}
		//return $array;
	}
	private function insert_favorito($referenceobject, $idlogin, $tipologin, $revista='AE'){
		$status_favorito = ESTADO_EXITO;
		$bad_request_message = NULL;

		try {
			if(!empty($referenceobject)){
				$this->bd->beginTransaction();
				//foreach($referenceobject as $reg){
				$reg = $referenceobject;
				$boletin_articles_is_starred = $reg->boletin_articles_is_starred;
				//echo "boletin_articles_is_starred ; ".$boletin_articles_is_starred;
				$boletin_articles_title = $reg->boletin_articles_title;
				$boletin_articles_category = $reg->boletin_articles_category;
				$boletin_articles_description = $reg->boletin_articles_description;
				$boletin_articles_date_added = $reg->boletin_articles_date_added;
				$boletin_articles_pdf = $reg->boletin_articles_pdf;
				$boletin_articles_url = $reg->boletin_articles_url;

				//BUSCAMOS SI LOS DATOS COINCIDEN CON UNA NOTICIA
				$array_noticia = self::get_noticia_by_array($boletin_articles_title, $boletin_articles_description, $boletin_articles_date_added);
				$iditem = $array_noticia["boletin_articles_id"];
				$tipoitem = 'N';

				//SINO LO ENCONTRAMOS EN NOTICIAS, BUSQUEMOS EN NORMA
				if(empty($iditem)){
					//BUSCAMOS SI LOS DATOS COINCIDEN CON UNA NORMA
					$array_norma = self::get_norma_legal_by_array($boletin_articles_title, $boletin_articles_description, $boletin_articles_date_added);
					$iditem = $array_norma["boletin_articles_id"];
					$tipoitem = 'R';
				}

				if($iditem == 0 || empty($iditem)) throw new ExcepcionApi(ESTADO_ERROR, "No encuentro la noticia o norma");

				//BUSCAMOS SI EXISTE COMO FAVORITOS PARA EL IDLOGIN
				$sql = "SELECT * FROM siip_favoritos_item
						WHERE idlogin = ? and tipologin = ?
						and iditem = ? and tipoitem = ? and revista = ?";
				$sentencia = $this->bd->prepare($sql);
				$sentencia->bindParam(1, $idlogin, PDO::PARAM_INT);
				$sentencia->bindParam(2, $tipologin, PDO::PARAM_STR);
				$sentencia->bindParam(3, $iditem, PDO::PARAM_INT);
				$sentencia->bindParam(4, $tipoitem, PDO::PARAM_STR);
				$sentencia->bindParam(5, $revista, PDO::PARAM_STR);
				$sentencia->execute();
				$rowfav = $sentencia->fetch(PDO::FETCH_ASSOC);
				$iditemfavorito = $rowfav["iditemfavorito"];

				//VERIFICAMOS SI SE NECESITA O NO QUE SE AGREGUE COMO FAVORITOS
				if($boletin_articles_is_starred){ // SI ES TRUE, PUES VERIFICAMOS ANTES DE AGREGARLO
					//SI NO EXISTE EL ITEM COMO FAVORITO
					if(empty($iditemfavorito)){
						//INSERTAMOS EL REGISTRO
						$sql = "INSERT INTO siip_favoritos_item(`idlogin`, `tipologin`, `iditem`, `tipoitem`, `revista`)
								VALUES(:idlogin, :tipologin, :iditem, :tipoitem, :revista)";
						$sentencia = $this->bd->prepare($sql);
						$sentencia->bindParam(":idlogin", $idlogin, PDO::PARAM_INT);
						$sentencia->bindParam(":tipologin", $tipologin, PDO::PARAM_STR);
						$sentencia->bindParam(":iditem", $iditem, PDO::PARAM_INT);
						$sentencia->bindParam(":tipoitem", $tipoitem, PDO::PARAM_STR);
						$sentencia->bindParam(":revista", $revista, PDO::PARAM_STR);
						$sentencia->execute();
						$iditemfavorito = $this->bd->lastInsertId('iditemfavorito');
					}

					if(empty($iditemfavorito)) throw new ExcepcionApi(ESTADO_ERROR, "No logré guardar en favoritos");
				}
				else{//SI ES FALSE
					if(!empty($iditemfavorito)){
						//ELIMINAMOS EL REGISTRO
						$sql = "DELETE FROM siip_favoritos_item
								WHERE idlogin = ? and tipologin = ?
								and iditem = ? and tipoitem = ? and revista = ?";
						$sentencia = $this->bd->prepare($sql);
						$sentencia->bindParam(1, $idlogin, PDO::PARAM_INT);
						$sentencia->bindParam(2, $tipologin, PDO::PARAM_STR);
						$sentencia->bindParam(3, $iditem, PDO::PARAM_INT);
						$sentencia->bindParam(4, $tipoitem, PDO::PARAM_STR);
						$sentencia->bindParam(5, $revista, PDO::PARAM_STR);
						$sentencia->execute();
					}
					//if(empty($iditemfavorito)) throw new ExcepcionApi(ESTADO_ERROR, "No logré guardar en favoritos");
				}
				//echo $iditemfavorito."<br>";
				//}
				$this->bd->commit();
			}
		}
		catch (ExcepcionApi $e) {
			$status_favorito = $e->getStatus();
			$bad_request_message = $e->getMessage();
		}

		$array = [
				 'estado' => $status_favorito,
				 'bad_request_message' => $bad_request_message
				 ];

		return $array;
	}
	private function get_noticia_by_array($titulo, $texto, $fechora){
		$sql ="SELECT id_noti as 'boletin_articles_id' FROM instiuo7_aempresa.noticias
			   WHERE titulo = :titulo and CONCAT(fecha_mod, ' ', hora_mod) = :fechora";//texto = :texto and
	//	echo $sql."\n".$titulo."\n".$fechora."\n\n";
		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":titulo", $titulo, PDO::PARAM_STR);
		//$sentencia->bindParam(":texto", $texto, PDO::PARAM_STR);
		$sentencia->bindParam(":fechora", $fechora, PDO::PARAM_STR);
		$sentencia->execute();
		$row = $sentencia->fetch(PDO::FETCH_ASSOC);
		return $row;
	}
	private function get_norma_legal_by_array($titulo, $texto, $fechora){
		$sql ="SELECT id as 'boletin_articles_id'
			   FROM instiuo7_aempresa.sumillas_nl
			   WHERE titulo = :titulo and CONCAT(fecha, ' ', hora) = :fechora";
		//and texto = :texto
		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":titulo", $titulo, PDO::PARAM_STR);
		//$sentencia->bindParam(":texto", $texto, PDO::PARAM_STR);
		$sentencia->bindParam(":fechora", $fechora, PDO::PARAM_STR);
		//echo $sql."\n".$titulo."\n".$texto."\n".$fechora;
		$sentencia->execute();
		$row = $sentencia->fetch(PDO::FETCH_ASSOC);
		return $row;
	}
	private function get_noticia($id, $md5=false){
		$sql ="SELECT a.id_noti as boletin_articles_id,
				IF(a.imagen='', NULL, CONCAT('http://aempresarial.com/web/boletin/',a.imagen)) as boletin_articles_image_url,
				CONCAT(fecha_mod, ' ', hora_mod) as boletin_articles_date_added,
				REPLACE(a.texto, '../web/downmailrev.php', 'http://aempresarial.com/web/downmailrev.php') as boletin_articles_description,
				a.titulo as boletin_articles_title,
				concat('http://aempresarial.com/web/informativo.php?id=', a.id_noti) as boletin_articles_url,
				if(a.relacion=1, 'Noticias',
					if(a.relacion=2, 'Comentarios Legales',
						if(a.relacion=3, 'Obligaciones',
							if(a.relacion=4, 'Tips Empresariales',
								if(a.relacion=5, 'Artículos Destacados',
									if(a.relacion=6, 'Jurisprudencia de la Semana', 'Otros')))))) as boletin_articles_category,
				CONCAT(fecha_mod,' ',hora_mod) as 'boletin_articles_fecha'
				FROM instiuo7_aempresa.noticias a WHERE ";
		$sql.= ($md5) ? "md5(CONCAT('".__SECRET_KEY__."', a.id_noti)) = ? " : " a.id_noti = ? ";

		$sentencia = $this->bd->prepare($sql);
		$sentencia->execute(array($id));
		$row = $sentencia->fetch(PDO::FETCH_ASSOC);
		//print_r($row);
		return $row;
	}
	private function get_norma_legal($id, $md5=false){
		$sql ="SELECT
				snl.id as 'boletin_articles_id',
				CONCAT(op.nombre,' ',enl.nombre) AS 'boletin_articles_category',
				snl.texto as 'boletin_articles_description',
				snl.titulo as 'boletin_articles_title',
				snl.ruta_pdf as 'boletin_articles_namepdf',
				CONCAT('http://aempresarial.com/web/nl_cesar_item/', snl.ruta_pdf) as 'boletin_articles_pdf',
				CONCAT(snl.fecha, ' ', snl.hora) as 'boletin_articles_fecha'
				FROM instiuo7_aempresa.sumillas_nl snl, instiuo7_aempresa.org_principal op, instiuo7_aempresa.entidad_nl enl
				where snl.cod_ent_prim = op.id and snl.cod_ent_sec = enl.id and ";
		$sql.= ($md5) ? " md5(CONCAT('".__SECRET_KEY__."', snl.id)) = ? " : " snl.id = ? ";
		//echo $sql;
		$sentencia = $this->bd->prepare($sql);
		$sentencia->execute(array($id));
		$row = $sentencia->fetch(PDO::FETCH_ASSOC);
		return $row;
	}
	private function get_norma_software($id, $md5=false){
		$sql ="SELECT
				ssco.idsiip_soft_contenido as 'boletin_articles_id',
				ssca.nombrecat AS 'boletin_articles_category',
				ssco.html as 'boletin_articles_description',
				ssco.norma as 'boletin_articles_title',
				ssco.file as 'boletin_articles_pdf',
				ssco.fechapublicacion as 'boletin_articles_fecha'
				FROM siip_soft_contenido ssco, siip_soft_categoria ssca, siip_revista sr
				WHERE ssco.soft_categoria_id = ssca.idsiip_soft_categoria and
				ssca.revista_idrevista = sr.idrevista and ssco.soft_categoria_id = :categoria";
		$sql.= ($md5) ? " md5(CONCAT('".__SECRET_KEY__."', ssco.idsiip_soft_contenido)) = ? " : " ssco.idsiip_soft_contenido = ? ";
		//echo $sql;
		$sentencia = $this->bd->prepare($sql);
		$sentencia->execute(array($id));
		$row = $sentencia->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	public function get_file($recurso, $type, $id){
		try{
			//echo $recurso." => ".$type." => ".$id;
			if($recurso=='normas_legales') $array = self::get_norma_legal($id, true);
			elseif($recurso=='normas_software') $array = self::get_norma_software($id, true);
			elseif($recurso=='boletin_diario') $array = self::get_noticia($id, true);
			elseif($recurso=='articulo_revista'){
				$clsSoftware = new software;
				$array = $clsSoftware->get_articulo($id, true);
			}


			if($type == 'xls') $extension = '.xls';
			if($type == 'word') $extension = '.doc';
			if($type == 'pdf') $extension = '.pdf';

			if(empty($array)) throw new ExcepcionApi(ESTADO_ERROR, "No tenemos un registro para descargar");

			//echo $recurso;
			if($recurso=='normas_legales'){
				if($type == 'md'){
					$description = $array["boletin_articles_description"];
					//$converter = new League\HTMLToMarkdown\HtmlConverter(array('strip_tags' => true));
					$text = "<h4>".htmlentities($array["boletin_articles_category"])."</h4>";
					$text.= "<h1>".htmlentities($array["boletin_articles_title"])."</h1>";
					$text.= "<b>".htmlentities($array["boletin_articles_fecha"])."</b>";
					//$text.= "![]()";

					//$text = "<h1>".htmlentities($array["boletin_articles_title"])."</h1>";
					$text.= $description;
					/*$description_md = $converter->convert($text);
					echo $description_md;*/
					$method = "POST";
					$data = array("html" => $text);
					$url = "http://fuckyeahmarkdown.com/go/";
					echo CallAPI($method, $url, $data);

				}else{
					if(empty($array["boletin_articles_pdf"])){
						$namePdf = time().$array["id_noti"];
						$text = htmlentities($array["boletin_articles_description"]);
						ob_start();
						$pdf = new HTML2FPDF();
						$pdf->AddPage();
						if(ini_get('magic_quotes_gpc')=='1') $text=stripslashes($text);
						$pdf->WriteHTML($text);
						$pdf->Output($namePdf.".pdf",'D');
						ob_end_flush();

					}else{
						header("Content-type:application/pdf");
						header("Content-Disposition:attachment;filename=".$array["boletin_articles_namepdf"]);
						readfile($array["boletin_articles_pdf"]);

					}
				}
			}
			elseif($recurso=='articulo_revista'){

				$text = "<h4>".htmlentities($array[0]["boletin_articles_categoria"])."</h4>";
				$text.= "<h1>".htmlentities($array[0]["boletin_articles_title"])."</h1>";
				$text.= "<b>".htmlentities($array[0]["boletin_articles_fecha"])."</b>";
				//if(!empty($array[0]["boletin_articles_image_url"])) $text.= "![IMAGEN](".$array[0]["boletin_articles_image_url"].")";
				//$text.= "![]()";

				if(!empty($array)){
					foreach($array as $reg){
						$text.= "<h2>".htmlentities($reg["titulo_contenidoitem"])."</h2>";
						if($type == 'md'){
							$text.= html_entity_decode($reg["detallehtml"]);
						}else{
							$text.= html_entity_decode(utf8_decode($reg["detallehtml"]));
						}
					}
				}

				if($type == 'md'){
					$method = "POST";
					$data = array("html" => $text);
					$url = "http://fuckyeahmarkdown.com/go/";
					echo CallAPI($method, $url, $data);

				}else{
					$namePdf = time().$array[0]["iditem"];
					ob_start();
					$pdf = new HTML2FPDF();
					$pdf->AddPage();
					if(ini_get('magic_quotes_gpc')=='1') $text=stripslashes($text);
					$pdf->WriteHTML($text);
					$pdf->Output($namePdf.".pdf",'D');
					ob_end_flush();
				}

			}
			else{
				$text = "<h4>".htmlentities($array["boletin_articles_category"])."</h4>";
				$text.= "<h1>".htmlentities($array["boletin_articles_title"])."</h1>";
				$text.= "<b>".htmlentities($array["boletin_articles_fecha"])."</b>";
				if(!empty($array["boletin_articles_image_url"])) $text.= "![IMAGEN](".$array["boletin_articles_image_url"].")";

				$text.= $array["boletin_articles_description"];
				$namePdf = time().$array["id_noti"];
				// Si queremos exportar a PDF

				if($type == 'pdf'){
					ob_start();
					$pdf = new HTML2FPDF();
					$pdf->AddPage();
					if(ini_get('magic_quotes_gpc')=='1') $text=stripslashes($text);
					$pdf->WriteHTML($text);
					$pdf->Output($namePdf.".pdf",'D');
					ob_end_flush();
				}
				elseif($type == 'md'){
					/*$description_md = NULL;
					$converter = new League\HTMLToMarkdown\HtmlConverter(array('strip_tags' => true));
					$description_md = $converter->convert($text);
					echo $description_md;*/
					$method = "POST";
					$data = array("html" => $text);
					$url = "http://fuckyeahmarkdown.com/go/";
					echo CallAPI($method, $url, $data);

				}
				else{
					header('Content-type: application/vnd.ms-'.$type);
					header("Content-Disposition: attachment; filename=".$namePdf.$extension);
					header("Pragma: no-cache");
					header("Expires: 0");
					echo "<html>";
					echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
					echo "<body>";
					echo $row["html"];
					echo "</body>";
					echo "</html>";
				}
			}

		}catch (ExcepcionApi $e) {
		//	$e->getStatus()
			echo '<div style="background-color: #f2dede; border-color: #ebcccc; color: #a94442;padding: .75rem 1.25rem; margin-bottom: 1rem; border: 1px solid transparent; border-radius: .25rem;">'.$e->getMessage().'</div>';
		}
	}
	/*private function noticias_legal_all($request, $step, $referenceobject, $userid, $revista='AE'){
        try {
			{
			$comando= "SELECT a.area_nom as boletin_articles_category, ";

			if(!empty($idlogin) && !empty($tipologin) && !empty($revista)){
				$comando.="
				IF((
				select iditemfavorito from instiuo7_siip1.siip_favoritos_item sfv
				where sfv.idlogin='".$userid."' and sfv.tipologin='S' and sfv.iditem = a.id_noti
				and sfv.tipoitem = 'NL' and sfv.revista = '".$revista."') IS NULL, 'false', 'true') as boletin_articles_is_starred,";

			}

			$comando.="
			IF(not_imagen='', NULL, CONCAT('http://actualidadlegal.institutopacifico.com.pe/imagenes/pic/',not_imagen)) as boletin_articles_image_url,

			CONCAT(n.not_fecha, ' ', n.not_hora) as boletin_articles_date_added,

			not_contenido as boletin_articles_description,

			not_titulo as boletin_articles_title,

			concat('http://actualidadlegal.institutopacifico.com.pe/', sec_nombre_url,'/', area_nom_url,'/',not_titulo_url,'-noticia-',cod_not,'.html') as boletin_articles_url

			FROM instiuo7_alegal.noticias n, instiuo7_alegal.areas a, instiuo7_alegal.secciones s, instiuo7_alegal.edicion_noticia en
			WHERE en.id_edicion=n.id_edicion and n.cod_area=a.cod_area and n.cod_sec=s.cod_sec and n.activo='S'";

			$limit = empty($step) ? '5' : $step;

			$fecha = $referenceobject->boletin_articles_date_added;

			if(!empty($fecha)){
				if($request == 'more_data') $comando.= " and CONCAT(n.not_fecha, ' ', n.not_hora)<'".$fecha."' ";
				if($request == 'refresh') $comando.= " and CONCAT(n.not_fecha, ' ', n.not_hora)>'".$fecha."' ";
			}

			$comando.= " ORDER BY CONCAT(n.not_fecha, ' ', n.not_hora) desc ";
			$comando.= " LIMIT ".$limit;
			//echo $comando;
			$sentencia = $this->bd->prepare($comando);


			if ($sentencia->execute()) {
				http_response_code(200);
				$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);

				$array = [];
				if(!empty($rows)){
					foreach($rows as $reg => $item){
						array_push($array, array(
										"boletin_articles_category" => $item["boletin_articles_category"],
										"boletin_articles_is_starred" => $item["boletin_articles_is_starred"],
										"boletin_articles_image_url" => $item["boletin_articles_image_url"],
										"boletin_articles_date_added" => $item["boletin_articles_date_added"],
										"boletin_articles_description" => html_entity_decode(strip_tags($item["boletin_articles_description"])),
										"boletin_articles_title" => $item["boletin_articles_title"],
										"boletin_articles_url" => $item["boletin_articles_url"]
										));
					}
				}

				return $array;

            } else
                throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");

        }
		}
		catch (PDOException $e) {
            throw new ExcepcionApi(self::ESTADO_ERROR, $e->getMessage());
        }
    ESTADO_ERRORESTADO_ERROR}*/
}
