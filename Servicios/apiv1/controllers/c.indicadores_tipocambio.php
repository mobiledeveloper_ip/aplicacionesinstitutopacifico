<?php
class indicadores_tipocambio
{
	private $bd, $vista;
	private $array;
	private $arraygeneral;

	public function __construct(){
        $this->bd = ConexionBD::obtenerInstancia()->obtenerBD();
		$this->vista = new VistaJson();
    }

	public function init(){
		try{
			$body = file_get_contents('php://input');
			$data = json_decode($body);
			//valores de variables enviada
			$user_data_object = $data->user_data_object;
			$revista = $data->revista;
			$request = $data->request;
			$request = (isset($data->request)) ? $data->request : NULL;
			$adittional = (isset($data->string_additional_parameter)) ? $data->string_additional_parameter : NULL;

			$compressed = (isset($data->compressed)) ? true : false;
			if($compressed) $this->vista = new VistaGzip();


				$array = self::main_function($request,$adittional);

		}catch (ExcepcionApi $e) {
			$status_favorito = $e->getStatus();
			$bad_request_message = $e->getMessage();

			$array = [
				 'estado' => $status_favorito,
				 'bad_request_message' => $bad_request_message
				 ];
		}

		//print_r($array);
		$this->vista->imprimir($array);
	}


private function main_function($request,$adittional){
//	if (($this->request != NULL) and ($this->adittional != NULL) ) {
		$comando ="SELECT *  from siip_indicadores_tipocambio WHERE siip_indicadores_tipocambio.id_moneda = '".$request."' AND siip_indicadores_tipocambio.id_moneda_cambio = '".$adittional."' ORDER BY idtipocambio DESC LIMIT 7";
/*}else{
    $comando ="SELECT *  from siip_indicadores_tipocambio ORDER BY idtipocambio DESC LIMIT 28";
	}*/
		$sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
		$sentencia->execute();
		$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);
		return array("array_indicadores_tipocambio" => $rows);
}
}
