<?php
class indicadores_tipocambio
{

    const NOMBRE_TABLA = "noticias";
  //  const REVISTA = "alias";
/*    const PRIMER_NOMBRE = "primerNombre";
    const PRIMER_APELLIDO = "primerApellido";
    const TELEFONO = "telefono";
    const CORREO = "correo";
    const ID_USUARIO = "idUsuario";*/

    const CODIGO_EXITO = 1;
    const ESTADO_EXITO = 1;
    const ESTADO_ERROR = 2;
    const ESTADO_ERROR_BD = 3;
    const ESTADO_ERROR_PARAMETROS = 4;
    const ESTADO_NO_ENCONTRADO = 5;
	
	public function postTipoCambioMoneda(){
		$idUsuario = usuarios::autorizar();
		$body = file_get_contents('php://input');
		$data = json_decode($body);
		$b_moneda = $data->b_moneda;
		$b_moneda_to = (empty($data->b_moneda_to)) ? 'PEN' : $data->b_moneda_to;
		$b_fecdesde = $data->b_desde;
		$b_fechasta = $data->b_hasta;
		$b_tipo = (empty($data->b_tipo)) ? 'igv' : $data->b_tipo;		
		
		return self::tipocambio($b_moneda, $b_moneda_to, $b_fecdesde, $b_fechasta, $b_tipo);
		
	}
	
	private function tipocambio($b_moneda, $b_moneda_to, $b_fecdesde, $b_fechasta, $b_tipo){
		try{
			if(empty($b_moneda)) throw new ExcepcionApi(self::ESTADO_ERROR, "No tengo una moneda para buscar.");
			//if($b_tipo!='igv' && $b_tipo!='ir') throw new ExcepcionApi(self::ESTADO_ERROR, "El tipo que está buscando no existe.");
			
			if($b_tipo=='ir') $campostipo = ", compra_ir, venta_ir";
			else $campostipo = ", compra_igv, venta_igv";
			
			if(strpos($b_moneda,',')){
				$b_moneda = explode(",", $b_moneda);
				$b_moneda = implode("','", $b_moneda);
			}
			
			$comando = "SELECT distinct(fechapublicacion) as fecha FROM `siip_indicadores_tipocambio` where id_moneda in('".$b_moneda."')";
			if($b_fecdesde==$b_fechasta && !empty($b_fecdesde)){
				$comando.=" and fechapublicacion = '".$b_fecdesde."'";
								
			}elseif(empty($b_fecdesde) && empty($b_fechasta)){
				$comando.=" and MONTH(fechapublicacion) = MONTH(CURDATE())";
				
			}elseif(!empty($b_fecdesde) && !empty($b_fechasta)){
				$comando.=" and fechapublicacion between '".$b_fecdesde."' AND '".$b_fechasta."'";
			}else{
				if(!empty($b_fecdesde)) $comando.=" and fechapublicacion>='".$b_fecdesde."' ";
				if(!empty($b_fechasta)) $comando.=" and fechapublicacion<='".$b_fecdesde."' ";
			}	
			$comando.=" AND id_moneda_cambio = '".$b_moneda_to."'";		
			$comando.=" order by fechapublicacion desc";
			//echo $comando;
			$sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
			$sentencia->execute();
			$datafechas = $sentencia->fetchAll(PDO::FETCH_ASSOC);
			
			if(!empty($datafechas)){
				foreach($datafechas as $reg){
					$fecha = $reg["fecha"];
					$comando ="SELECT id_moneda, fechapublicacion ".$campostipo."  from siip_indicadores_tipocambio 
							   where fechapublicacion = '".$reg["fecha"]."' and id_moneda in('".$b_moneda."')";
					$comando.=" AND id_moneda_cambio = '".$b_moneda_to."'";		
					//echo "SQL: ".$comando."\n";
					$sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
					$sentencia->execute();
					$data = $sentencia->fetchAll(PDO::FETCH_ASSOC);
					
					if(!empty($data)){
						$arraydata = array();
						foreach($data as $regitem){	
							$moneda = $regitem["id_moneda"];
							$fechapub = $regitem["fechapublicacion"];
							if($b_tipo=='ir'){
								$arraydata["compra_ir_".$moneda] = $regitem["compra_ir"];
								$arraydata["venta_ir_".$moneda] = $regitem["venta_ir"];
							}else{
								$arraydata["compra_igv_".$moneda] = $regitem["compra_igv"];
								$arraydata["venta_igv_".$moneda] = $regitem["venta_igv"];
							}
							$arraydata["fechapublicacion_".$moneda] = $fechapub;
						}
						$array[] = $arraydata;
					}
				}
			}

			//if ($sentencia->execute()) {
				http_response_code(200);
				return
					[
						"estado" => self::ESTADO_EXITO,
						"datos" => $array
					];
			/*} else
				throw new ExcepcionApi(self::ESTADO_ERROR, "Se ha producido un error en articulos destacados");*/
			
		} catch (PDOException $e) {
            throw new ExcepcionApi(self::ESTADO_ERROR_BD, $e->getMessage());
        }	
	}
	
}