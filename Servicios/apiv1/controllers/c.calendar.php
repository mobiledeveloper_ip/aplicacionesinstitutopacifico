<?php
class calendar
{
	private $bd, $vista;
	private $array;
	private $arraygeneral;

	public function __construct(){
        $this->bd = ConexionBD::obtenerInstancia()->obtenerBD();
		$this->vista = new VistaJson();
    }

	public function init(){
		try{
			$body = file_get_contents('php://input');
			$data = json_decode($body);
			//valores de variables enviada
			$user_data_object = $data->user_data_object;
			$revista = $data->revista;
			$request = $data->request;

			$adittional = (isset($data->string_additional_parameter)) ? $data->string_additional_parameter : NULL;

			$compressed = (isset($data->compressed)) ? true : false;
			if($compressed) $this->vista = new VistaGzip();


				$array = self::vencimientotributario();

		}catch (ExcepcionApi $e) {
			$status_favorito = $e->getStatus();
			$bad_request_message = $e->getMessage();

			$array = [
				 'estado' => $status_favorito,
				 'bad_request_message' => $bad_request_message
				 ];
		}

		//print_r($array);
		$this->vista->imprimir($array);
	}


private function vencimientotributario(){

		$comando = " SELECT idvencimiento as calendar_id , nombrecategoria as calendar_category , detalle as calendar_description , fechavencimiento as calendar_date ";
		$comando.=" FROM siip_vencimientocategoria svc, siip_vencimientotributario svt ";
		$comando.=" WHERE svt.id_vencimientocategoria = svc.idvencimientocategoria ";
		$comando.=" and svc.estado = 1 ";

		$comando.=" order by fechavencimiento asc";

		$sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);

		$sentencia->execute();
		$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);

		return array("array_of_rich_calendar_date_objects" => $rows);

}
}
