<?php
class software1
{
	private $bd, $vista;
	private $array;
	private $arraygeneral;

	public function __construct(){
        $this->bd = ConexionBD::obtenerInstancia()->obtenerBD();
		$this->vista = new VistaJson();
    }

	public function init(){
		try{
			$body = file_get_contents('php://input');
			$data = json_decode($body);
			//valores de variables enviada
			$user_data_object = $data->user_data_object;
			$revista = $data->revista;
			$request = $data->request;

			$adittional = (isset($data->string_additional_parameter) && ($data->string_additional_parameter!='')) ? $data->string_additional_parameter : NULL;

			$compressed = (isset($data->compressed)) ? $data->compressed : false;
			if($compressed) $this->vista = new VistaGzip();

			//VALIDANDO AL USUARIO
			$user_data_object = self::validate_user($user_data_object, $revista);

			// traemos la data solicitada
			if($request=='sistema_de_informacion'){
				$array = self::tree_files_software($revista, $adittional, $user_data_object);
				$array = ["array_of_sub_items" => $array];
			}elseif($request=='codigos'){
				$array = self::tree_files_revpio($revista, 'Cod', $adittional, $user_data_object);
			}elseif($request=='revista'){
				$array = self::tree_files_revpio($revista, 'Rev', $adittional, $user_data_object);
			}elseif($request=='pionner'){
				$array = self::tree_files_revpio($revista, 'Pio', $adittional, $user_data_object);
			}

		}catch (ExcepcionApi $e) {
			$status_favorito = $e->getStatus();
			$bad_request_message = $e->getMessage();

			$array = [
				 'estado' => $status_favorito,
				 'bad_request_message' => $bad_request_message
				 ];
		}

		//print_r($array);
		$this->vista->imprimir($array);
	}

	//VERIFICANDO DATOS DEL USUARIO ENVIADO
	private function validate_user($user_data_object, $revista){
		$session = new session;
		$array_session = $session->get_sign_in($user_data_object, $revista, true, false);
		$estado_session = $array_session["estado"];

		if($estado_session == 0 || empty($estado_session)) throw new ExcepcionApi(ESTADO_ERROR, "Error de base de datos, usuario no encontrado.");

		return $array_session;
	}

	/** Inicio de Arbol de Revista **/
	private function tree_files_revpio($revista, $tipo, $adittional=NULL, $user_data_object){
		$array_session_details = $array_session["user_data_object"];
		$about_membership = $array_session_details->about_membership;
		$userid = $array_session_details->user_id;
		$tipologin = (strtoupper($about_membership) == strtoupper('Invitado')) ? 'V' : 'S';
		$html = self::all_ediciones_revista($revista, $tipo, $adittional, $userid, $tipologin, $revista);
		return $html;
	}

	private function all_ediciones_revista($revista, $tipo = 'Rev', $adittional=NULL, $userid, $tipologin, $revista){
		$array_parents = $array_ediciones = $array = array();
		//echo "adittional ".$adittional;
		$sql =	"SELECT idedicion, nrovol, CONCAT('http://institutopacifico.com.pe/siip/uploads/revistas/ediciones/', revista, '/', imgmovil) as 'imagenedicion'
				 FROM siip_revista_edicion
				 WHERE tipo = :tipo and revista = :revista
				 and estado = 1 ";
		if($adittional!='SHOW_ALL_ROOTS' && !is_null($adittional)) $sql.= " and idedicion = :idedicion ";
		$sql.= " ORDER BY fechareg DESC";

		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":revista", $revista, PDO::PARAM_STR);
		if($adittional!='SHOW_ALL_ROOTS' && !is_null($adittional)) $sentencia->bindParam(":idedicion", $adittional, PDO::PARAM_STR);
		$sentencia->bindParam(":tipo", $tipo, PDO::PARAM_STR);
		$sentencia->execute();
		$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);

		if(!empty($rows)){
			foreach($rows as $row){
				//$adittional!='SHOW_ALL_ROOTS' &&

				if($adittional!='SHOW_ALL_ROOTS')
				{
					$sql =	"SELECT idindice
							FROM siip_revista_indice
							WHERE idparentindice = 0 and id_edicion = :id_edicion
							ORDER BY orden ASC";
					//echo $sql." => ".$row["idedicion"]."\n";
					$sentencia = $this->bd->prepare($sql);
					$sentencia->bindParam(":id_edicion", $row["idedicion"], PDO::PARAM_STR);
					$sentencia->execute();
					$rowsIndice = $sentencia->fetchAll(PDO::FETCH_ASSOC);
					$array_parents = NULL;

					if(!empty($rowsIndice)){
						foreach($rowsIndice as $indice){
							//echo "idindice : ".$indice["idindice"];
							$array_parent = self::tree_busqueda_revista($indice["idindice"], 0, $userid, $tipologin, $revista, $adittional);
							//$array_parents =
							$array_parent = array_shift($array_parent);
							$array_parents[] = $array_parent;
							//$array_parents = $this->arraygeneral;
							//if(!empty($array_parent)) array_push($array_parents, $array_parent);
						}
					}
				}
				//print_r($array_parents);

				$array_edicion = array(
									"array_of_sub_items" => $array_parents,
									"string_id" => $row["idedicion"],
									"string_date_added" => NULL,
									"string_category" => NULL,
									"string_date_when_article_is_starred" => NULL,
									"string_description" => NULL,
									"string_entidad" => NULL,
									"string_organismo" => NULL,
									"string_name_or_title" => $row["nrovol"],
									"string_url_link_to_image_resource" => $row["imagenedicion"],
									"string_url_link_to_content_in_pdf" => NULL,
									"string_url_link_to_content_in_markdown" => NULL,
									"string_url_link_to_web" => NULL
									);


				array_push($array_ediciones, $array_edicion);
				//echo "\n\n\n";
			}
		}

	//	return $array_ediciones;
		return array("array_of_sub_items" => $array_ediciones);

	}

	public function get_contenidoitem($id, $md5=false){
		$sql = "SELECT i.iditem, ri.titulo as 'boletin_articles_categoria', ci.titulo as 'boletin_articles_title',
				ci.detallehtml, i.fecharegitem as 'boletin_articles_fecha'
				FROM siip_revista_item i, siip_revista_indice ri, siip_revista_contenidoitem ci
				WHERE i.iditem = ci.id_item and i.id_indice = ri.idindice and ";
		$sql.= ($md5) ? " md5(CONCAT('".__SECRET_KEY__."', idcontenidoitem)) = :id " : " idcontenidoitem = :id ";
		//echo $sql." => ".$id;
		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":id", $id, PDO::PARAM_INT);
		if(!$sentencia->execute()) throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");
		$articulo = $sentencia->fetchAll(PDO::FETCH_ASSOC);
		return $articulo;
	}

	public function get_articulo($id, $md5=false){
		$sql = "SELECT i.iditem, ri.titulo as 'boletin_articles_categoria', i.titulo as 'boletin_articles_title',
				ci.titulo as titulo_contenidoitem, ci.detallehtml, i.fecharegitem as 'boletin_articles_fecha'
				FROM siip_revista_item i, siip_revista_indice ri, siip_revista_contenidoitem ci
				WHERE i.iditem = ci.id_item and i.id_indice = ri.idindice and ";
		$sql.= ($md5) ? " md5(CONCAT('".__SECRET_KEY__."', id_item)) = :iditem " : " id_item = :iditem ";
		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":iditem", $id, PDO::PARAM_INT);
		if(!$sentencia->execute()) throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");
		$articulo = $sentencia->fetchAll(PDO::FETCH_ASSOC);
		return $articulo;
	}

	private function all_files_bycat_revista($categoria, $userid, $tipologin, $revista){

		$sql = "SELECT
				iditem as 'boletin_articles_id',
				id_indice as 'boletin_articles_idindice',
				IF((select iditemfavorito from instiuo7_siip1.siip_favoritos_item sfv where sfv.idlogin='".$userid."' and sfv.tipologin='".$tipologin."' and sfv.iditem = iditem and sfv.tipoitem = 'R' and sfv.revista = '".$revista."') IS NULL, 'false', 'true') as boletin_articles_is_starred,
				IFNULL((select date_added from instiuo7_siip1.siip_favoritos_item sfv where sfv.idlogin='".$userid."' and sfv.tipologin='".$tipologin."' and sfv.iditem = iditem and sfv.tipoitem = 'R' and sfv.revista = '".$revista."'), '') as boletin_articles_date_when_article_is_starred,
				(select titulo from siip_revista_indice where idindice = id_indice) as 'boletin_articles_category',
				CONCAT('https://institutopacifico.com.pe/siip/uploads/revistas/ediciones/".$revista."/', foto) as 'boletin_articles_image_url',
				fecharegitem as 'boletin_articles_date_added',
				'' as 'boletin_articles_description',
				titulo as 'boletin_articles_title'
				FROM siip_revista_item
				WHERE id_indice=:id_indice";

		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":id_indice", $categoria, PDO::PARAM_INT);
		//echo "------------------------\n".$sql." => ".$categoria." \n\n";

		if(!$sentencia->execute()) throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");
		$rows_articles = $sentencia->fetchAll(PDO::FETCH_ASSOC);
//		echo count($rows_articles);

		if(!empty($rows_articles)){
			$i = 0;
			foreach($rows_articles as $item){
				//cada articulo tengo que preparar sus contenido item
				$sql_content = "SELECT * FROM `siip_revista_contenidoitem` WHERE `id_item` = '".$item["boletin_articles_id"]."'";
				//echo $sql_content;
				$sentencia = $this->bd->prepare($sql_content);
				$sentencia->execute();
				$rows_contenido = $sentencia->fetchAll(PDO::FETCH_ASSOC);
				$nregistros_contenido = count($rows_contenido);
				$array_sub_contenido = NULL;

				if(!empty($rows_contenido)){
					if($nregistros_contenido>1){
						foreach($rows_contenido as $row_contenido){
							$idpdf = md5(__SECRET_KEY__.$row_contenido["idcontenidoitem"]);
							$boletin_articles_description_md_url =	'https://institutopacifico.com.pe/apiv1/articulo_revista_contenido/md/'.$idpdf;
							$boletin_articles_pdf =	'https://institutopacifico.com.pe/apiv1/articulo_revista_contenido/pdf/'.$idpdf;
							$description = utf8_encode(substr($row_contenido["detalletxt"],0,200));
							$array_sub_contenido[] = [
													"array_of_sub_items" => NULL,
													"string_id" => $row_contenido["idcontenidoitem"],
													"string_date_added" => NULL,
													"string_category" => NULL,
													"string_date_when_article_is_starred" => NULL,
													"string_description" => $description,
													"string_entidad" => NULL,
													"string_organismo" => NULL,
													"string_name_or_title" => $row_contenido["titulo"],
													"string_url_link_to_image_resource" => NULL,
													"string_url_link_to_content_in_pdf" => $boletin_articles_pdf,
													"string_url_link_to_content_in_markdown" => $boletin_articles_description_md_url,
													"string_url_link_to_web" => NULL
													];

						}

						//self::all_files_bycat_revista($idcatsoft, $userid, $tipologin, $revista);
						$array_data_[] = [
										"array_of_sub_items" => $array_sub_contenido,
										"string_id" => $item["boletin_articles_id"],
										"string_date_added" => $item["boletin_articles_date_added"],
										"string_category" => $item["boletin_articles_category"],
										"string_date_when_article_is_starred" => $item["boletin_articles_date_when_article_is_starred"],
									//	"string_description" => html_entity_decode(strip_tags($description)),
										"string_entidad" => NULL,
										"string_organismo" => NULL,
										"string_name_or_title" => $item["boletin_articles_title"],
										"string_url_link_to_image_resource" => $item["boletin_articles_image_url"],
										"string_url_link_to_content_in_pdf" => NULL,
										"string_url_link_to_content_in_markdown" => NULL,
										"string_url_link_to_web" => NULL
										];


					}
					else{
						$row_contenido = $rows_contenido[0];
						$idpdf = md5(__SECRET_KEY__.$row_contenido["idcontenidoitem"]);
						$boletin_articles_description_md_url =	'https://institutopacifico.com.pe/apiv1/articulo_revista_contenido/md/'.$idpdf;
						$boletin_articles_pdf =	'https://institutopacifico.com.pe/apiv1/articulo_revista_contenido/pdf/'.$idpdf;
						$description = utf8_encode(substr($row_contenido["detalletxt"],0,200));
						$array_data_[] = [
										"array_of_sub_items" => NULL,
										"string_id" => $row_contenido["idcontenidoitem"],
										"string_date_added" => NULL,
										"string_category" => NULL,
										"string_date_when_article_is_starred" => NULL,
										"string_description" => $description,
										"string_entidad" => NULL,
										"string_organismo" => NULL,
										"string_name_or_title" => $row_contenido["titulo"],
										"string_url_link_to_image_resource" => NULL,
										"string_url_link_to_content_in_pdf" => $boletin_articles_pdf,
										"string_url_link_to_content_in_markdown" => $boletin_articles_description_md_url,
										"string_url_link_to_web" => NULL
										];

					}
				}


			}
		}

		return $array_data_;
	}

	private function tree_busqueda_revista($padre, $init=1, $userid, $tipologin, $revista, $adittional){
	//	$array_directories = $narray = array();

		if($init==0){
			$sql="select idindice, titulo, rw, idparentindice from siip_revista_indice ";
			$sql.="where idindice = :padre";
		}else{
			$sql="select idindice, titulo, rw, idparentindice from siip_revista_indice ";
			$sql.="where idparentindice = :padre order by orden asc";
		}
		//echo $sql." => ".$padre."\n\n";
		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":padre", $padre, PDO::PARAM_INT);

		if(!$sentencia->execute()) throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");
		$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);

		//echo '<ul id="browser" class="filetree">';
		$contReg = 0;
		if(!empty($rows)){
			$array_data_directory = NULL;
			foreach($rows as $row){
				$idcatsoft = $row["idindice"];
				$string_name_or_title = $row["titulo"];
				$array_of_sub_items = NULL;

				$array_data = NULL;
				//echo $adittional;
				//if($adittional!='SHOW_ALL_ROOTS'){
					$files_bycat_revista = self::all_files_bycat_revista($idcatsoft, $userid, $tipologin, $revista);
					if(!empty($files_bycat_revista)) $array_of_sub_items = $files_bycat_revista;

					$array_of_sub_item = self::tree_busqueda_revista($idcatsoft, 1, $userid, $tipologin, $revista, $adittional);
					if(!empty($array_of_sub_item)) $array_of_sub_items = $array_of_sub_item;
					//array_push($array_of_sub_items, $files_bycat_revista);
				//}

				$array_data_directory[] =  [//"boletin_articles_id" => $item["boletin_articles_id"],
											"array_of_sub_items" => $array_of_sub_items,
											"string_id" => NULL,
											"string_date_added" => NULL,
											"string_category" => NULL,
											"string_date_when_article_is_starred" => NULL,
											"string_description" => NULL,
											"string_entidad" => "",
											"string_organismo" => "",
											"string_name_or_title" => $string_name_or_title,
											"string_url_link_to_image_resource" => NULL,
											"string_url_link_to_content_in_pdf" => NULL,
											"string_url_link_to_content_in_markdown" => NULL,
											"string_url_link_to_web" => NULL
											];

				/*

				*/

				$array_of_sub_items = NULL;
				//echo $string_name_or_title."\n\n";
				//print_r($array_data_directory);

				//array_push($array_directories, $array_data_directory);


			}
			//$this->arraygeneral[] = $array_data_directory;
			return $array_data_directory;
		}

	}
	/** Fin de Arbol **/


	/** Inicio de Arbol de Software **/
	private function tree_files_software($revista, $adittional, $user_data_object){
		$array_session_details = $array_session["user_data_object"];
		$about_membership = $array_session_details->about_membership;
		$userid = $array_session_details->user_id;
		$tipologin = (strtoupper($about_membership) == strtoupper('Invitado')) ? 'V' : 'S';

		//$IdCategoria = 12;
		$categoriarow = $this->get_categoria_parent($revista);
		$IdCategoria = $categoriarow["idsiip_soft_categoria"];
		//echo $IdCategoria."\n";
		$array = self::tree_busqueda_software($IdCategoria, 1, $userid, $tipologin, $revista, $adittional);
		return $array;
	}

	private function get_categoria_parent($revista){
		$sql="select ssc.* from siip_soft_categoria ssc, siip_revista sr
			  where ssc.revista_idrevista = sr.idrevista and alias = :revista and ssc.parent = 0 ";
		//echo $sql." => ".$revista;
		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":revista", $revista, PDO::PARAM_STR);
		$sentencia->execute();
		$row = $sentencia->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	private function categoria_parent_software($padre, $init=1, $userid, $tipologin, $revista){
		$sql="select idsiip_soft_categoria, nombrecat, catrewrite, parent from siip_soft_categoria ";
		$sql.="where parent = :padre order by orden asc";
		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":padre", $padre, PDO::PARAM_INT);
	}
	private function all_files_bycat_software($categoria, $userid, $tipologin, $revista, $adittional=NULL){
		$array = [];
		$sql = "SELECT

				idsiip_soft_contenido as 'boletin_articles_id',

				IF((select iditemfavorito from instiuo7_siip1.siip_favoritos_item sfv where sfv.idlogin='".$userid."' and sfv.tipologin='".$tipologin."' and sfv.iditem = ssco.idsiip_soft_contenido and sfv.tipoitem = 'R' and sfv.revista = '".$revista."') IS NULL, 'false', 'true') as boletin_articles_is_starred,

				IFNULL((select date_added from instiuo7_siip1.siip_favoritos_item sfv where sfv.idlogin='".$userid."' and sfv.tipologin='".$tipologin."' and sfv.iditem = ssco.idsiip_soft_contenido and sfv.tipoitem = 'R' and sfv.revista = '".$revista."'), '') as boletin_articles_date_when_article_is_starred,

				ssca.nombrecat as 'boletin_articles_category',

				'' as 'boletin_articles_image_url',

				fechapublicacion 'boletin_articles_date_added',

				ssco.sumilla as 'boletin_articles_description',

				ssco.titulo as 'boletin_articles_title',

				if(ssco.text='', CONCAT('http://aempresarial.com/aesoft/archivos_pdf/', file), ssco.text) as 'boletin_articles_pdf'

				FROM siip_soft_contenido ssco, siip_soft_categoria ssca, siip_revista sr
				WHERE ssco.soft_categoria_id = ssca.idsiip_soft_categoria and
				ssca.revista_idrevista = sr.idrevista and ssco.soft_categoria_id = :categoria";

				//if($adittional!='SHOW_ALL_ROOTS')
				//$sql.= " and (ssco.titulo like '%".$adittional."%' or ssco.sumilla like '%".$adittional."%')";

		//echo $sql." => ".$categoria;

		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":categoria", $categoria, PDO::PARAM_INT);

		if(!$sentencia->execute()) throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");
		$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);

		if(!empty($rows)){
			foreach($rows as $item){
				//print_r($row);
				$idpdf = md5(__SECRET_KEY__.$item["boletin_articles_id"]);
				$boletin_articles_description_md_url =	'https://institutopacifico.com.pe/apiv1/normas_software/md/'.$idpdf;
				$boletin_articles_pdf =	'https://institutopacifico.com.pe/apiv1/normas_software/pdf/'.$idpdf;
				$description = strip_tags(html_entity_decode($item["boletin_articles_description"]));

				$array_data = array(
								"string_id" => $item["boletin_articles_id"],
								//"boletin_articles_is_starred" => $item["boletin_articles_is_starred"],
								"string_date_when_article_is_starred" => $item["boletin_articles_date_when_article_is_starred"],
								"string_category" => $item["boletin_articles_category"],
								"string_date_added" => $item["boletin_articles_date_added"],
								"string_description" => $description,
								"string_entidad" => NULL,
								"string_organismo" => NULL,
								"string_name_or_title" => $item["boletin_articles_title"],
								//"boletin_articles_url" => $item["boletin_articles_url"],
								"string_url_link_to_image_resource" => $item["boletin_articles_image_url"],
								"string_url_link_to_content_in_pdf" => $boletin_articles_pdf,
								"string_url_link_to_content_in_markdown" => $boletin_articles_description_md_url,
								"string_url_link_to_web" => null
								);

				/*$array_data = array(
								//"boletin_articles_id" => $item["boletin_articles_id"],
								"boletin_articles_is_starred" => $item["boletin_articles_is_starred"],
								"boletin_articles_date_when_article_is_starred" => $item["boletin_articles_date_when_article_is_starred"],
								"boletin_articles_category" => $item["boletin_articles_category"],
								"boletin_articles_image_url" => $item["boletin_articles_image_url"],
								"boletin_articles_date_added" => $item["boletin_articles_date_added"],
								"boletin_articles_description" => $description,
								"boletin_articles_description_md_url" => $boletin_articles_description_md_url,
								"boletin_articles_title" => $item["boletin_articles_title"],
								//"boletin_articles_url" => $item["boletin_articles_url"],
								"boletin_articles_pdf" => $boletin_articles_pdf
								);*/
				array_push($array, $array_data);
			}
		}

		return $array;
	}

	private function tree_busqueda_software($padre, $init=1, $userid, $tipologin, $revista, $adittional=NULL){
		$array_software = array();

		if($init==0){
			$sql="select idsiip_soft_categoria, nombrecat, catrewrite, parent from siip_soft_categoria ";
			$sql.="where idsiip_soft_categoria = :padre";
			if($adittional!='SHOW_ALL_ROOTS' && !is_null($adittional)) $sql.= " and CONCAT('c_',idsiip_soft_categoria) = :idcat ";
		}else{
			$sql="select idsiip_soft_categoria, nombrecat, catrewrite, parent from siip_soft_categoria ";
			$sql.="where parent = :padre ";
			if($adittional!='SHOW_ALL_ROOTS' && !is_null($adittional)) $sql.= " and CONCAT('c_',idsiip_soft_categoria) = :idcat ";
			$sql.= " order by orden asc";
		}

		//$sql.=" LIMIT 2";
		//echo $sql." => ".$padre."\n\n";
		$sentencia = $this->bd->prepare($sql);
		$sentencia->bindParam(":padre", $padre, PDO::PARAM_INT);
		if($adittional!='SHOW_ALL_ROOTS' && !is_null($adittional)) $sentencia->bindParam(":idcat", $adittional, PDO::PARAM_STR);

		if(!$sentencia->execute()) throw new ExcepcionApi(ESTADO_ERROR, "Se ha producido un error");
		$rows = $sentencia->fetchAll(PDO::FETCH_ASSOC);
		$numregCat = count($rows);
		if($numregCat>0 && $adittional!='SHOW_ALL_ROOTS') $adittional = NULL;

		//echo '<ul id="browser" class="filetree">';
		$contReg = 0;
		if(!empty($rows)){
			foreach($rows as $row){

				$idcatsoft = $row["idsiip_soft_categoria"];
				$array_of_sub_items = NULL;

				$array_of_boletin_item_views = NULL;

				//echo $adittional;
				if($adittional!='SHOW_ALL_ROOTS'){
					$array_of_boletin_item_views = self::all_files_bycat_software($idcatsoft, $userid, $tipologin, $revista, $adittional);
					if(!empty($array_of_boletin_item_views)) $array_of_sub_items = $array_of_boletin_item_views;
				}

				if($adittional!='SHOW_ALL_ROOTS'){
					$array_of_sub_item = self::tree_busqueda_software($idcatsoft, 1, $userid, $tipologin, $revista, $adittional);
					//echo "sddddddddddddddddd\n";
					if(!empty($array_of_sub_item)) $array_of_sub_items = $array_of_sub_item;
				}

				$string_name_or_title = $row["nombrecat"];
				$string_id_categoria = "c_".$row["idsiip_soft_categoria"];
				//$array_of_sub_items = NULL;

				$array = [
							"array_of_sub_items" => $array_of_sub_items,
							"string_date_added" => "",
							"string_category" => NULL,
							"string_date_when_article_is_starred" => NULL,
							"string_description" => NULL,
							"string_entidad" => NULL,
							"string_organismo" => NULL,
							"string_id" => $string_id_categoria,
							"string_name_or_title" => $string_name_or_title,
							"string_url_link_to_image_resource" => NULL,
							"string_url_link_to_content_in_pdf" => "",
							"string_url_link_to_content_in_markdown" => "",
							"string_url_link_to_web" => NULL
							];

				//print_r($array);

				array_push($array_software, $array);
			}
		}

		return $array_software;
		//return array("array_of_sub_items" => $array_software);
	}
	/** Fin de Arbol **/

}
