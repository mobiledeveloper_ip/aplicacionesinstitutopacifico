<?php ob_start("ob_gzhandler");

try{
	//error_reporting(0);
	if($_SERVER['SERVER_NAME'] == 'localhost')
		require '../../institutopacifico.com.pe/siip/class/Config.php';
	else
		require '/home/instiuo7/public_html/siip/class/Config.php';

	//UTILIDADES
	require DIRECTORY_SYSTEM_SIIP."class/Vistas/VistaApi.php";
	require DIRECTORY_SYSTEM_SIIP."class/Vistas/VistaJson.php";
	require DIRECTORY_SYSTEM_SIIP."class/Vistas/VistaXML.php";
	require DIRECTORY_SYSTEM_SIIP."class/Vistas/VistaGzip.php";
	require DIRECTORY_SYSTEM_SIIP."class/Fpdf/src/html2fpdf.php";
	require DIRECTORY_SYSTEM_SIIP."class/Markdown/vendor/autoload.php";

	//controladores de webservices
	require "controllers/c.session.php";
	require "controllers/c.software.php";
	require "controllers/c.software1.php";
	require "controllers/c.noticiasae.php";
	require "controllers/c.noticiasae1.php";
	require "controllers/c.calendar.php";
	require "controllers/c.indicadores_tipocambio.php";

	// Preparar manejo de excepciones
	$formato = isset($_GET['formato']) ? $_GET['formato'] : 'json';

	switch ($formato){

		case 'xml':
			$vista = new VistaXML();
			break;
		case 'json':
		default:
		   $vista = new VistaJson();
		   //$vista = new VistaXML();

	}

	set_exception_handler(function ($exception) use ($vista) {

		$cuerpo = array(
			"estado" => $exception->estado,
			"mensaje" => $exception->getMessage()
		);
		if ($exception->getCode()) {
			$vista->estado = $exception->getCode();
		} else {
			$vista->estado = 500;
		}

		$vista->imprimir($cuerpo);

	});

	$peticion = array();
	// Extraer segmento de la url
	if (isset($_GET['PATH_INFO'])) $peticion = explode('/', $_GET['PATH_INFO']);
	else throw new ExcepcionApi(ESTADO_URL_INCORRECTA, utf8_encode("No se reconoce la petición"));

	// Obtener recurso
	$recurso = array_shift($peticion);
	$recursos_existentes = array('boletin_diario', 'boletin_diario_1', 'articulo_revista', 'normas_legales', 'session', 'starred_articles', 'normas_software', 'articulo_revista_contenido',
								 'boletin_diario_starred_articles', 'normas_legales_starred_articles',
								 'tree_of_folders_and_articles', 'tree_of_folders_and_articles_t', 'calendar' , 'indicadores_tipocambio'
								);//, 'boletin_diario_legal');

	// Comprobar si existe el recurso
	if (!in_array($recurso, $recursos_existentes)) {
		throw new ExcepcionApi(ESTADO_EXISTENCIA_RECURSO, "No se reconoce el recurso al que intenta acceder");
	}

	$metodo = strtolower($_SERVER['REQUEST_METHOD']);

	// Filtrar método
	switch ($metodo) {
		case 'post':
			if($recurso=='tree_of_folders_and_articles_t'){
				$clsSoftware = new software1;
				$clsSoftware->init();
			}

			if($recurso=='tree_of_folders_and_articles'){
				$clsSoftware = new software;
				$clsSoftware->init();
			}

			if($recurso=='starred_articles'){
				$boletin = new noticiasae;
				$vista->imprimir($boletin->add_favorito($recurso));
			}

			if($recurso=='boletin_diario_starred_articles' || $recurso=='normas_legales_starred_articles'){
				$boletin = new noticiasae;
				$vista->imprimir($boletin->favorites($recurso));
			}

			if($recurso=='favorites'){
				$boletin = new noticiasae;
				$vista->imprimir($boletin->favorites());
			}

			if($recurso=='boletin_diario_1'){
				$boletin = new noticiasae1;
				$boletin->get_data($recurso);
			}

			if($recurso=='boletin_diario' || $recurso=='normas_legales'){
				$boletin = new noticiasae;
				$boletin->get_data($recurso);
				//$array = $boletin->get_data($recurso);
				//$vista->imprimir($array);
			}

			if($recurso=='session'){
				$session = new session;
				$vista->imprimir($session->sign());
			}

			if($recurso=='calendar'){
				$calendar = new calendar;
				$calendar->init();
			}

			if($recurso=='indicadores_tipocambio'){
				$indicadores_tipocambio = new indicadores_tipocambio;
				$indicadores_tipocambio->init();
			}

			break;

		case 'get':
			$type = array_shift($peticion);
			$id = array_shift($peticion);

			if($recurso == 'boletin_diario' || $recurso == 'normas_legales' || $recurso == 'normas_software' || $recurso == 'articulo_revista' || $recurso == 'articulo_revista_contenido'){
				$boletin = new noticiasae;
				$boletin->get_file($recurso, $type, $id);
			}

			break;

		default:
			// Método no aceptado
			$vista->estado = 405;
			$cuerpo = [
				"estado" => ESTADO_METODO_NO_PERMITIDO,
				"mensaje" => utf8_encode("Método no permitido")
			];
			$vista->imprimir($cuerpo);
			break;
	}

}catch (ExcepcionApi $e) {
//	$e->getStatus()
	echo '<div style="background-color: #f2dede; border-color: #ebcccc; color: #a94442;padding: .75rem 1.25rem; margin-bottom: 1rem; border: 1px solid transparent; border-radius: .25rem;">'.$e->getMessage().'</div>';

}
