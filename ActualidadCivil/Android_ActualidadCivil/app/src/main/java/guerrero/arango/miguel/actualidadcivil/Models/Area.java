package guerrero.arango.miguel.actualidadcivil.Models;

/**
 * Created by Miguel on 09/08/2016.
 */
public class Area {

    String idsiip_soft_categoria;

    String nombrecat;
    String NCarpetas;
    String NArchivos;

    public Area() {
    }

    public Area(String nombrecat) {
        this.nombrecat = nombrecat;
    }

    public String getIdsiip_soft_categoria() {
        return idsiip_soft_categoria;
    }

    public void setIdsiip_soft_categoria(String idsiip_soft_categoria) {
        this.idsiip_soft_categoria = idsiip_soft_categoria;
    }

    public String getNombrecat() {
        return nombrecat;
    }

    public void setNombrecat(String nombrecat) {
        this.nombrecat = nombrecat;
    }

    public String getNCarpetas() {
        return NCarpetas;
    }

    public void setNCarpetas(String NCarpetas) {
        this.NCarpetas = NCarpetas;
    }

    public String getNArchivos() {
        return NArchivos;
    }

    public void setNArchivos(String NArchivos) {
        this.NArchivos = NArchivos;
    }

    public String toString() {
        return (this.nombrecat);
    }
}
