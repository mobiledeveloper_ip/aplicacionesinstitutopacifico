package guerrero.arango.miguel.actualidadcivil.Adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadcivil.Fragments.BoletinDiario;
import guerrero.arango.miguel.actualidadcivil.Models.Seccion;
import guerrero.arango.miguel.actualidadcivil.R;

/**
 * Created by Miguel on 23/05/2016.
 */
public class SeccionesAdapter extends RecyclerView.Adapter<SeccionesAdapter.ViewHolder> {

    ArrayList<Seccion> secciones;

    AppCompatActivity activity;
    int tipoBoletin = 99999;
    FragmentManager fManager;


    public SeccionesAdapter(ArrayList<Seccion> secciones, AppCompatActivity activity, int tipoBoletin) {
        this.secciones = secciones;

        this.tipoBoletin = tipoBoletin;


        this.activity = activity;

        fManager = this.activity.getSupportFragmentManager();
    }

    @Override
    public SeccionesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_seccion, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final SeccionesAdapter.ViewHolder holder, final int position) {

        holder.tvSeccion.setText(secciones.get(position).getNombre());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BoletinDiario boletinDetalleFragment = new BoletinDiario();

                Bundle bundle = new Bundle();
                bundle.putInt("tipo",tipoBoletin);
                bundle.putString("id",secciones.get(position).getId());
                boletinDetalleFragment.setArguments(bundle);

                fManager.beginTransaction().replace(R.id.contenedor, boletinDetalleFragment).addToBackStack(null).commit();
            }
        });




    }

    @Override
    public int getItemCount() {
        return secciones.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView tvSeccion;

        public ViewHolder(View itemView) {
            super(itemView);

            cv = (CardView) itemView.findViewById(R.id.cv);
            tvSeccion = (TextView) itemView.findViewById(R.id.tvSeccion);

        }
    }
}
