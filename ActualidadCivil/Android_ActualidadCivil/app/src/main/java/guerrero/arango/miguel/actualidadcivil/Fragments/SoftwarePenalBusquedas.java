package guerrero.arango.miguel.actualidadcivil.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadcivil.Activities.LoginConfirmacion;
import guerrero.arango.miguel.actualidadcivil.Adapters.BusquedaAdapter;
import guerrero.arango.miguel.actualidadcivil.Custom.AuthRequest;
import guerrero.arango.miguel.actualidadcivil.Singletons.SesionUsuario;
import guerrero.arango.miguel.actualidadcivil.Singletons.VariablesGlobales;
import guerrero.arango.miguel.actualidadcivil.Activities.PrincipalActivity;
import guerrero.arango.miguel.actualidadcivil.Models.BusquedaSoftwareModel;
import guerrero.arango.miguel.actualidadcivil.R;
import guerrero.arango.miguel.actualidadcivil.Singletons.AlertSingleton;
import guerrero.arango.miguel.actualidadcivil.Singletons.ProgressSingleton;
import guerrero.arango.miguel.actualidadcivil.Singletons.VolleySingleton;

/**
 * Created by Miguel on 21/07/2016.
 */
public class SoftwarePenalBusquedas extends Fragment {

    private String TAG = SoftwarePenalBusquedas.this.getClass().getName();

    LinearLayout ll;

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    ArrayList<BusquedaSoftwareModel> listaBusqueda = new ArrayList<>();

    ImageView ivAtras,ivSiguiente,ivCarpetas,ivInicio;
    TextView tvResultados;

    Button bCargar;

    int filas;
    int busquedaDesde = 10;

    String busqueda,desde,hasta,emisor,area,estado,idcategoria = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String dataResult =getArguments().getString("data");

        JSONArray jsonArray = new JSONArray();
        try {
            Gson gson = new Gson();
            jsonArray =new JSONArray(dataResult);

            //listaBusqueda.clear();

            for (int i = 0 ; i < jsonArray.length();i++){
                BusquedaSoftwareModel busquedaSoftwareModel = new BusquedaSoftwareModel();
                busquedaSoftwareModel = gson.fromJson(jsonArray.getJSONObject(i).toString(), BusquedaSoftwareModel.class);

                listaBusqueda.add(busquedaSoftwareModel);
            }

            //tvResultados.setText(""+jsonArray.length() + " de "+ filas);
            //adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.fragment_software_result,container,false);

        String dataResult =getArguments().getString("data");
        filas =  getArguments().getInt("filas");

        ivAtras = (ImageView) ll.findViewById(R.id.ivAtras);
        ivSiguiente = (ImageView) ll.findViewById(R.id.ivSiguiente);
        ivCarpetas = (ImageView) ll.findViewById(R.id.ivCarpetas);
        ivInicio = (ImageView) ll.findViewById(R.id.ivInicio);
        tvResultados = (TextView) ll.findViewById(R.id.tvResultados);
        bCargar = (Button) ll.findViewById(R.id.bCargar);
        bCargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BuscarMas(busquedaDesde);
            }
        });

        // Obtener el Recycler
        recycler = (RecyclerView) ll.findViewById(R.id.rv);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new BusquedaAdapter(listaBusqueda, (AppCompatActivity) getActivity());
        recycler.setAdapter(adapter);

        /*
        JSONArray jsonArray = new JSONArray();
        try {
            Gson gson = new Gson();
            jsonArray =new JSONArray(dataResult);

            listaBusqueda.clear();

            for (int i = 0 ; i < jsonArray.length();i++){
                BusquedaSoftwareModel busquedaSoftwareModel = new BusquedaSoftwareModel();
                busquedaSoftwareModel = gson.fromJson(jsonArray.getJSONObject(i).toString(), BusquedaSoftwareModel.class);

                listaBusqueda.add(busquedaSoftwareModel);
            }

            tvResultados.setText(""+jsonArray.length() + " de "+ filas);
            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(listaBusqueda.size() >= filas){
            bCargar.setVisibility(View.GONE);
        }   else{
            bCargar.setVisibility(View.VISIBLE);
        }*/

        tvResultados.setText(""+listaBusqueda.size() + " de "+ filas);
        if(listaBusqueda.size() >= filas){
            bCargar.setVisibility(View.GONE);
        }   else{
            bCargar.setVisibility(View.VISIBLE);
        }



        ivAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ivSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ivCarpetas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), PrincipalActivity.class);

                i.putExtra("pantalla",0);
                getActivity().startActivity(i);
                getActivity().finish();
            }
        });
        ivInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getActivity().finish();

                Intent intent = new Intent(getActivity(),
                        LoginConfirmacion.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
            }
        });


        idcategoria = getArguments().getString("idcategoria");
        busqueda = getArguments().getString("busqueda");
        desde = getArguments().getString("desde");
        hasta = getArguments().getString("hasta");
        emisor = getArguments().getString("emisor");
        estado = getArguments().getString("estado");
        area = getArguments().getString("area");

        check();




        return  ll;
    }


    void check(){
        if(listaBusqueda.size() == 0 ){
            //getActivity().onBackPressed();
            AlertSingleton.getInstance(getActivity(),getString(R.string.no_data)).dialog.show();
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    void BuscarMas(int resultados_desde){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/contenidosoft";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("id_suscripcion", SesionUsuario.getInstance(getActivity()).getIdCliente());
            jo.put("aliasrevista", VariablesGlobales.getInstance().getRevista());
            jo.put("idcategoria", idcategoria);
            jo.put("busqueda", busqueda);
            jo.put("desde", desde);
            jo.put("hasta", hasta);
            jo.put("emisor", emisor);
            jo.put("estado", estado);
            jo.put("area", area);
            jo.put("resultados_desde", resultados_desde);
            jo.put("resultados_maximo", "10");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                int filas = response.getInt("filasencontradas");

                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    BusquedaSoftwareModel busquedaSoftwareModel = new BusquedaSoftwareModel();
                                    busquedaSoftwareModel = gson.fromJson(result.getJSONObject(i).toString(), BusquedaSoftwareModel.class);

                                    listaBusqueda.add(busquedaSoftwareModel);
                                }

                                busquedaDesde += result.length();

                                if(listaBusqueda.size() >= filas){
                                    bCargar.setVisibility(View.GONE);
                                }   else{
                                    bCargar.setVisibility(View.VISIBLE);
                                }

                                tvResultados.setText(""+listaBusqueda.size() + " de "+ filas);
                                adapter.notifyDataSetChanged();



                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.no_data)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d(TAG,error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }

}
