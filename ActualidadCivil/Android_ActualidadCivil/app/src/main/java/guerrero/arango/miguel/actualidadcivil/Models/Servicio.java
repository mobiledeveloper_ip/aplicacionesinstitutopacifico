package guerrero.arango.miguel.actualidadcivil.Models;

import android.graphics.drawable.Drawable;

/**
 * Created by Miguel on 05/08/2016.
 */
public class Servicio {

    String nombre;
    Drawable imagen;

    public Servicio(String nombre, Drawable imagen) {
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public Servicio() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Drawable getImagen() {
        return imagen;
    }

    public void setImagen(Drawable imagen) {
        this.imagen = imagen;
    }
}
