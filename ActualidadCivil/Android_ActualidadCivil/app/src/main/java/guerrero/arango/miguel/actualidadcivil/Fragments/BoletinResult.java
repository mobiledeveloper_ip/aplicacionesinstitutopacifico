package guerrero.arango.miguel.actualidadcivil.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadcivil.Adapters.BusquedaAdapter;
import guerrero.arango.miguel.actualidadcivil.Models.BusquedaSoftwareModel;
import guerrero.arango.miguel.actualidadcivil.R;
import guerrero.arango.miguel.actualidadcivil.Singletons.AlertSingleton;

/**
 * Created by Miguel on 21/07/2016.
 */
public class BoletinResult extends Fragment {


    LinearLayout ll;

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    ArrayList<BusquedaSoftwareModel> listaBusqueda = new ArrayList<>();




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.fragment_software_result,container,false);

        String dataResult =getArguments().getString("data");





        // Obtener el Recycler
        recycler = (RecyclerView) ll.findViewById(R.id.rv);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new BusquedaAdapter(listaBusqueda, (AppCompatActivity) getActivity());
        recycler.setAdapter(adapter);

        JSONArray jsonArray = new JSONArray();
        try {
            Gson gson = new Gson();

            jsonArray =new JSONArray(dataResult);

            for (int i = 0 ; i < jsonArray.length();i++){
                BusquedaSoftwareModel busquedaSoftwareModel = new BusquedaSoftwareModel();
                busquedaSoftwareModel = gson.fromJson(jsonArray.getJSONObject(i).toString(), BusquedaSoftwareModel.class);

                listaBusqueda.add(busquedaSoftwareModel);

            }

            adapter.notifyDataSetChanged();



        } catch (JSONException e) {
            e.printStackTrace();
        }





        check();


        return  ll;
    }

    void check(){

        if(listaBusqueda.size() == 0 ){
            //getActivity().onBackPressed();
            AlertSingleton.getInstance(getActivity(),getString(R.string.no_data)).dialog.show();

            getActivity().getSupportFragmentManager().popBackStack();


        }

    }
}
