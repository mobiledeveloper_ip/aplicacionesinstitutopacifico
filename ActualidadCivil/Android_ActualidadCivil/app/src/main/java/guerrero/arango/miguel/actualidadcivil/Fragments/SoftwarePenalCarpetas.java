package guerrero.arango.miguel.actualidadcivil.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadcivil.Activities.LoginConfirmacion;
import guerrero.arango.miguel.actualidadcivil.Custom.AuthRequest;
import guerrero.arango.miguel.actualidadcivil.Singletons.ProgressSingleton;
import guerrero.arango.miguel.actualidadcivil.Singletons.SesionUsuario;
import guerrero.arango.miguel.actualidadcivil.Singletons.VariablesGlobales;
import guerrero.arango.miguel.actualidadcivil.Adapters.CarpetasAdapter;
import guerrero.arango.miguel.actualidadcivil.Models.Carpeta;
import guerrero.arango.miguel.actualidadcivil.R;
import guerrero.arango.miguel.actualidadcivil.Singletons.AlertSingleton;
import guerrero.arango.miguel.actualidadcivil.Singletons.VolleySingleton;

/**
 * Created by Miguel on 19/07/2016.
 */
public class SoftwarePenalCarpetas extends Fragment {


    private static String TAG = "CarpetasFragment";
    LinearLayout ll;

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    ArrayList<Carpeta> carpetas = new ArrayList<>();

    ImageView ivInicio,ivAtras,ivCarpetas;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.fragment_software_carpetas,container,false);

        ivInicio = (ImageView) ll.findViewById(R.id.ivInicio);
        ivAtras = (ImageView) ll.findViewById(R.id.ivAtras);
        ivCarpetas = (ImageView) ll.findViewById(R.id.ivCarpetas);
        ivCarpetas.setVisibility(View.GONE);

        // Obtener el Recycler
        recycler = (RecyclerView) ll.findViewById(R.id.rv);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new CarpetasAdapter(carpetas, (AppCompatActivity) getActivity(), this);

        recycler.setAdapter(adapter);

        ivInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getActivity().finish();
                Intent intent = new Intent(getActivity(),
                        LoginConfirmacion.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
            }
        });

        ivAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });



        return ll;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String idsoft = null;
        Bundle arguments = getArguments();

        if (arguments != null){
            idsoft = getArguments().getString("id");
        } else {
            idsoft = "";
        }

        if(idsoft.isEmpty()){

            getCarpetas();
        }   else{
            getCarpetas(idsoft);

        }

    }

    void getCarpetas(){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/categoriasoftpadres";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
          //  jo.put("idCategoria", "");
            jo.put("Revista", VariablesGlobales.getInstance().getRevista());
           // jo.put("Limit", "0");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");


                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    Carpeta carpeta = new Carpeta();
                                    carpeta = gson.fromJson(result.getJSONObject(i).toString(), Carpeta.class);

                                    //Picasso.with(getActivity()).load(boletinModel.getNot_imagen()).fetch();

                                    carpetas.add(carpeta);
                                }


                                adapter.notifyDataSetChanged();

                                if(result.length() == 0){
                                    AlertSingleton.getInstance(getActivity(), getString(R.string.no_data)).dialog.show();
                                    getActivity().onBackPressed();
                                }

                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.Fail_Login)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d("CARPETAS",error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }

    void getCarpetas(String idsoft){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        carpetas.clear();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/categoriashijos";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            //  jo.put("idCategoria", "");
            jo.put("idCategoriaParent", idsoft);
            jo.put("incluyeNCarpetas", 1);
            jo.put("incluyeNArchivos", 1);
            // jo.put("Limit", "0");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    Carpeta carpeta = new Carpeta();
                                    carpeta = gson.fromJson(result.getJSONObject(i).toString(), Carpeta.class);

                                    //Picasso.with(getActivity()).load(boletinModel.getNot_imagen()).fetch();

                                    carpetas.add(carpeta);
                                }
                                adapter.notifyDataSetChanged();

                                if(result.length() == 0){
                                    AlertSingleton.getInstance(getActivity(), getString(R.string.no_data)).dialog.show();
                                    getActivity().onBackPressed();
                                }


                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.Fail_Login)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d("CARPETAS",error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }


    public void Buscar(final String idCategoria){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/contenidosoft";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("id_suscripcion", SesionUsuario.getInstance(getActivity()).getIdCliente());
            jo.put("aliasrevista", VariablesGlobales.getInstance().getRevista());
            jo.put("idcategoria", idCategoria);
            jo.put("busqueda", "");
            jo.put("desde", "");
            jo.put("hasta", "");
            jo.put("emisor", "");
            jo.put("estado", "");
            jo.put("area", "");
            jo.put("resultados_desde", "0");
            jo.put("resultados_maximo", "10");
            jo.put("busqueda_interna", "0");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");
                                int filas = response.getInt("filasencontradas");

                                SoftwarePenalBusquedas softwarePenalResult = new SoftwarePenalBusquedas();
                                Bundle bundle = new Bundle();
                                String dataResult = result.toString();
                                bundle.putString("data",dataResult);
                                bundle.putInt("filas", filas);
                                bundle.putString("idcategoria",idCategoria);
                                softwarePenalResult.setArguments(bundle);

                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, softwarePenalResult ).addToBackStack("null").commit();

                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.no_data)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d(TAG,error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }
}
