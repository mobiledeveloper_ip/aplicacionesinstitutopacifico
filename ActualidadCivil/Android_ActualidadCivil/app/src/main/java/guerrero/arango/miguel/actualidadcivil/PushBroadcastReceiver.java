package guerrero.arango.miguel.actualidadcivil;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.NotificationCompat;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import guerrero.arango.miguel.actualidadcivil.Activities.Login;
import guerrero.arango.miguel.actualidadcivil.Activities.PrincipalActivity;
import guerrero.arango.miguel.actualidadcivil.Fragments.BoletinDiario;
import guerrero.arango.miguel.actualidadcivil.Singletons.SesionUsuario;


/**
 * Created by Miguel on 31/08/2016.
 */
public class PushBroadcastReceiver extends ParsePushBroadcastReceiver {

    private static final String TAG = "ParsePushReceiver";

    /** The name of the Intent extra which contains the JSON payload of the Notification. */
    public static final String KEY_PUSH_DATA = "com.parse.Data";

    /** The name of the Intent fired when a notification has been opened. */
    public static final String ACTION_PUSH_OPEN = "com.parse.push.intent.OPEN";

    protected static final int SMALL_NOTIFICATION_MAX_CHARACTER_LIMIT = 38;


    @Override
    protected Notification getNotification(Context context, Intent intent) {
        //return super.getNotification(context, intent);
        return null;
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        //super.onPushReceive(context, intent);

        JSONObject data = getDataFromIntent(intent);
        // Do something with the data. To create a notification do:

        String tipo = "";
        String texto = "";
        String titulo = "";

        String id_contenido = "";
        String cod_not = "";


        try {
            tipo = data.getString("tipo");
            titulo = data.getString("titulo");
            texto = data.getString("texto");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Random r = new Random();
        int random = r.nextInt(9999999 - 0);

        switch (tipo){
            case "SOFTWARE":

                NotificationManager notificationManagerSoft =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                NotificationCompat.Builder builderSoft = new NotificationCompat.Builder(context);

                builderSoft.setContentTitle(titulo);
                builderSoft.setContentText(texto);
                builderSoft.setSmallIcon(getIcon());
                builderSoft.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.logo));
                builderSoft.setAutoCancel(true);

                try {
                    id_contenido = data.getString("id_contenido");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intentSoftware =new Intent(ACTION_PUSH_OPEN);

                intentSoftware.putExtra("pantalla",3);
                intentSoftware.putExtra("id_contenido",id_contenido);
                intentSoftware.putExtra("bypass","1");

                PendingIntent contentIntent = PendingIntent.getBroadcast(context, random,      intentSoftware, PendingIntent.FLAG_CANCEL_CURRENT);

                builderSoft.setContentIntent(contentIntent);

                notificationManagerSoft.notify(random, builderSoft.build());
                break;
            case "BOLETIN":
                NotificationManager notificationManager =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
                builder.setContentTitle(titulo);
                builder.setContentText(texto);
                builder.setSmallIcon(getIcon());
                builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.logo));
                builder.setAutoCancel(true);




                try {
                    cod_not = data.getString("fecha");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intentBoletin =new Intent(ACTION_PUSH_OPEN);

                intentBoletin.putExtra("pantalla",4);
                intentBoletin.putExtra("cod_not",cod_not);
                intentBoletin.putExtra("bypass","1");

                PendingIntent contentIntentBoletin = PendingIntent.getBroadcast(context, random,  intentBoletin, PendingIntent.FLAG_CANCEL_CURRENT);

                builder.setContentIntent(contentIntentBoletin);

                notificationManager.notify(random, builder.build());

                break;
            default:
                break;
        }

    }

    private int getIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.silueta : R.drawable.logo;
    }

    private JSONObject getDataFromIntent(Intent intent) {
        JSONObject data = null;
        try {
            data = new JSONObject(intent.getExtras().getString(KEY_PUSH_DATA));
        } catch (JSONException e) {
            // Json was not readable...
        }
        return data;
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        //super.onPushOpen(context, intent);

        Intent intentSoftware;
        if(SesionUsuario.getInstance(context).estaLogeado()){
            intentSoftware  = new Intent(context, PrincipalActivity.class);
        }   else{
            intentSoftware  = new Intent(context, Login.class);
        }

        intentSoftware.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intentSoftware.putExtras(intent.getExtras());

        context.startActivity(intentSoftware);
    }
}
