package guerrero.arango.miguel.actualidadcivil.Activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import guerrero.arango.miguel.actualidadcivil.Adapters.ServiciosAdapter;
import guerrero.arango.miguel.actualidadcivil.Models.Servicio;
import guerrero.arango.miguel.actualidadcivil.R;
import guerrero.arango.miguel.actualidadcivil.Singletons.ProgressSingleton;
import guerrero.arango.miguel.actualidadcivil.Singletons.SesionUsuario;

/**
 * Created by Miguel on 05/08/2016.
 */
public class LoginConfirmacion extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    ArrayList<Servicio> servicios = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_confirmacion);

        Servicio servicio1 = new Servicio(getString(R.string.servicio1), ContextCompat.getDrawable(this, R.drawable.software)  );
        servicios.add(servicio1);
        Servicio servicio2 = new Servicio(getString(R.string.servicio2), ContextCompat.getDrawable(this, R.drawable.boletin1)  );
        servicios.add(servicio2);
        Servicio servicio3 = new Servicio(getString(R.string.servicio3), ContextCompat.getDrawable(this, R.drawable.boletin2)  );
        servicios.add(servicio3);

        // Obtener el Recycler
        recycler = (RecyclerView) findViewById(R.id.rv);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new ServiciosAdapter(servicios,this);
        recycler.setAdapter(adapter);

        //imprimir();

    }

    @Override
    public void onBackPressed() {
        if(SesionUsuario.getInstance(LoginConfirmacion.this).estaLogeado()){
            //No hace nada
        }   else{
            super.onBackPressed();
        }

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    void imprimir(){
        PrintDocumentAdapter pda = new PrintDocumentAdapter(){

            @Override
            public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback){
                InputStream input = null;
                OutputStream output = null;

                try {

                    input = new FileInputStream("/sdcard/pdf/Imprimir.pdf");
                    output = new FileOutputStream(destination.getFileDescriptor());

                    byte[] buf = new byte[1024];
                    int bytesRead;

                    while ((bytesRead = input.read(buf)) > 0) {
                        output.write(buf, 0, bytesRead);
                    }

                    callback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});

                } catch (FileNotFoundException ee){
                    //Catch exception
                } catch (Exception e) {
                    //Catch exception
                } finally {
                    try {
                        input.close();
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras){

                if (cancellationSignal.isCanceled()) {
                    callback.onLayoutCancelled();
                    return;
                }

                //int pages = computePageCount(newAttributes);

                PrintDocumentInfo pdi = new PrintDocumentInfo.Builder("Name of file").setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT).build();

                callback.onLayoutFinished(pdi, true);
            }
        };

        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);
        String jobName = this.getString(R.string.app_name) + " Document";
        printManager.print(jobName, pda, null);
    }

}
