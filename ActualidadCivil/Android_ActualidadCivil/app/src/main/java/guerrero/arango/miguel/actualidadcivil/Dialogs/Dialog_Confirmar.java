package guerrero.arango.miguel.actualidadcivil.Dialogs;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import guerrero.arango.miguel.actualidadcivil.Activities.PrincipalActivity;
import guerrero.arango.miguel.actualidadcivil.R;

/**
 * Created by Miguel on 27/06/2016.
 */
public class Dialog_Confirmar extends DialogFragment {

    LinearLayout ll;
    Button bSubmit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.dialog_confirmar,container,false);


        getDialog().setTitle("Bienvenido!");
        setCancelable(false);
        bSubmit = (Button) ll.findViewById(R.id.bSubmit);
        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PrincipalActivity.class);
                startActivity(intent);

                getDialog().cancel();
            }
        });

        return ll;
    }
}
