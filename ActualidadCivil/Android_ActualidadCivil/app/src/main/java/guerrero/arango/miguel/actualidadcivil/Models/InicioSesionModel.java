package guerrero.arango.miguel.actualidadcivil.Models;

/**
 * Created by Miguel on 04/07/2016.
 */
public class InicioSesionModel {

    String dni_ruc;
    String codigo;
    String ape_paterno;
    String nombres_razonsocial;
    String iniciosuscripcion;
    String tipo_persona;
    String ape_materno;
    String finsuscripcion;
    String tiempoexpira;
    String idsuscripcion;

    public InicioSesionModel() {
    }

    public String getDni_ruc() {
        return dni_ruc;
    }

    public void setDni_ruc(String dni_ruc) {
        this.dni_ruc = dni_ruc;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getApe_paterno() {
        return ape_paterno;
    }

    public void setApe_paterno(String ape_paterno) {
        this.ape_paterno = ape_paterno;
    }

    public String getNombres_razonsocial() {
        return nombres_razonsocial;
    }

    public void setNombres_razonsocial(String nombres_razonsocial) {
        this.nombres_razonsocial = nombres_razonsocial;
    }

    public String getIniciosuscripcion() {
        return iniciosuscripcion;
    }

    public void setIniciosuscripcion(String iniciosuscripcion) {
        this.iniciosuscripcion = iniciosuscripcion;
    }

    public String getTipo_persona() {
        return tipo_persona;
    }

    public void setTipo_persona(String tipo_persona) {
        this.tipo_persona = tipo_persona;
    }

    public String getApe_materno() {
        return ape_materno;
    }

    public void setApe_materno(String ape_materno) {
        this.ape_materno = ape_materno;
    }

    public String getFinsuscripcion() {
        return finsuscripcion;
    }

    public void setFinsuscripcion(String finsuscripcion) {
        this.finsuscripcion = finsuscripcion;
    }

    public String getTiempoexpira() {
        return tiempoexpira;
    }

    public void setTiempoexpira(String tiempoexpira) {
        this.tiempoexpira = tiempoexpira;
    }

    public String getIdsuscripcion() {
        return idsuscripcion;
    }

    public void setIdsuscripcion(String idsuscripcion) {
        this.idsuscripcion = idsuscripcion;
    }
}
