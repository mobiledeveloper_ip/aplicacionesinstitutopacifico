package guerrero.arango.miguel.actualidadcivil.Singletons;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v7.app.NotificationCompat;

import guerrero.arango.miguel.actualidadcivil.R;

/**
 * Created by Miguel on 22/08/2016.
 */
public class NotificacionSingleton {

    private static NotificacionSingleton singleton;
    private static Context contextsingleton;

    NotificationCompat.Builder mBuilder;

    public static NotificacionSingleton getInstance(Context context) {

        if(singleton == null){
            singleton = new NotificacionSingleton(context);
        }   else{
            if(context != contextsingleton){
                contextsingleton = context;
                singleton = new NotificacionSingleton(context);

            }
        }

        return singleton;
    }


    public NotificacionSingleton(Context context){

        mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle("Archivo Descargado")
                        .setContentText("Actualidad Penal");

        // Creates an explicit intent for an Activity in your app
        //Intent resultIntent = new Intent(this, SoftwareBusquedaDetalle.class);

        // The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        //TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
// Adds the back stack for the Intent (but not the Intent itself)
        //stackBuilder.addParentStack(getActivity());
// Adds the Intent that starts the Activity to the top of the stack
        //stackBuilder.addNextIntent(resultIntent);
        /*
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);*/
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(0, mBuilder.build());

    }
}
