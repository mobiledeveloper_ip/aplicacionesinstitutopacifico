package guerrero.arango.miguel.actualidadcivil.Adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadcivil.Fragments.SoftwareBusquedaDetalle;
import guerrero.arango.miguel.actualidadcivil.Models.BusquedaSoftwareModel;
import guerrero.arango.miguel.actualidadcivil.R;
import guerrero.arango.miguel.actualidadcivil.Singletons.VariablesGlobales;

/**
 * Created by Miguel on 23/05/2016.
 */
public class BusquedaAdapter extends RecyclerView.Adapter<BusquedaAdapter.ViewHolder> {

    ArrayList<BusquedaSoftwareModel> busquedas;

    AppCompatActivity activity;
    FragmentManager fManager;

    public BusquedaAdapter(ArrayList<BusquedaSoftwareModel> busquedas, AppCompatActivity activity) {
        this.busquedas = busquedas;

        this.activity = activity;

        fManager = this.activity.getSupportFragmentManager();

    }

    @Override
    public BusquedaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_busqueda, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final BusquedaAdapter.ViewHolder holder, final int position) {

        holder.tvTitulo.setText(busquedas.get(position).getTitulo());
        holder.tvArea.setText(busquedas.get(position).getNombrecat());
        holder.tvContenido.setText(Html.fromHtml(busquedas.get(position).getText()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SoftwareBusquedaDetalle boletinDetalleFragment = new SoftwareBusquedaDetalle();

                Bundle bundle = new Bundle();
                bundle.putString("result_titulo",busquedas.get(position).getTitulo());
                bundle.putString("result_area",busquedas.get(position).getNombrecat());
                bundle.putString("result_contenido",busquedas.get(position).getHtml());
                bundle.putString("id_contenido",busquedas.get(position).getIdsiip_soft_contenido());
                bundle.putString("favorito",busquedas.get(position).getEnfavoritos());
                bundle.putString("pdf",busquedas.get(position).getPdf());
                VariablesGlobales.getInstance().setBusquedaSoftware(busquedas.get(position));
                boletinDetalleFragment.setArguments(bundle);

                fManager.beginTransaction().replace(R.id.contenedor, boletinDetalleFragment).addToBackStack(null).commit();
            }
        });


    }

    @Override
    public int getItemCount() {
        return busquedas.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView cv;

        TextView tvTitulo,tvArea, tvContenido;





        public ViewHolder(View itemView) {
            super(itemView);

            cv = (CardView) itemView.findViewById(R.id.cv);
            tvArea = (TextView) itemView.findViewById(R.id.tvArea);
            tvTitulo = (TextView) itemView.findViewById(R.id.tvTitulo);

            tvContenido = (TextView) itemView.findViewById(R.id.tvContenido);

        }
    }
}
