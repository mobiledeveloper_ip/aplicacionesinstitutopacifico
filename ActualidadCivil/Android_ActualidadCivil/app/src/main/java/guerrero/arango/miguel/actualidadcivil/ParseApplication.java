package guerrero.arango.miguel.actualidadcivil;

import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import guerrero.arango.miguel.actualidadcivil.Singletons.VariablesGlobales;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Miguel on 30/07/2015.
 */
public class ParseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        Parse.initialize(this, "Tan6mOsGvcPgJvRGCDFXv5Ye4nt3R9GeKJYYj3gH", "1Z9yllB2GeEe2lD1x6GczK925CWiv4SH04C4Phwd");


        /*
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("s5jPFdNw4XvMW4FYxl1ankp3NNUSW5UXLjommKPw")
                .clientKey("WL0HhQRuuHgtJVAkoS7UFETMqEQsbvWU1KpPV1zX")
                .server("https://parseapi.back4app.com")
                .build()
        );*/

        Parse.setLogLevel(Log.VERBOSE);

        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        //installation.put("GCMSenderId", "94096866337");
        installation.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null){
                    System.out.println("InstallationID guardado!");
                }   else{
                    System.out.println(e.getMessage());
                }
            }
        });




        /*
        ParseObject parseObject = new ParseObject("Test");
        parseObject.put("foo","bar");
        parseObject.saveInBackground();*/

        VariablesGlobales.getInstance().setUrl_base("http://institutopacifico.com.pe/api/");
        VariablesGlobales.getInstance().setRevista("AC");

    }
}
