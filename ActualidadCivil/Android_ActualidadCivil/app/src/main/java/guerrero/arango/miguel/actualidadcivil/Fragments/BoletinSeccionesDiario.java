package guerrero.arango.miguel.actualidadcivil.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadcivil.Activities.LoginConfirmacion;
import guerrero.arango.miguel.actualidadcivil.Activities.PrincipalActivity;
import guerrero.arango.miguel.actualidadcivil.Adapters.SeccionesAdapter;
import guerrero.arango.miguel.actualidadcivil.Models.Seccion;
import guerrero.arango.miguel.actualidadcivil.R;

/**
 * Created by Miguel on 27/06/2016.
 */
public class BoletinSeccionesDiario extends Fragment {

    LinearLayout ll;
    private String TAG = BoletinSeccionesDiario.this.getClass().getName();

    ArrayList<Seccion> secciones = new ArrayList<>();

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    ImageView ivAtras,ivInicio,ivBuscar,ivFecha;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Seccion seccion1 = new Seccion("S01","NORMAS IMPORTANTES DEL DÍA");
        Seccion seccion2 = new Seccion("S02","NOTICIAS IMPORTANTES DEL DÍA");
        Seccion seccion3 = new Seccion("S03","JURISPRUDENCIA ACTUAL");
        Seccion seccion4 = new Seccion("S04","RESUMEN DE LAS NORMAS LEGALES DEL DÍA");
        Seccion seccion5 = new Seccion("S05","TABLA DE ACTUALIZACIÓN NORMATIVA");


        secciones.add(seccion1);
        secciones.add(seccion2);
        secciones.add(seccion3);
        secciones.add(seccion4);
        secciones.add(seccion5);



    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.fragment_boletin_diario,container,false);

        //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Boletín Diario");

        ivAtras = (ImageView) ll.findViewById(R.id.ivAtras);
        ivBuscar = (ImageView) ll.findViewById(R.id.ivBuscar);
        ivInicio = (ImageView) ll.findViewById(R.id.ivInicio);
        ivFecha = (ImageView) ll.findViewById(R.id.ivFecha);

        ivBuscar.setVisibility(View.GONE);
        ivFecha.setVisibility(View.GONE);


        // Obtener el Recycler
        recycler = (RecyclerView) ll.findViewById(R.id.rv);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new SeccionesAdapter(secciones, (AppCompatActivity) getActivity() , 0);

        recycler.setAdapter(adapter);

        ivAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ivFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ivBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), PrincipalActivity.class);

                i.putExtra("pantalla",0);
                getActivity().startActivity(i);
                getActivity().finish();
            }
        });
        ivInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),
                        LoginConfirmacion.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
            }
        });


        return ll;
    }




}
