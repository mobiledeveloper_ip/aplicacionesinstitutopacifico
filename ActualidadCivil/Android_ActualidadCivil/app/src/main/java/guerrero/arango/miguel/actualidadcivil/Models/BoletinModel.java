package guerrero.arango.miguel.actualidadcivil.Models;

/**
 * Created by Miguel on 13/07/2016.
 */
public class BoletinModel {
    String cod_not;
    String cod_area;
    String cod_sec;
    String area;
    String seccion;
    String not_titulo;
    String not_titulo_url;
    String not_imagen;
    String not_contenido_res;
    String not_contenido;
    String thumbnail;
    String not_fecha;
    String not_hora;
    String not_fuente;
    public BoletinModel() {
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getCod_not() {
        return cod_not;
    }

    public void setCod_not(String cod_not) {
        this.cod_not = cod_not;
    }

    public String getCod_area() {
        return cod_area;
    }

    public void setCod_area(String cod_area) {
        this.cod_area = cod_area;
    }

    public String getCod_sec() {
        return cod_sec;
    }

    public void setCod_sec(String cod_sec) {
        this.cod_sec = cod_sec;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getNot_titulo() {
        return not_titulo;
    }

    public void setNot_titulo(String not_titulo) {
        this.not_titulo = not_titulo;
    }

    public String getNot_titulo_url() {
        return not_titulo_url;
    }

    public void setNot_titulo_url(String not_titulo_url) {
        this.not_titulo_url = not_titulo_url;
    }

    public String getNot_imagen() {
        return not_imagen;
    }

    public void setNot_imagen(String not_imagen) {
        this.not_imagen = not_imagen;
    }

    public String getNot_contenido_res() {
        return not_contenido_res;
    }

    public void setNot_contenido_res(String not_contenido_res) {
        this.not_contenido_res = not_contenido_res;
    }

    public String getNot_contenido() {
        return not_contenido;
    }

    public void setNot_contenido(String not_contenido) {
        this.not_contenido = not_contenido;
    }


    public String getNot_fecha() {
        return not_fecha;
    }

    public void setNot_fecha(String not_fecha) {
        this.not_fecha = not_fecha;
    }

    public String getNot_hora() {
        return not_hora;
    }

    public void setNot_hora(String not_hora) {
        this.not_hora = not_hora;
    }

    public String getNot_fuente() {
        return not_fuente;
    }

    public void setNot_fuente(String not_fuente) {
        this.not_fuente = not_fuente;
    }
}
