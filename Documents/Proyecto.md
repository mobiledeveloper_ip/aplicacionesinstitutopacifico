# RESUMEN DEL PROYECTO

## APLICACIONE MODULAR ANDROID PARA: ACTUALIDAD GUBERNAMENTAL, CIVIL Y PENAL


### Problemas encontrados en el proyecto y soluciones planteadas

Los siguientes problemas se traducen en continuos retrasos del proyecto.

 Problema| Solucion
 ---|---
Plazos mal planteados| [Llevar una mejor planificacion de los proyectos y considerar los periodos de solucion de errores](https://www.wrike.com/es/blog/grandes-razones-por-las-que-los-proyectos-fracasan/).
Falta de acceso a la base de datos | Solicitar acceso a la base de datos.

## Recomendaciones para los programadores
|Recomendaciones|
|---|
|Debe ser la misma persona quien diseñe e implemente los servicios de las aplicaciones en Web y Móbil|
|Apostar por el diseño de  modular para asi poder sacar mas aplicaciones en el menor tiempo y no programar lo mismo una y otra vez.   |
|  Restringir el uso de librearias especializadas, y apostar por el diseño 100% nativo. |
|En caso usar librerias, asegurar su compatibilidad con android 4.1 o inferior.   |
|Trabajar subir y testear las versiones Beta en google play antes de publicarlas. Enviar correo cada vez que se suba una nueva version de las aplicaciones.   |


### Diagrama de Gantt
Diagrama del proyecto, comenzado desde la semana [14 del año 2017]( https://www.calendario-365.es/calendario-2017.html).

Tareas/Semanas | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 |22 | 23 |24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|
---------------|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
1. ETAPA: Aplicacion|
1.1 Analisis y diseño de la aplicacion|x|x||||||||||||||||||
1.2 Implementacion de las funcionalidades de la aplicacion|||x|x|x|||||||||||||||
1.4 Diseño del frontend|||||x|x|x|x|x|||||||||||
1.5 Rediseño de la aplicacion segun gerentes|||||||x|x|x|x||||||||||
2. SERVICIO: Boletines|
2.1 Implementacion de servicios|||||x|x|x|x|x||x|||||||||
2.2 Solucion de Errores ||||||||x|x|x|x
2.4 Subir la aplicacion al PlayStore|||||||||||x|
3. SERVICIO: Favoritos|
3.1 Implementacion de servicios|||||x|x|x|x|x||x
3.2 Solucion de Errores ||||||||x|x|x|x
4. SERVICIO: Tree Of Folders and Articles : Revista, Sistema de Informacion, Codigos, Pionner|
4.1 Diseño de servicio|||||||||||x
4.2 Implementacion de servicios||||||||||||x|x|x|x|x|x|x|x|x
4.3 MEJORA: Navegacion mediante SwipeView|||||||||||||||||||x|
4.4 Solucion de Errores||||||||||||x|x|x|x|x|x|x|x|x
5. SERVICIO: Usuarios y cuentas|
5.1 Diseño de servicio|||||||||||x
5.2 Implementacion de servicios||||||||||||x|x|||||||
6. IMPLEMENTACION: Produccion de Aplicaciones CIVIL, y Penal|||||||||||||x|x||||||
7. MEJORA/WEB: Uso de Controlador de Versiones GIT/[Bitbucket](bitbucket.org)|||||||||||||||x|||||
8. MEJORA: Implementacion de compresion de Datos Gzip||||||||||||||||x||||
9. MEJORA: Cambio de hacia generacion de servicios Local, sin depender de Chiclayo||||||||||||||||||||x|x
10. MEJORA: Eliminacion de asteriscos en modulos de revista. ||||||||||||||||||||||x
11. MEJORA: Solucion de errores en servicio de lectura, trabajo con limite de tamaño de texto en lectura |||||||||||||||||||||||x
11. SERVICIO: Vencimientos y Obligaciones||||||||||||||||||||||||x|x|x|||
12. SERVICIO: Indicadores Financieros|||||||||||||||||||||||||||x|x|x

### Tareas Pendientes a la fecha: ***semana 42***
Las siguientes tareas se encuentran pendientes

* Desarrollo de la nueva ***aplicacion para control de personal de ventas***.
* Desarrollo de Aplicacion ***ACTUALIDAD LITE*** tipo ***WebApp*** para ***ANDROID y iOS***.
* Solicitar recursos para compilar la aplicacion ***Actualidad Empresarial***.

---
Autor:

***Fernando Rubio Burga***

Analista Programador Móbil

Bachiller Ing. Mecatronico

nando.rrb.00@gmail.com
