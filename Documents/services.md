# SERVICIOS DE LAS APLICACIONES DE INSTITUTO PACIFICO

URL del API:

https://institutopacifico.com.pe/apiv1/

Tipos de Servicios

1. ***boletin_diario***
2. ***normas_legales***
3. ***starred_articles***
4. ***session***
5. ***tree_of_folders_and_articles***

## INTRODUCCION
Se usa el HTTP request ***POST*** el cual tiene como body la peticion, luego se recibe otro archivo JSON el cual contiene los objetos solicitados, asi como el estado de la peticion y la razon de la falla, en el caso que lo hubiera.

### Body Object
```json
{
  "user_data_object":  {...},
  "request":"load_initial_values",
  "revista":"AG",
  "string_additional_parameter":"",
  "reference_object":{...},
  "step":"14"
}
```

### Response Object
```json
{
"estado":1,
"datos":[],
"bad_request_message":"",
"user_data_object":"user_data_object"
}
```

Donde,

***bad_request_message***
  * es la razon de por qué no se ha podido registrar o logger el usuario.

---
## 1. y 2. BOLETINES:

Los boletines se solicitan enviando el siguiente archivo JSON.

```json
{
"user_data_object":
  {
    "about_membership":"Registrese para poder guardar sus datos",
    "about_user_name":"Usuario Anonimo",
    "about_address":"direccion del usuario",
    "user_email":"guest@email.com.pe",
    "user_id":"",
    "user_password":"000000",
    "about_phone_number":"987654321"},
"request":"load_initial_values",
"revista":"AG",
"step":"14"}
```
donde:

Se manda el objeto de usuario, ***user_data_object***, importante para descargar los favoritos.

### Tipos de request:

1.	***load_initial_values***
    * mostrar las ultimas noticias
2.	***more_data***
    * mostrar mas noticias
3.	***refresh***
    * revisar si hay nuevas noticias

### Tipos de revista
Para el parametro ***revista*** , pueden existir los valores:
1. ***AG*** , actualidad gubernamental
2. ***AE*** , actualidad empresarial,
3. etc

El `step` son el numero de objetos que se esta pidiendo, deben ser enteros `step>0` para ***load_initial_values*** y/o ***more_data***. Así como pueden ser `0` para refresh, dado que no se sabe cuantas nuevas noticias hay.

***reference_object***
* sera un objeto json, pudiendo ser el primer objeto de la lista para el caso ***refresh***, o el ultimo objeto para el caso ***load_more***.

### Ejemplo de peticion ***more_data***:

```json
{
  "user_data_object":{
    "about_membership":"Registrese para poder guardar sus datos",
    "about_user_name":"Usuario Anonimo",
    "about_address":"direccion delusuario",
    "user_email":"guest@email.com.pe",
    "user_id":"",
    "user_password":"000000",
    "about_phone_number":"987654321"},
    "reference_object":{
      "boletin_articles_is_starred":false,
      "boletin_articles_image_url":"http://aempresarial.com/web/boletin/2017-05-31__YUQ.jpg",
      "boletin_articles_date_added":"2017-05-3110:36:43",
      "boletin_articles_date_when_article_is_starred":"",
      "boletin_articles_description":"La Libertad. Productores agrícolas de la región La Libertad concretaron la exportación de palta y espárrago",
      "boletin_articles_title":"Exportarán palta y espárrago por S/ 500 millones para el año 2018",
      "boletin_articles_category":"Noticias",
      "boletin_articles_pdf":"https://institutopacifico.com.pe/apiv1/boletin_diario/pdf/e3913cfedc6d75a8c3aa22cf7946b980",
      "boletin_articles_description_md_url":"https://institutopacifico.com.pe/apiv1/boletin_diario/md/e3913cfedc6d75a8c3aa22cf7946b980",
      "boletin_articles_url":"http://aempresarial.com/web/informativo.php?id\u003d38560"},
      "request":"more_data",
      "revista":"AG",
      "step":"14"
    }
```

### Respuesta:
Respuesta del servidor:
```json
{
"estado":1,
"datos":[
{
  "boletin_articles_is_starred":"false",
  "boletin_articles_date_when_article_is_starred":"",
  "boletin_articles_category":"Comentarios Legales",
  "boletin_articles_organismo":"",
  "boletin_articles_entidad":"",
  "boletin_articles_image_url":"http://aempresarial.com/web/boletin/2017-06-01__XRA.jpg",
  "boletin_articles_date_added":"2017-06-01 11:05:50",
  "boletin_articles_description":"Decreto Supremo Nº 009-2017-TR\r\n",
  "boletin_articles_description_md_url":"https://institutopacifico.com.pe/apiv1/boletin_diario/md/35a1b6b56dcc14641ec850b46343b4a7",
  "boletin_articles_title":"Decreto Supremo que modifica el Reglamento de la Ley de Relaciones Colectivas de Trabajo, aprobado por Decreto Supremo Nº 011-92-TR",
  "boletin_articles_url":"http://aempresarial.com/web/informativo.php?id=38579",
  "boletin_articles_pdf":"https://institutopacifico.com.pe/apiv1/boletin_diario/pdf/35a1b6b56dcc14641ec850b46343b4a7"
},
{...},{...},...,{...}
]}
```

En dicha respuesta, los objetos solicitados estan en el JsonArray, ***datos***.
En cada objeto se tiene lo siguiente:


Donde:

* ***boletin_articles_is_starred***
  * denota si el objeto esta marcado como favorito
* ***boletin_articles_category***
  * _SOLO PARA BOLETIN DIARIO_: categoria del boletin diario
* ***boletin_articles_organismo***
  *  _SOLO PARA NORMAS LEGALES_: Organismo que presenta la norma
* ***boletin_articles_entidad***
  *  _SOLO PARA NORMAS LEGALES_: Entidad que presenta la norma
* ***boletin_articles_image_url***
  *  URL de la imagen del boletin
* ***boletin_articles_date_added***
  *  fecha en que ha sido añadida la norma
* ***boletin_articles_description***
  *  Descripcion corta de la norma(250 caractereres)
* ***boletin_articles_description_md_url***
  *   Link del archivo Markdown de la norma.
* ***boletin_articles_title***
  *  Titulo de la norma.
* ***boletin_articles_url***
  *  Url de la norma, sirve para compartir el articulo.
* ***boletin_articles_pdf***
  *  URL del archivo en PDF de la norma.

Dichos datos son mostrados en las listas de la aplicacion respecto a las necesidades del usuario.

### Busqueda
El servicio de busqueda, consiste en enviar un filtro que se encuentra dentro de la peticion, en el parametro ***string_additional_parameter***, dicho filtro cuenta con distintas caracteristicas.
Varios parametros de busqueda pueden ser usados al mismo tiempo.

Nuestras busquedas se basan en los [comandos de busqueda de google (Google Search Operators)](https://bynd.com/news-ideas/google-advanced-search-comprehensive-list-google-search-operators/) .
* ***daterange:*** _JulianFormatNumber_-_JulianFormatNumber2_
  * rango de fechas en que se busca los articulos. Las fechas estan en formato juliano, yyyyD=2017171. Se puede usar una o dos fechas, para una fecha en concreta o un rango de fechas respectivamente.
* ***SHOW_MY_STARRED_ARTICLES***
  * Sirve para mostrar todos los articulos marcados como favoritos.

#### Ejemplos de peticiones ***string_additional_parameter***
Para buscar en un rango de fechas:

 ```json
 {"user_data_object":{},
   "reference_object":{},
     "request":"save_starred_articles",
     "string_additional_parameter":"sunat daterange:2017171-2017179",
     "revista":"AG"
   }
 ```
Para buscar en un solo dia:
 ```json
 {...
     "string_additional_parameter":"sunat daterange:2017171",
     ...
   }
 ```



## 3. GUARDAR FAVORITOS
Enviando un Body de la siguiente manera:
 ```json
 {"user_data_object":{
   "about_membership":"Invitado",
   "about_user_name":"Iván Fiestas",
   "about_address":"Av. Augusto B. Leguía",
   "user_email":"ifiestass@institutopacifico.com.pe",
   "user_id":"1939",
   "user_password":"12342342134",
   "about_phone_number":"1351234"
 },
   "reference_object":{
     "boletin_articles_is_starred":true,
     "boletin_articles_image_url":"",
     "boletin_articles_date_added":"2017-06-02 10:50:17",
     "boletin_articles_date_when_article_is_starred":"",
     "boletin_articles_description":"Las rentas producidas de los bienes comunes por la sociedad conyugal, pueden optar por atribuirse a solo uno de los cónyuge",
     "boletin_articles_title":"¿Es posible atribuir las rentas por los bienes comunes de una sociedad conyugal a solo uno de los cónyuges, para efectos de la declaración y el pago del impuesto?",
     "boletin_articles_category":"Tips Empresariales",
     "boletin_articles_pdf":"https://institutopacifico.com.pe/apiv1/boletin_diario/pdf/f798bbea56dbb8902d3343ea6bcbc281",
     "boletin_articles_description_md_url":"https://institutopacifico.com.pe/apiv1/boletin_diario/md/f798bbea56dbb8902d3343ea6bcbc281",
     "boletin_articles_url":"http://aempresarial.com/web/informativo.php?id\u003d38589"},
     "request":"save_starred_articles",
     "revista":"AG"
   }
```

Se espererara un JSON como el siguiente
 ```json
{
    "estado":1,
    "bad_request_message":null
  }
  ```
---

## 4. SESIONES Y USUARIOS

Se tienen las siguientes peticiones posibles:
1. ***sign_in***
  * para Acceder a una cuenta ya creada
2. ***sign_up***
  * Para registrar Usuario
3. ***I_FORGOT_MY_DATA***
  * Para recuperar una contraseña

API: ***.../session***

Enviando un Body de la siguiente manera:
 ```json
 {"user_data_object":{
   "about_membership":"Regístrate y disfruta de todos nuesteros servicios.",
   "about_user_name":"Usuario Anonimo",
   "about_address":"direccion del usuario",
   "user_email":"ifiestass@institutopacifico.com.pe",
   "user_id":"",
   "user_password":"sdfsdfds",
   "about_phone_number":"242342"},
   "request":"sign_in",
   "revista":"AG"}

```

Se espererara un JSON como el siguiente
 ```json
{
  "user_data_object":{
    "about_membership":"Invitado",
    "about_user_name":"Iván Fiestas",
    "about_address":"Av. Augusto B. Leguía",
    "user_email":"ifiestass@institutopacifico.com.pe",
    "user_id":"1939",
    "user_password":"4353453454354",
    "about_phone_number":"35345345"},
    "estado":1,
    "bad_request_message":null
  }
 ```


###  Recuperacion de contraseña

Se enviara el siguiente objeto:
```json
{
  "user_data_object":  {
    ...
    "user_email":"ifiestass@institutopacifico.com.pe",
    ...},
  "request":"I_FORGOT_MY_DATA",
  "revista":"AG",
}
```

Donde los parametros mas importantes, son ***user_email***, ***request*** y ***revista***.

Se espererara un JSON como el siguiente
 ```json
{
    "estado":1,
    "bad_request_message":null
  }
  ```

  Al usuario se le enviara un mensaje de recuperacion de contraseña al correo dado.

  ---
  ## 5. SISTEMA DE INFORMACION y 6. REVISTA , PIONERS, ETC
API: ***.../tree_of_folders_and_articles***

### Tipos de request:

1.	***sistema_de_informacion***
2.	***revista***

  Se Pedira lo siguiente:
  ```json
 {"request":"sistema_de_informacion",
   "revista":"AG",
   "user_data_object":{
     "about_membership":"Invitado",
     "about_user_name":"Iván Fiestas",
     "about_address":"Av. Augusto B. Leguía",
     "user_email":"ifiestass@institutopacifico.com.pe",
     "user_id":"1939",
     "user_password":"918bacef714f3ff760915e7e4f9e4479",
     "about_phone_number":"946521561"}
}
   ```

   Se Obtendra el siguiente JsonObject

   ```json
   {"array_of_sub_items":[
     {"array_of_sub_items":[],
       "string_date_added":"",
       "string_category":null,
       "string_date_when_article_is_starred":null,
       "string_description":null,
       "string_entidad":null,
       "string_organismo":null,
       "string_name_or_title":"",
       "string_url_link_to_image_resource":null,
       "string_url_link_to_content_in_pdf":"",
       "string__url_link_to_content_in_markdown":"",
       "string_url_link_to_web":null
       }
    ,{"array_of_sub_items":[],
      "string_date_added":null,
      "string_category":null,
      "string_date_when_article_is_starred":null,
      "string_description":null,
      "string_entidad":null,
      "string_organismo":null,
      "string_name_or_title":"",
      "string_url_link_to_image_resource":null,
      "string_url_link_to_content_in_pdf":"",
      "string__url_link_to_content_in_markdown":"",
      "string_url_link_to_web":null},
      {"array_of_sub_items":[],
        "string_date_added":null,
        "string_category":null,
        "string_date_when_article_is_starred":null,
        "string_description":null,
        "string_entidad":null,
        "string_organismo":null,
        "string_name_or_title":"",
        "string_url_link_to_image_resource":null,
        "string_url_link_to_content_in_pdf":"",
        "string__url_link_to_content_in_markdown":"",
        "string_url_link_to_web":null}
      ],
      "string_date_added":null,
      "string_category":null,
      "string_date_when_article_is_starred":null,
      "string_description":null,
      "string_entidad":null,
      "string_organismo":null,
      "string_name_or_title":"N.º 376",
      "string_url_link_to_image_resource":null,
      "string_url_link_to_content_in_pdf":"",
      "string__url_link_to_content_in_markdown":"",
      "string_url_link_to_web":null
   }
```
