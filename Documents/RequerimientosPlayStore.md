# REQUERIMIENTOS REVISTAS ACTUALIDAD

## GOOGLE PLAY STORE



### Título

Actualidad Gubernamental

***maximo 50 caracteres***

### Descripción breve


***maximo 80 caracteres***

### Descripción completa


***maximo 4000 caracteres***


### Imagenes
* Capturas de pantalla:
Archivo JPEG o PNG de 24 bits (sin alpha). Longitud mínima para los laterales: 320 píxeles. Longitud máxima para los laterales: 3840 píxeles.

  * Se necesitan al menos 2 capturas de pantalla en total (8 capturas de pantalla como máximo por tipo). Arrástralas para reorganizarlas o para desplazarte entre los tipos.
Para que tu aplicación aparezca destacada en la lista de aplicaciones diseñadas para tablets de Google Play Store, debes cargar al menos una captura de pantalla para tablets de 7" y otra para tablets de 10". Si ya cargaste capturas de pantalla, asegúrate de ubicarlas en el área correspondiente que aparece a continuación.
Obtén más información sobre cómo se mostrarán las capturas de pantalla de tablets en las fichas de Google Play Store.



* Ícono de alta resolución:
512 x 512
PNG de 32 bits (alfa)


* Gráfico de la función:
1024 x 500
JPG o PNG de 24 bits (no alfa)


* Gráfico promocional:
180 x 120
JPG o PNG de 24 bits (no alfa)

* Banner de TV:
1280 de ancho por 720 de largo
JPG o PNG de 24 bits (no alfa)


* Imagen estereoscópica de Daydream en 360º:
4,096 de ancho x 4,096 de alto
JPG o PNG de 24 bits (no alfa)


### Video promocional
Video de Youtube:
Ingresa una URL.




### Sitio web
http://www.institutopacifico.com.pe

### Dirección de correo

webmaster@institutopacifico.com.pe

### Teléfono
+51 987654321
### Política de privacidad
Si quieres proporcionar una dirección URL a la política de privacidad para esta app, escríbela a continuación. Además, consulta nuestra política de Datos de usuario para evitar incumplimientos comunes.

***http://...***


---
Autor:

***Fernando Rubio Burga***

Analista programador Mobil

Bachiller Ing. Mecatronico
