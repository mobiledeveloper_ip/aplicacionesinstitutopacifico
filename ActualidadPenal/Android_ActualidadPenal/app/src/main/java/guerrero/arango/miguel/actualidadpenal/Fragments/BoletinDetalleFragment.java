package guerrero.arango.miguel.actualidadpenal.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import guerrero.arango.miguel.actualidadpenal.Activities.PrincipalActivity;
import guerrero.arango.miguel.actualidadpenal.Custom.AuthRequest;
import guerrero.arango.miguel.actualidadpenal.Models.BoletinModel;
import guerrero.arango.miguel.actualidadpenal.R;
import guerrero.arango.miguel.actualidadpenal.Singletons.AlertSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.ProgressSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.VariablesGlobales;
import guerrero.arango.miguel.actualidadpenal.Singletons.VolleySingleton;

/**
 * Created by Miguel on 14/07/2016.
 */
public class BoletinDetalleFragment extends Fragment {


    LinearLayout ll;
    TextView tvTitulo,tvArea,tvFecha,tvFuente;
    ImageView ivImagen;

    String titulo,area,contenido,fecha,fuente;

    ImageView ivAtras,ivCompartir,ivBuscar,ivFecha;

    JustifiedTextView jtvContenido;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.fragment_boletin_detalle,container,false);



        ivAtras = (ImageView) ll.findViewById(R.id.ivAtras);
        ivBuscar = (ImageView) ll.findViewById(R.id.ivBuscar);
        ivCompartir = (ImageView) ll.findViewById(R.id.ivCompartir);
        ivFecha = (ImageView) ll.findViewById(R.id.ivFecha);
        tvFecha = (TextView) ll.findViewById(R.id.tvFecha);
        tvFuente = (TextView) ll.findViewById(R.id.tvFuente);
        jtvContenido = (JustifiedTextView) ll.findViewById(R.id.jtvContenido);
        jtvContenido.setAutoLinkMask(Linkify.WEB_URLS);


        ivBuscar.setVisibility(View.GONE);
        ivFecha.setVisibility(View.GONE);


        tvTitulo = (TextView) ll.findViewById(R.id.tvTitulo);
        tvArea = (TextView) ll.findViewById(R.id.tvArea);
        ivImagen = (ImageView) ll.findViewById(R.id.ivImagen);
        ivImagen.setScaleType(ImageView.ScaleType.FIT_XY);
        ivImagen.setAdjustViewBounds(true);






       // ivImagen.setScaleType(ImageView.ScaleType.FIT_XY);


        if(getArguments().getString("bypass") =="1"){
            getData(getArguments().getString("cod_not"));

        }   else{
            titulo =getArguments().getString("result_titulo");
            area =getArguments().getString("result_area");
            fuente = getArguments().getString("fuente_imagen");
            contenido = Html.fromHtml(getArguments().getString("result_contenido").toString()).toString();

            String url = getArguments().getString("result_url");
            fecha =   getArguments().getString("fecha");
            tvFecha.setText(""+fecha);


            Picasso.with(getActivity()).load(url).into(ivImagen);

            tvTitulo.setText(titulo);
            tvArea.setText(area);
            tvFuente.setText("Fuente: "+fuente);
            jtvContenido.setText(contenido);

        }
        ivAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ivFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                        */
            }
        });

        ivBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), PrincipalActivity.class);

                i.putExtra("pantalla",0);
                getActivity().startActivity(i);
                getActivity().finish();
            }
        });
        ivCompartir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String textoCompartir = titulo + "\n" + area + "\n" + contenido;

                intent.putExtra(Intent.EXTRA_TEXT, textoCompartir);
                startActivity(Intent.createChooser(intent, "Compartir con"));

            }
        });


        return ll;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    void getData(String cod_not){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/noticiaspenales";

        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("cod_noticia","");
            jo.put("cod_not", cod_not);
            jo.put("cod_area", "");
            jo.put("cod_sec", "");
            jo.put("fecha", "");
            jo.put("criterio", "");
            jo.put("resultados_desde", "0");
            jo.put("resultados_maximo", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        System.out.println(response.toString());
                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    BoletinModel boletinModel = new BoletinModel();
                                    boletinModel = gson.fromJson(result.getJSONObject(i).toString(), BoletinModel.class);

                                    titulo = boletinModel.getNot_titulo();
                                    area = boletinModel.getArea();
                                    fuente = boletinModel.getNot_fuente();
                                    contenido = Html.fromHtml(boletinModel.getNot_contenido()).toString();

                                    String url = boletinModel.getNot_imagen();
                                    fecha =   boletinModel.getNot_fecha() + " " + boletinModel.getNot_hora();
                                    tvFecha.setText(""+fecha);


                                    Picasso.with(getActivity()).load(url).into(ivImagen);

                                    tvTitulo.setText(titulo);
                                    tvArea.setText(area);
                                    tvFuente.setText("Fuente: "+fuente);
                                    jtvContenido.setText(contenido);
                                }


                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.no_data)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d("PUSH",error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);
    }
}
