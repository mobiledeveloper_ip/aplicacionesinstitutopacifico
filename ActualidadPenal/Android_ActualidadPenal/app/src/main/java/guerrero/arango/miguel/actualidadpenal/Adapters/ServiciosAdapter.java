package guerrero.arango.miguel.actualidadpenal.Adapters;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadpenal.Activities.LoginConfirmacion;
import guerrero.arango.miguel.actualidadpenal.Activities.PrincipalActivity;
import guerrero.arango.miguel.actualidadpenal.Fragments.SoftwareBusquedaDetalle;
import guerrero.arango.miguel.actualidadpenal.Models.BusquedaSoftwareModel;
import guerrero.arango.miguel.actualidadpenal.Models.Servicio;
import guerrero.arango.miguel.actualidadpenal.R;

/**
 * Created by Miguel on 23/05/2016.
 */
public class ServiciosAdapter extends RecyclerView.Adapter<ServiciosAdapter.ViewHolder> {

    ArrayList<Servicio> servicios;
    LoginConfirmacion loginConfirmacion;


    public ServiciosAdapter(ArrayList<Servicio> servicios, LoginConfirmacion loginConfirmacion) {
        this.servicios = servicios;
        this.loginConfirmacion = loginConfirmacion;

    }

    @Override
    public ServiciosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_servicio, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ServiciosAdapter.ViewHolder holder, final int position) {
        holder.ivImagen.setImageDrawable(servicios.get(position).getImagen());
        holder.tvNombre.setText(servicios.get(position).getNombre());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(loginConfirmacion, PrincipalActivity.class);

                i.putExtra("pantalla",position);
                loginConfirmacion.startActivity(i);

                //loginConfirmacion.finish();

            }
        });
    }

    @Override
    public int getItemCount() {
        return servicios.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView cv;

        TextView tvNombre;
        ImageView ivImagen;

        public ViewHolder(View itemView) {
            super(itemView);

            cv = (CardView) itemView.findViewById(R.id.cv);
            tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);

            ivImagen = (ImageView) itemView.findViewById(R.id.ivImagen);

        }
    }
}
