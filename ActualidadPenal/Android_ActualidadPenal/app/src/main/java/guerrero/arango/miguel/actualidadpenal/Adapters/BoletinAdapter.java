package guerrero.arango.miguel.actualidadpenal.Adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadpenal.Fragments.BoletinDetalleFragment;
import guerrero.arango.miguel.actualidadpenal.Fragments.SoftwareBusquedaDetalle;
import guerrero.arango.miguel.actualidadpenal.Models.BoletinModel;
import guerrero.arango.miguel.actualidadpenal.R;
import guerrero.arango.miguel.actualidadpenal.Singletons.VariablesGlobales;

/**
 * Created by Miguel on 23/05/2016.
 */
public class BoletinAdapter extends RecyclerView.Adapter<BoletinAdapter.ViewHolder> {

    ArrayList<BoletinModel> boletines;

    AppCompatActivity activity;
    FragmentManager fManager;

    public BoletinAdapter(ArrayList<BoletinModel> pagos, AppCompatActivity activity) {
        this.boletines = pagos;

        this.activity = activity;

        fManager = this.activity.getSupportFragmentManager();

    }

    @Override
    public BoletinAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_boletin, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final BoletinAdapter.ViewHolder holder, final int position) {

        holder.tvTitulo.setText(boletines.get(position).getNot_titulo());
        holder.tvArea.setText(boletines.get(position).getArea());

        //holder.ivImagen.getLayoutParams().height = holder.ivImagen.getLayoutParams().width;
        holder.ivImagen.setScaleType(ImageView.ScaleType.FIT_CENTER);
        holder.ivImagen.setAdjustViewBounds(true);

        holder.tvFecha.setText(boletines.get(position).getNot_fecha() + " " + boletines.get(position).getNot_hora());


        Picasso.with(VariablesGlobales.getInstance().getMainActivity()).load(boletines.get(position).getThumbnail()).into(holder.ivImagen);

        holder.ivImagen.setScaleType(ImageView.ScaleType.FIT_XY);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BoletinDetalleFragment boletinDetalleFragment = new BoletinDetalleFragment();

                Bundle bundle = new Bundle();
                bundle.putString("result_titulo",boletines.get(position).getNot_titulo());
                bundle.putString("result_area",boletines.get(position).getArea());
                bundle.putString("result_contenido",boletines.get(position).getNot_contenido());
                bundle.putString("result_url",boletines.get(position).getNot_imagen());
                bundle.putString("fecha",boletines.get(position).getNot_fecha() + " " + boletines.get(position).getNot_hora());
                bundle.putString("fuente_imagen",boletines.get(position).getNot_fuente());
                boletinDetalleFragment.setArguments(bundle);

                fManager.beginTransaction().replace(R.id.contenedor, boletinDetalleFragment).addToBackStack(null).commit();
            }
        });




    }

    @Override
    public int getItemCount() {
        return boletines.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView tvArea,tvTitulo,tvFecha;
        ImageView ivImagen;

        public ViewHolder(View itemView) {
            super(itemView);

            cv = (CardView) itemView.findViewById(R.id.cv);
            tvArea = (TextView) itemView.findViewById(R.id.tvArea);
            tvTitulo = (TextView) itemView.findViewById(R.id.tvTitulo);
            tvFecha = (TextView) itemView.findViewById(R.id.tvFecha);

            ivImagen = (ImageView) itemView.findViewById(R.id.ivImagen);

        }
    }
}
