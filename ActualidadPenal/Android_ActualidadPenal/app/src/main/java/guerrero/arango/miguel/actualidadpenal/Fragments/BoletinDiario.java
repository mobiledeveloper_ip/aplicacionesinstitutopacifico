package guerrero.arango.miguel.actualidadpenal.Fragments;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import guerrero.arango.miguel.actualidadpenal.Activities.LoginConfirmacion;
import guerrero.arango.miguel.actualidadpenal.Activities.PrincipalActivity;
import guerrero.arango.miguel.actualidadpenal.Adapters.BoletinAdapter;
import guerrero.arango.miguel.actualidadpenal.Custom.AuthRequest;
import guerrero.arango.miguel.actualidadpenal.Models.BoletinModel;
import guerrero.arango.miguel.actualidadpenal.R;
import guerrero.arango.miguel.actualidadpenal.Singletons.AlertSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.ProgressSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.VariablesGlobales;
import guerrero.arango.miguel.actualidadpenal.Singletons.VolleySingleton;

/**
 * Created by Miguel on 27/06/2016.
 */
public class BoletinDiario extends Fragment {

    LinearLayout ll,llRv;
    private String TAG = BoletinDiario.this.getClass().getName();

    ArrayList<BoletinModel> listaBoletines = new ArrayList<>();

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    String codigoArea;

    DatePickerDialog.OnDateSetListener date;
    Calendar myCalendar;

    ImageView ivAtras,ivInicio,ivBuscar,ivFecha;

    int tipoBoletin;

    String tipo = "";

    NestedScrollView nscVista;

    Button bCargar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.fragment_boletin_diario,container,false);

        //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Boletín Diario");


        bCargar = (Button) ll.findViewById(R.id.bCargar);
        bCargar.setVisibility(View.GONE);
        bCargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BuscarMas(listaBoletines.size());

            }
        });
        nscVista = (NestedScrollView) ll.findViewById(R.id.nscVista);
        ivAtras = (ImageView) ll.findViewById(R.id.ivAtras);
        ivBuscar = (ImageView) ll.findViewById(R.id.ivBuscar);
        ivInicio = (ImageView) ll.findViewById(R.id.ivInicio);
        ivFecha = (ImageView) ll.findViewById(R.id.ivFecha);
        llRv = (LinearLayout) ll.findViewById(R.id.llRv);


        myCalendar = Calendar.getInstance();

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateFecha();
            }


        };


        // Obtener el Recycler
        recycler = (RecyclerView) ll.findViewById(R.id.rv);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new BoletinAdapter(listaBoletines, (AppCompatActivity) getActivity());

        recycler.setAdapter(adapter);


        ivAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ivFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        ivBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                alert.setTitle("Buscar...");
                //alert.setMessage("Message");


                // Set an EditText view to get user input
                final EditText input = new EditText(getActivity());
                input.setBackgroundResource(R.drawable.edittext_rounded);

                DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
                float dp = 35f;
                float fpixels = metrics.density * dp;
                int pixels = (int) (fpixels + 0.5f);
                int paddingDp = (int) ( (metrics.density * 5f) + 0.5f);

                float dpi = getActivity().getResources().getDisplayMetrics().density;

                input.setHeight(pixels);
                input.setSingleLine(true);
                input.setPadding(paddingDp,paddingDp,paddingDp,paddingDp);

                //input.setPadding(10,0,10,0);
                alert.setView(input, (int)(19*dpi), (int)(5*dpi), (int)(14*dpi), (int)(5*dpi) );

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        BuscarTexto(input.getText().toString());

                        // Do something with value!
                    }
                });

                alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

                alert.show();
                /*
                Intent i = new Intent(getActivity(), PrincipalActivity.class);

                i.putExtra("pantalla",0);
                getActivity().startActivity(i);
                getActivity().finish();*/
            }
        });
        ivInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getActivity().finish();

                Intent intent = new Intent(getActivity(),
                        LoginConfirmacion.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);

            }
        });

        return ll;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        codigoArea =getArguments().getString("id");
        tipoBoletin = getArguments().getInt("tipo");
        Buscar(tipoBoletin);

    }

    private void updateFecha() {

        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        Buscar(tipoBoletin, sdf.format(myCalendar.getTime()));
    }

    void Buscar(int tipo){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        listaBoletines.clear();
        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "";
        if(tipo == 0){
            URL_COMPLEMENTO = "/noticiaslegales";
        }   else if(tipo == 1){
            URL_COMPLEMENTO = "/noticiaspenales";
        }

        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("cod_noticia", "");
            jo.put("cod_area", "");
            jo.put("cod_sec", codigoArea);
            jo.put("fecha", "");
            jo.put("criterio", "");
            jo.put("resultados_desde", "0");
            jo.put("resultados_maximo", "200");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        System.out.println(response.toString());
                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    BoletinModel boletinModel = new BoletinModel();
                                    boletinModel = gson.fromJson(result.getJSONObject(i).toString(), BoletinModel.class);

                                    //Picasso.with(getActivity()).load(boletinModel.getNot_imagen()).fetch();

                                    listaBoletines.add(boletinModel);
                                }

                                if(listaBoletines.size() == 0){
                                    nscVista.setVisibility(View.GONE);
                                    //llRv.setVisibility(View.GONE);
                                }   else{
                                    nscVista.setVisibility(View.VISIBLE);
                                    //llRv.setVisibility(View.VISIBLE);
                                }
                                adapter.notifyDataSetChanged();

                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.Fail_Login)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d(TAG,error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }


    void Buscar(int tipo, String fecha){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        listaBoletines.clear();
        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();


        String URL_COMPLEMENTO = "";
        if(tipo == 0){
            URL_COMPLEMENTO = "/noticiaslegales";
        }   else if(tipo == 1){
            URL_COMPLEMENTO = "/noticiaspenales";
        }
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("cod_noticia", "");
            jo.put("cod_area", "");
            jo.put("cod_sec", "");
            jo.put("fecha", fecha);
            jo.put("criterio", "");
            jo.put("resultados_desde", "0");
            jo.put("resultados_maximo", "200");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        System.out.println(response.toString());
                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    BoletinModel boletinModel = new BoletinModel();
                                    boletinModel = gson.fromJson(result.getJSONObject(i).toString(), BoletinModel.class);

                                    //Picasso.with(getActivity()).load(boletinModel.getNot_imagen()).fetch();

                                    listaBoletines.add(boletinModel);
                                }

                                if(listaBoletines.size() == 0){
                                    nscVista.setVisibility(View.GONE);
                                    //llRv.setVisibility(View.GONE);
                                }   else{
                                    nscVista.setVisibility(View.VISIBLE);
                                    //llRv.setVisibility(View.VISIBLE);
                                }
                                adapter.notifyDataSetChanged();

                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.Fail_Login)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d(TAG,error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }


    void BuscarTexto(String texto){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        listaBoletines.clear();
        bCargar.setVisibility(View.VISIBLE);
        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/noticiaslegales";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("cod_noticia", "");
            jo.put("cod_area", "");
            jo.put("cod_sec", "");
            jo.put("fecha", "");
            jo.put("criterio", texto);
            jo.put("resultados_desde", "0");
            jo.put("resultados_maximo", "200");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());
        VariablesGlobales.getInstance().setJoBoletin(jo);

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        System.out.println(response.toString());
                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    BoletinModel boletinModel = new BoletinModel();
                                    boletinModel = gson.fromJson(result.getJSONObject(i).toString(), BoletinModel.class);

                                    //Picasso.with(getActivity()).load(boletinModel.getNot_imagen()).fetch();

                                    listaBoletines.add(boletinModel);
                                }

                                if(listaBoletines.size() == 0){
                                    nscVista.setVisibility(View.GONE);
                                    //llRv.setVisibility(View.GONE);
                                }   else{
                                    nscVista.setVisibility(View.VISIBLE);
                                    //llRv.setVisibility(View.VISIBLE);
                                }
                                adapter.notifyDataSetChanged();

                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.Fail_Login)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d(TAG,error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }

    void BuscarMas(int desde){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        //listaBoletines.clear();
        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/noticiaslegales";
        // Mapeo de los pares clave-valor

        JSONObject jo = VariablesGlobales.getInstance().getJoBoletin();

        try {
            jo.put("resultados_desde", desde);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        System.out.println(response.toString());
                        JSONArray result = new JSONArray();


                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    BoletinModel boletinModel = new BoletinModel();
                                    boletinModel = gson.fromJson(result.getJSONObject(i).toString(), BoletinModel.class);

                                    //Picasso.with(getActivity()).load(boletinModel.getNot_imagen()).fetch();

                                    listaBoletines.add(boletinModel);
                                }

                                if(result.length() == 0){
                                    bCargar.setVisibility(View.GONE);
                                }

                                adapter.notifyDataSetChanged();

                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.no_data)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d(TAG,error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }




}
