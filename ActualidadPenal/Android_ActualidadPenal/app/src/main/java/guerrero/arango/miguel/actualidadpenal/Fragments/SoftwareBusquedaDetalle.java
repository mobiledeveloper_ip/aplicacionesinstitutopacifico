package guerrero.arango.miguel.actualidadpenal.Fragments;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.NotificationCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import guerrero.arango.miguel.actualidadpenal.Custom.AuthRequest;
import guerrero.arango.miguel.actualidadpenal.Models.BusquedaSoftwareModel;
import guerrero.arango.miguel.actualidadpenal.R;
import guerrero.arango.miguel.actualidadpenal.Singletons.AlertSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.NotificacionSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.ProgressSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.SesionUsuario;
import guerrero.arango.miguel.actualidadpenal.Singletons.VariablesGlobales;
import guerrero.arango.miguel.actualidadpenal.Singletons.VolleySingleton;

/**
 * Created by Miguel on 14/07/2016.
 */
public class SoftwareBusquedaDetalle extends Fragment {

    LinearLayout ll;
    TextView tvTitulo,tvArea,tvContenido;

    private final static String NOMBRE_DOCUMENTO = "prueba.pdf";

    String titulo,area,contenido,idContenido,favorito,pdf;
    boolean isFavorito;
    ImageView ivFavorito,ivAtras,ivDescargar,ivImprimir;

    File filePdf;
    JustifiedTextView jtvContenido;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.fragment_software_busqueda_detalle,container,false);

        ivImprimir = (ImageView) ll.findViewById(R.id.ivImprimir);
        if(Build.VERSION.SDK_INT < 19) {
            ivImprimir.setVisibility(View.GONE);
        }
        ivImprimir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                alert.setTitle("Importante!");
                alert.setMessage("Para imprimir este artículo es necesario que cuente con una impresora Wi-Fi configurada en la misma red que su dispositivo móvil");

                alert.setPositiveButton("Imprimir", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        imprimirPDF();
                        // Do something with value!
                    }
                });

                alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

                alert.show();

            }
        });

        jtvContenido = (JustifiedTextView) ll.findViewById(R.id.jtvContenido);
        //jtvContenido.setAutoLinkMask(Linkify.WEB_URLS);

        ivAtras = (ImageView) ll.findViewById(R.id.ivAtras);
        ivAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ivDescargar = (ImageView) ll.findViewById(R.id.ivDescargar);
        ivDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String url = "http://www.example.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(pdf));
                startActivity(i);
                /*
                SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
                String pdfName = "pdf"
                        + sdf.format(Calendar.getInstance().getTime());
                new DownloadFile().execute(pdf, pdfName+".pdf");*/
                //crearPDF();
                //String TextoDescargar = titulo + "\n" + area + "\n" + contenido;
                //GuardarEnSD(idContenido, TextoDescargar);

            }
        });


        ivFavorito = (ImageView) ll.findViewById(R.id.ivFavorito);
        ivFavorito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFavorito){
                    quitarFavorito();
                }   else{
                    setFavorito();
                }
            }
        });

        tvTitulo = (TextView) ll.findViewById(R.id.tvTitulo);
        tvArea = (TextView) ll.findViewById(R.id.tvArea);
        tvContenido= (TextView) ll.findViewById(R.id.tvContenido);

        //ivImagen.setScaleType(ImageView.ScaleType.FIT_XY);

        if(getArguments().getString("bypass") == "1"){

            String categoria = getArguments().getString("id_contenido");
            getData(categoria);


        }   else{
            titulo =getArguments().getString("result_titulo");
            area =getArguments().getString("result_area");
            idContenido = getArguments().getString("id_contenido");
            favorito = getArguments().getString("favorito");
            pdf = getArguments().getString("pdf");
            contenido = Html.fromHtml(getArguments().getString("result_contenido").toString()).toString();

            if(favorito != null){
                if(favorito.contains("1")){
                    ivFavorito.setImageResource(R.drawable.favoritos);
                    isFavorito = true;
                }   else{
                    ivFavorito.setImageResource(R.drawable.favoritos_unselected);
                    isFavorito = false;
                }
            }   else{
                ivFavorito.setImageResource(R.drawable.favoritos_unselected);
                isFavorito = false;
            }

            tvTitulo.setText(titulo);
            tvArea.setText(area);
            tvContenido.setText(contenido);
        }

        return ll;

    }

    void getData(String categoria){

        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/contenidosoft";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("id_suscripcion", SesionUsuario.getInstance(getActivity()).getIdCliente());
            jo.put("aliasrevista", VariablesGlobales.getInstance().getRevista());
            jo.put("idcategoria", "");
            jo.put("id_contenido", categoria);
            jo.put("busqueda", "");
            jo.put("desde", "");
            jo.put("hasta", "");
            jo.put("emisor", "");
            jo.put("estado", "");
            jo.put("area", "");
            jo.put("resultados_desde", "0");
            jo.put("resultados_maximo", "10");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                Gson gson = new Gson();

                                //listaBusqueda.clear();

                                for (int i = 0 ; i < result.length();i++){
                                    BusquedaSoftwareModel busquedaSoftwareModel = new BusquedaSoftwareModel();
                                    busquedaSoftwareModel = gson.fromJson(result.getJSONObject(i).toString(), BusquedaSoftwareModel.class);

                                    titulo = busquedaSoftwareModel.getTitulo();
                                    area = busquedaSoftwareModel.getNombrecat();
                                    idContenido = busquedaSoftwareModel.getIdsiip_soft_contenido();
                                    favorito = busquedaSoftwareModel.getEnfavoritos();
                                    pdf = busquedaSoftwareModel.getPdf();
                                    contenido = Html.fromHtml(busquedaSoftwareModel.getHtml()).toString();

                                    if(favorito != null){
                                        if(favorito.contains("1")){
                                            ivFavorito.setImageResource(R.drawable.favoritos);
                                            isFavorito = true;
                                        }   else{
                                            ivFavorito.setImageResource(R.drawable.favoritos_unselected);
                                            isFavorito = false;
                                        }
                                    }   else{
                                        ivFavorito.setImageResource(R.drawable.favoritos_unselected);
                                        isFavorito = false;
                                    }

                                    tvTitulo.setText(titulo);
                                    tvArea.setText(area);
                                    tvContenido.setText(contenido);
                                }




                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.no_data)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d("PUSH",error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    void mandarImpresion(){

        PrintDocumentAdapter pda = new PrintDocumentAdapter(){

            @Override
            public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback){
                InputStream input = null;
                OutputStream output = null;

                try {

                    input = new FileInputStream(filePdf.getAbsolutePath());
                    System.out.println("Path: "+filePdf.getAbsolutePath());
                    System.out.println("Path: "+filePdf.toURI().toString());
                    System.out.println("Path: "+Uri.fromFile(filePdf));
                    output = new FileOutputStream(destination.getFileDescriptor());

                    byte[] buf = new byte[1024];
                    int bytesRead;

                    while ((bytesRead = input.read(buf)) > 0) {
                        output.write(buf, 0, bytesRead);
                    }

                    callback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});

                } catch (FileNotFoundException ee){
                    //Catch exception
                } catch (Exception e) {
                    //Catch exception
                } finally {
                    try {
                        input.close();
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras){

                if (cancellationSignal.isCanceled()) {
                    callback.onLayoutCancelled();
                    return;
                }

                //int pages = computePageCount(newAttributes);

                PrintDocumentInfo pdi = new PrintDocumentInfo.Builder("Name of file").setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT).build();

                callback.onLayoutFinished(pdi, true);
            }
        };

        PrintManager printManager = (PrintManager) getActivity().getSystemService(Context.PRINT_SERVICE);
        String jobName = this.getString(R.string.app_name) + " Document";
        printManager.print(jobName, pda, null);
    }

    void imprimirPDF(){

        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();

        File folder = new File(extStorageDirectory, "pdf");

        if(!folder.exists()){
            folder.mkdir();
        }

        filePdf = new File(folder, "Imprimir.pdf");

        try {
            filePdf.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        //Descargador.DownloadFile("http://actualidadpenal.com.pe/software/output_movil.php?t=pdf&id=9921", file);

        Object[] descargaParams = { pdf, filePdf};
        new DescargarArchivo().execute(descargaParams);

    }

    private class DescargarArchivo extends AsyncTask<Object,Void,Void>{

        @Override
        protected Void doInBackground(Object... params) {
            try {
                FileOutputStream f = new FileOutputStream((File)params[1]);
                URL u = new URL((String)params[0]);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ProgressSingleton.getInstance(getActivity()).pDialog.show();
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

            mandarImpresion();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }


    private void setFavorito(){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/setcontenidosoftfavoritos";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("id_suscripcion", SesionUsuario.getInstance(getActivity()).getIdCliente());
            jo.put("id_softcontenido", idContenido);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        try {
                            if(response.getInt("estado") == 1){

                                Toast.makeText(getActivity(),"Se guardo la informacion",Toast.LENGTH_SHORT).show();
                                ivFavorito.setImageResource(R.drawable.favoritos);
                                isFavorito = true;

                                if(VariablesGlobales.getInstance().getBusquedaSoftware() != null){
                                    VariablesGlobales.getInstance().getBusquedaSoftware().setEnfavoritos("1");
                                }
                            }   else{
                                Toast.makeText(getActivity(),"No se guardo la informacion",Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();

                        Crashlytics.log(error.toString());

                        Log.d("BUSQEUDA",error.toString());
                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);
    }

    private void quitarFavorito(){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/dropcontenidosoftfavoritos";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("id_suscripcion", SesionUsuario.getInstance(getActivity()).getIdCliente());
            jo.put("id_softcontenido", idContenido);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        try {
                            if(response.getInt("estado") == 1){

                                Toast.makeText(getActivity(),"Se elimino la información",Toast.LENGTH_SHORT).show();
                                ivFavorito.setImageResource(R.drawable.favoritos_unselected);
                                isFavorito = false;


                                if(VariablesGlobales.getInstance().getBusquedaSoftware() != null){
                                    VariablesGlobales.getInstance().getBusquedaSoftware().setEnfavoritos("0");
                                }

                            }   else{
                                Toast.makeText(getActivity(),"No se pudo eliminar la informacion",Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();

                        Crashlytics.log(error.toString());

                        Log.d("BUSQEUDA",error.toString());
                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);
    }



    public void GuardarEnSD(String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), getString(R.string.app_name));
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            Toast.makeText(getActivity(), "Guardado en la carpeta: "+getString(R.string.app_name), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    void crearPDF(){
        // Create a object of PdfDocument
        PdfDocument document = new PdfDocument();

// content view is EditText for my case in which user enters pdf content
        View content = ll.getRootView();

// crate a page info with attributes as below
// page number, height and width
// i have used height and width to that of pdf content view
        int pageNumber = 1;
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(content.getWidth(),
                content.getHeight(), pageNumber).create();

// create a new page from the PageInfo
        PdfDocument.Page page = document.startPage(pageInfo);

// repaint the user's text into the page
        content.draw(page.getCanvas());

// do final processing of the page
        document.finishPage(page);

// saving pdf document to sdcard
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
        String pdfName = "pdf"
                + sdf.format(Calendar.getInstance().getTime()) + ".pdf";

// all created files will be saved at path /sdcard/PDFDemo_AndroidSRC/

        try {
            File root = new File(Environment.getExternalStorageDirectory(), getString(R.string.app_name));
            if (!root.exists()) {
                root.mkdirs();
            }
            File outputFile = new File(root, pdfName);
            outputFile.createNewFile();
            OutputStream out = new FileOutputStream(outputFile);
            document.writeTo(out);
            document.close();
            out.close();
            Toast.makeText(getActivity(), "Guardado en la carpeta: "+getString(R.string.app_name), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private class DownloadFile extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ProgressSingleton.getInstance(getActivity()).pDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, getString(R.string.app_name));
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
                return false;
            }
            FileDownloader.downloadFile(fileUrl, pdfFile, getActivity());
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(aBoolean == false){
                ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
            }
        }
    }


    private static class FileDownloader {
        private static final int  MEGABYTE = 1024 * 1024;

        private static void downloadFile(String fileUrl, File directory, Context context){
            try {

                URL url = new URL(fileUrl);
                HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
                //urlConnection.setRequestMethod("GET");
                //urlConnection.setDoOutput(true);
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(directory);
                int totalSize = urlConnection.getContentLength();

                byte[] buffer = new byte[MEGABYTE];
                int bufferLength = 0;
                while((bufferLength = inputStream.read(buffer))>0 ){
                    fileOutputStream.write(buffer, 0, bufferLength);
                }
                fileOutputStream.close();
                //CrearNotificacion();

                NotificacionSingleton.getInstance(context);

                //File pdfFile = new File(Environment.getExternalStorageDirectory() + "/testthreepdf/" + "maven.pdf");  // -> filename = maven.pdf
                //Uri path = Uri.fromFile(directory);

                //VerPdf(path);
                ProgressSingleton.getInstance(context).pDialog.cancel();

            } catch (FileNotFoundException e) {
                ProgressSingleton.getInstance(context).pDialog.cancel();
                e.printStackTrace();
            } catch (MalformedURLException e) {
                ProgressSingleton.getInstance(context).pDialog.cancel();
                e.printStackTrace();
            } catch (IOException e) {
                ProgressSingleton.getInstance(context).pDialog.cancel();
                e.printStackTrace();
            }
        }
    }









}
