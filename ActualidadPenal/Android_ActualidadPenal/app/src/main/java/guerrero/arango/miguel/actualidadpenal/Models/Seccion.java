package guerrero.arango.miguel.actualidadpenal.Models;

/**
 * Created by Miguel on 05/08/2016.
 */
public class Seccion {

    String id;
    String nombre;


    public Seccion(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Seccion() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
