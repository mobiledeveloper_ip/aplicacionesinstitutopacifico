package guerrero.arango.miguel.actualidadpenal.Models;

/**
 * Created by Miguel on 08/08/2016.
 */
public class Emisor {

    String organoemisor;

    public Emisor(String organoemisor) {
        this.organoemisor = organoemisor;
    }

    public Emisor() {
    }

    public String getOrganoemisor() {
        return organoemisor;
    }

    public void setOrganoemisor(String organoemisor) {
        this.organoemisor = organoemisor;
    }

    public String toString() {
        return (this.getOrganoemisor());
    }
}
