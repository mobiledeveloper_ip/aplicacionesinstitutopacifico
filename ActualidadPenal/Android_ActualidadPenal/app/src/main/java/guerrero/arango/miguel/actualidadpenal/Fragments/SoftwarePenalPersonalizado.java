package guerrero.arango.miguel.actualidadpenal.Fragments;


import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import guerrero.arango.miguel.actualidadpenal.Activities.LoginConfirmacion;
import guerrero.arango.miguel.actualidadpenal.Custom.AuthRequest;
import guerrero.arango.miguel.actualidadpenal.Models.Area;
import guerrero.arango.miguel.actualidadpenal.Models.Carpeta;
import guerrero.arango.miguel.actualidadpenal.Models.Emisor;
import guerrero.arango.miguel.actualidadpenal.Models.Estado;
import guerrero.arango.miguel.actualidadpenal.R;
import guerrero.arango.miguel.actualidadpenal.Singletons.AlertSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.ProgressSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.SesionUsuario;
import guerrero.arango.miguel.actualidadpenal.Singletons.VariablesGlobales;
import guerrero.arango.miguel.actualidadpenal.Singletons.VolleySingleton;

/**
 * Created by Miguel on 27/06/2016.
 */
public class SoftwarePenalPersonalizado extends Fragment {

    private String TAG = SoftwarePenalPersonalizado.this.getClass().getName();

    LinearLayout ll;
    EditText etBusqueda;
    Button bSubmit;


    Spinner sEmisor,sEstado,sArea;
    EditText etDesde,etHasta;
    DatePickerDialog.OnDateSetListener date;
    Calendar myCalendar;
    EditText edittextSelected = null;

    ImageView ivCarpetas,ivAtras,ivSiguiente,ivInicio;

    String emisor,estado,area,categoria = "";

    ArrayAdapter<Emisor> sa_Emisores;
    ArrayAdapter<Estado> sa_Estados;
    ArrayAdapter<Area> sa_Areas;


    ArrayList<Emisor> emisores = new ArrayList<>();
    ArrayList<Estado> estados = new ArrayList<>();
    ArrayList<Area> areas = new ArrayList<>();



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.fragment_software,container,false);

        ivCarpetas = (ImageView) ll.findViewById(R.id.ivCarpetas);
        ivAtras = (ImageView) ll.findViewById(R.id.ivAtras);
        ivSiguiente = (ImageView) ll.findViewById(R.id.ivSiguiente);
        ivInicio = (ImageView) ll.findViewById(R.id.ivInicio);

        sEmisor = (Spinner) ll.findViewById(R.id.sEmisor);
        sEstado = (Spinner) ll.findViewById(R.id.sEstado);
        sArea = (Spinner) ll.findViewById(R.id.sArea);
        etDesde = (EditText) ll.findViewById(R.id.etDesde);
        etDesde.setFocusable(false);
        etHasta= (EditText) ll.findViewById(R.id.etHasta);
        etHasta.setFocusable(false);

        etBusqueda = (EditText) ll.findViewById(R.id.etBusqueda);

        bSubmit = (Button) ll.findViewById(R.id.bSubmit);
        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentarBuscar();
            }
        });




        myCalendar = Calendar.getInstance();

         date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(edittextSelected);
            }


        };

        etDesde.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                edittextSelected = etDesde;
            }
        });


        etHasta.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                edittextSelected = etHasta;
            }
        });

        ivAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ivSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //crearPDF();
            }
        });

        ivCarpetas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, new SoftwarePenalCarpetas()).addToBackStack(null).commit();
            }
        });

        ivInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getActivity().finish();
                Intent intent = new Intent(getActivity(),
                        LoginConfirmacion.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
            }
        });

        //Creamos el adaptador
        sa_Emisores = new ArrayAdapter<Emisor>(getActivity(), android.R.layout.simple_spinner_item, emisores);
        //Añadimos el layout para el menú y se lo damos al spinner
        sa_Emisores.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sEmisor.setAdapter(sa_Emisores);
        sEmisor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position > 0){
                    Emisor obj = (Emisor) parent.getSelectedItem();
                    emisor = obj.getOrganoemisor();

                }   else{
                    emisor = "";
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Creamos el adaptador
        sa_Estados = new ArrayAdapter<Estado>(getActivity(), android.R.layout.simple_spinner_item, estados);
        //Añadimos el layout para el menú y se lo damos al spinner
        sa_Estados.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sEstado.setAdapter(sa_Estados);
        sEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position > 0){
                    Estado obj = (Estado) parent.getSelectedItem();
                    estado = obj.getEstado();

                }   else{
                    estado = "";
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Creamos el adaptador
        sa_Areas = new ArrayAdapter<Area>(getActivity(), android.R.layout.simple_spinner_item, areas);
        //Añadimos el layout para el menú y se lo damos al spinner
        sa_Areas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sArea.setAdapter(sa_Areas);
        sArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(areas.size() > 0 ) {
                    Area obj = (Area) parent.getSelectedItem();
                    if(obj != null){
                        area = obj.getNombrecat();
                        categoria = obj.getIdsiip_soft_categoria();
                    }
                }



                /*
                if(position > 0){


                }   else{
                    area = "";
                }*/


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





        //crearPDF();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        return ll;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getEmisores();
        getAreas();
        getEstados();
    }

    void getEstados(){
        estados.clear();

        Estado estado0 = new Estado("Estado");
        Estado estado1 = new Estado("VIGENTE");
        Estado estado2 = new Estado("DEROGADO");
        Estado estado3 = new Estado("PROMULGADO");

        estados.add(estado0);
        estados.add(estado1);
        estados.add(estado2);
        estados.add(estado3);

        //sa_Estados.notifyDataSetChanged();
    }



    private void updateLabel(EditText etSelected) {

        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etSelected.setText(sdf.format(myCalendar.getTime()));
    }

    void IntentarBuscar(){
        LimpiarErrores();

        String busqueda = etBusqueda.getText().toString();
        String desde = etDesde.getText().toString();
        String hasta = etHasta.getText().toString();


        boolean cancelar = false;
        View focusView = null;

        if (busqueda.isEmpty()){
            etBusqueda.setError(getString(R.string.EmptyField));
            focusView = etBusqueda;
            cancelar = true;
        }

        if(cancelar){
            focusView.requestFocus();
        }   else{
            Buscar(busqueda, desde, hasta);
        }
    }
    void LimpiarErrores(){
        etBusqueda.setError(null);
    }

    void Buscar(final String busqueda, final String desde, final String hasta){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/contenidosoft";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("id_suscripcion", SesionUsuario.getInstance(getActivity()).getIdCliente());
            jo.put("aliasrevista", VariablesGlobales.getInstance().getRevista());
            jo.put("idcategoria", categoria);
            jo.put("busqueda", busqueda);
            jo.put("desde", desde);
            jo.put("hasta", hasta);
            jo.put("emisor", emisor);
            jo.put("estado", estado);
            jo.put("area", area);
            jo.put("resultados_desde", "0");
            jo.put("resultados_maximo", "10");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                int filas = response.getInt("filasencontradas");


                                SoftwarePenalBusquedas softwarePenalResult = new SoftwarePenalBusquedas();
                                Bundle bundle = new Bundle();
                                String dataResult = result.toString();
                                bundle.putInt("filas",filas);
                                bundle.putString("data",dataResult);
                                bundle.putString("idcategoria",categoria);
                                bundle.putString("busqueda",busqueda);
                                bundle.putString("desde",desde);
                                bundle.putString("hasta",hasta);
                                bundle.putString("estado",estado);
                                bundle.putString("emisor",emisor);
                                bundle.putString("area",area);
                                softwarePenalResult.setArguments(bundle);


                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, softwarePenalResult ).addToBackStack("null").commit();



                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.Fail_Login)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d(TAG,error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }

    void getEmisores(){

        emisores.clear();

        emisores.add(new Emisor("Órgano emisor"));
        //sa_Emisores.notifyDataSetChanged();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/organoemisorsoft";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("aliasrevista", VariablesGlobales.getInstance().getRevista());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    Emisor emisor = new Emisor();
                                    emisor = gson.fromJson(result.getJSONObject(i).toString(), Emisor.class);
                                    //Picasso.with(getActivity()).load(boletinModel.getNot_imagen()).fetch();
                                    emisores.add(emisor);
                                }
                                sa_Emisores.notifyDataSetChanged();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d(TAG,error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }

    void getAreas(){

        areas.clear();

       // areas.add(new Area("Area"));
        //sa_Areas.notifyDataSetChanged();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/categoriasoftpadres";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("Revista", VariablesGlobales.getInstance().getRevista());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {




                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");


                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    Area area = new Area();
                                    area = gson.fromJson(result.getJSONObject(i).toString(), Area.class);

                                    //Picasso.with(getActivity()).load(boletinModel.getNot_imagen()).fetch();

                                    areas.add(area);
                                }
                                sa_Areas.notifyDataSetChanged();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        Log.d(TAG,error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);


    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    void crearPDF(){
        // Create a object of PdfDocument
        PdfDocument document = new PdfDocument();

// content view is EditText for my case in which user enters pdf content
        View content = ll.getRootView();

// crate a page info with attributes as below
// page number, height and width
// i have used height and width to that of pdf content view
        int pageNumber = 1;
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(content.getWidth(),
                content.getHeight(), pageNumber).create();

// create a new page from the PageInfo
        PdfDocument.Page page = document.startPage(pageInfo);

// repaint the user's text into the page
        content.draw(page.getCanvas());

// do final processing of the page
        document.finishPage(page);

// saving pdf document to sdcard
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
        String pdfName = "pdfdemo"
                + sdf.format(Calendar.getInstance().getTime()) + ".pdf";

// all created files will be saved at path /sdcard/PDFDemo_AndroidSRC/

        try {
            File root = new File(Environment.getExternalStorageDirectory(), getString(R.string.app_name));
            if (!root.exists()) {
                root.mkdirs();
            }
            File outputFile = new File(root, pdfName);
            outputFile.createNewFile();
            OutputStream out = new FileOutputStream(outputFile);
            document.writeTo(out);
            document.close();
            out.close();
            Toast.makeText(getActivity(), "Guardado en la carpeta: Actualidad Penal", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
