package guerrero.arango.miguel.actualidadpenal.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadpenal.Activities.LoginConfirmacion;
import guerrero.arango.miguel.actualidadpenal.Activities.PrincipalActivity;
import guerrero.arango.miguel.actualidadpenal.Adapters.BoletinAdapter;
import guerrero.arango.miguel.actualidadpenal.Custom.AuthRequest;
import guerrero.arango.miguel.actualidadpenal.Models.BoletinModel;
import guerrero.arango.miguel.actualidadpenal.R;
import guerrero.arango.miguel.actualidadpenal.Singletons.AlertSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.ProgressSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.VariablesGlobales;
import guerrero.arango.miguel.actualidadpenal.Singletons.VolleySingleton;

/**
 * Created by Miguel on 27/06/2016.
 */
public class BoletinSemanal extends Fragment {

    LinearLayout ll;
    private String TAG = BoletinSemanal.this.getClass().getName();

    ArrayList<BoletinModel> listaBoletines = new ArrayList<>();

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    ImageView ivAtras,ivInicio,ivBuscar,ivFecha;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.fragment_boletin_diario,container,false);

        //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Boletín Semanal");


        ivAtras = (ImageView) ll.findViewById(R.id.ivAtras);
        ivBuscar = (ImageView) ll.findViewById(R.id.ivBuscar);
        ivInicio = (ImageView) ll.findViewById(R.id.ivInicio);
        ivFecha = (ImageView) ll.findViewById(R.id.ivFecha);

        // Obtener el Recycler
        recycler = (RecyclerView) ll.findViewById(R.id.rv);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new BoletinAdapter(listaBoletines, (AppCompatActivity) getActivity());

        recycler.setAdapter(adapter);


        ivAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ivFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ivBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), PrincipalActivity.class);

                i.putExtra("pantalla",0);
                getActivity().startActivity(i);
                getActivity().finish();
            }
        });
        ivInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getActivity().finish();
                Intent intent = new Intent(getActivity(),
                        LoginConfirmacion.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);

            }
        });


        return ll;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Buscar();


    }

    void Buscar(){
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        listaBoletines.clear();
        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/noticiaspenales";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("cod_noticia", "");
            jo.put("cod_area", "");
            jo.put("cod_sec", "");
            jo.put("fecha", "");
            jo.put("criterio", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        JSONArray result = new JSONArray();

                        try {

                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");

                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    BoletinModel boletinModel = new BoletinModel();
                                    boletinModel = gson.fromJson(result.getJSONObject(i).toString(), BoletinModel.class);

                                    //Picasso.with(getActivity()).load(boletinModel.getNot_imagen()).fetch();

                                    listaBoletines.add(boletinModel);
                                }
                                adapter.notifyDataSetChanged();

                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.Fail_Login)).dialog.show();


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();


                        Log.d(TAG,error.toString());

                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);

    }
}
