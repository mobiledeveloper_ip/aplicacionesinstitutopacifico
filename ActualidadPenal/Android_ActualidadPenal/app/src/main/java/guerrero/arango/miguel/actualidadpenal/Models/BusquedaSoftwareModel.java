package guerrero.arango.miguel.actualidadpenal.Models;

/**
 * Created by Miguel on 04/07/2016.
 */
public class BusquedaSoftwareModel {

    String titulo;
    String nombrecat;
    String text;
    String enfavoritos;

    String idsiip_soft_contenido;
    String pdf;
    String html;

    public BusquedaSoftwareModel() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getNombrecat() {
        return nombrecat;
    }

    public void setNombrecat(String nombrecat) {
        this.nombrecat = nombrecat;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIdsiip_soft_contenido() {
        return idsiip_soft_contenido;
    }

    public void setIdsiip_soft_contenido(String idsiip_soft_contenido) {
        this.idsiip_soft_contenido = idsiip_soft_contenido;
    }

    public String getEnfavoritos() {
        return enfavoritos;
    }

    public void setEnfavoritos(String enfavoritos) {
        this.enfavoritos = enfavoritos;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}
