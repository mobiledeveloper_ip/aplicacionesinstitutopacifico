package guerrero.arango.miguel.actualidadpenal.Activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Bundle;
import android.print.pdf.PrintedPdfDocument;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import guerrero.arango.miguel.actualidadpenal.Fragments.BoletinDetalleFragment;
import guerrero.arango.miguel.actualidadpenal.Fragments.BoletinSeccionesDiario;
import guerrero.arango.miguel.actualidadpenal.Fragments.BoletinSeccionesSemanal;
import guerrero.arango.miguel.actualidadpenal.Fragments.BoletinSemanal;
import guerrero.arango.miguel.actualidadpenal.Fragments.Favoritos;
import guerrero.arango.miguel.actualidadpenal.Fragments.SoftwareBusquedaDetalle;
import guerrero.arango.miguel.actualidadpenal.Fragments.SoftwarePenalPersonalizado;
import guerrero.arango.miguel.actualidadpenal.R;
import guerrero.arango.miguel.actualidadpenal.Singletons.NotificacionSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.SesionUsuario;
import guerrero.arango.miguel.actualidadpenal.Singletons.VariablesGlobales;

public class PrincipalActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    View headerLayout;
    TextView tvNombre;

    FragmentManager fragmentManager;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(getString(R.string.app_name));

        VariablesGlobales.getInstance().setMainActivity(this);
        VariablesGlobales.getInstance().setFragmentManager(this.getFragmentManager());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.setItemIconTintList(null);

        headerLayout = navigationView.getHeaderView(0);

        //fragmentManager = getSupportFragmentManager();
        fragmentManager = getSupportFragmentManager();

        tvNombre = (TextView) headerLayout.findViewById(R.id.tvNombre);
        tvNombre.setText(SesionUsuario.getInstance(PrincipalActivity.this).getNombre());
        //tvNombre.setText("Miguel Arango Guerrero");

        //drawer.openDrawer(GravityCompat.START);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int value = extras.getInt("pantalla");
            //The key argument here must match that used in the other activity

            switch (value){
                case 0:
                    onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_software));
                    break;
                case 1:
                    onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_boletin_diario));
                    break;
                case 2:
                    onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_boletin_semanal));
                    break;
                case 3:
                    SoftwareBusquedaDetalle softwareBusquedaDetalle = new SoftwareBusquedaDetalle();
                    Bundle bundle = new Bundle();
                    bundle.putString("id_contenido",extras.getString("id_contenido"));
                    bundle.putString("bypass","1");
                    VariablesGlobales.getInstance().setBusquedaSoftware(null);
                    softwareBusquedaDetalle.setArguments(bundle);
                    fragmentManager.beginTransaction().replace(R.id.contenedor, softwareBusquedaDetalle).commit();

                    break;
                case 4:

                    BoletinDetalleFragment boletinDetalleFragment = new BoletinDetalleFragment();
                    Bundle bundleSoft = new Bundle();
                    bundleSoft.putString("cod_not", extras.getString("cod_not"));
                    bundleSoft.putString("bypass","1");
                    boletinDetalleFragment.setArguments(bundleSoft);
                    fragmentManager.beginTransaction().replace(R.id.contenedor, boletinDetalleFragment).commit();

                    break;
            }
        }

        //onNavigationItemSelected(navigationView.getMenu().getItem(1));
        //onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_software));
    }

    @Override
    public void onBackPressed() {
        /*
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (fragmentManager.getBackStackEntryCount() > 0) {
                fragmentManager.popBackStack();
            }   else{
                if(SesionUsuario.getInstance(PrincipalActivity.this).estaLogeado() && isTaskRoot()){
                    Intent intent = new Intent(getApplicationContext(), LoginConfirmacion.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }   else if(!(SesionUsuario.getInstance(PrincipalActivity.this).estaLogeado()) && isTaskRoot()){
                    Intent intent = new Intent(getApplicationContext(), Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }   else{
                    super.onBackPressed();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.principal, menu);
        //toolbar.inflateMenu(R.menu.principal);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_software) {
            // Handle the camera action

            fragmentManager.popBackStack(null, fragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.contenedor, new SoftwarePenalPersonalizado()).commit();

            getSupportActionBar().setTitle("Software Penal");


        } else if (id == R.id.nav_boletin_diario) {
            fragmentManager.popBackStack(null, fragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.contenedor, new BoletinSeccionesDiario()).commit();

            getSupportActionBar().setTitle("Boletín Diario");

        } else if (id == R.id.nav_boletin_semanal) {
            fragmentManager.popBackStack(null, fragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.contenedor, new BoletinSeccionesSemanal()).commit();

            getSupportActionBar().setTitle("Boletín Semanal");
        } else if (id == R.id.nav_favoritos) {
            fragmentManager.popBackStack(null, fragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.contenedor, new Favoritos()).commit();

            getSupportActionBar().setTitle("Favoritos");
        } else if (id == R.id.nav_cerrar) {
            SesionUsuario.getInstance(PrincipalActivity.this).setLogeado(false);

            Intent intent = new Intent(getApplicationContext(), Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            //finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*
    @TargetApi(Build.VERSION_CODES.KITKAT)
    void crearPdf(){
        // open a new document
        PrintedPdfDocument document = new PrintedPdfDocument(PrincipalActivity.this,
                printAttributes);

// start a page
        PdfDocument.Page page = document.startPage(0);

// draw something on the page
        View content = getContentView();
        content.draw(page.getCanvas());

// finish the page
        document.finishPage(page);
        . . .
// add more pages
        . . .
// write the document content
        document.writeTo(getOutputStream());

//close the document
        document.close();
    }
    */
}
