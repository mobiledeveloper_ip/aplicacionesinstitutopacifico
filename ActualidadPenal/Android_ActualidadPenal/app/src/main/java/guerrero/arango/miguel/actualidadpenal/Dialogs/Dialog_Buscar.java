package guerrero.arango.miguel.actualidadpenal.Dialogs;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import guerrero.arango.miguel.actualidadpenal.Activities.PrincipalActivity;
import guerrero.arango.miguel.actualidadpenal.Fragments.BoletinDiario;
import guerrero.arango.miguel.actualidadpenal.R;

/**
 * Created by Miguel on 27/06/2016.
 */
public class Dialog_Buscar extends DialogFragment {

    LinearLayout ll;
    Button bSubmit;

    EditText etBuscar;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.dialog_buscar,container,false);


        //getDialog().setTitle("Buscar contenido");
        setCancelable(false);
        etBuscar = (EditText) ll.findViewById(R.id.etBuscar);
        bSubmit = (Button) ll.findViewById(R.id.bSubmit);
        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return ll;
    }


}
