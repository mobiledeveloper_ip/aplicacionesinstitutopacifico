package guerrero.arango.miguel.actualidadpenal.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadpenal.Activities.LoginConfirmacion;
import guerrero.arango.miguel.actualidadpenal.Adapters.BusquedaAdapter;
import guerrero.arango.miguel.actualidadpenal.Custom.AuthRequest;
import guerrero.arango.miguel.actualidadpenal.Models.BusquedaSoftwareModel;
import guerrero.arango.miguel.actualidadpenal.Models.Carpeta;
import guerrero.arango.miguel.actualidadpenal.R;
import guerrero.arango.miguel.actualidadpenal.Singletons.AlertSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.ProgressSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.SesionUsuario;
import guerrero.arango.miguel.actualidadpenal.Singletons.VariablesGlobales;
import guerrero.arango.miguel.actualidadpenal.Singletons.VolleySingleton;

/**
 * Created by Miguel on 10/08/2016.
 */
public class Favoritos extends Fragment {

    LinearLayout ll;

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    ArrayList<BusquedaSoftwareModel> listaBusqueda = new ArrayList<>();

    ImageView ivInicio,ivCarpetas,ivAtras,ivSiguiente;
    TextView tvResultados;
    Button bCargar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFavoritos();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ll = (LinearLayout) inflater.inflate(R.layout.fragment_software_result,container,false);

        ivCarpetas = (ImageView) ll.findViewById(R.id.ivCarpetas);
        ivAtras = (ImageView) ll.findViewById(R.id.ivAtras);
        ivSiguiente = (ImageView) ll.findViewById(R.id.ivSiguiente);
        ivInicio = (ImageView) ll.findViewById(R.id.ivInicio);
        tvResultados = (TextView) ll.findViewById(R.id.tvResultados);
        bCargar = (Button) ll.findViewById(R.id.bCargar);
        bCargar.setVisibility(View.GONE);

        // Obtener el Recycler
        recycler = (RecyclerView) ll.findViewById(R.id.rv);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new BusquedaAdapter(listaBusqueda, (AppCompatActivity) getActivity());
        recycler.setAdapter(adapter);

        ivAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ivSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ivCarpetas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, new SoftwarePenalCarpetas()).addToBackStack(null).commit();
            }
        });

        ivInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getActivity().finish();
                Intent intent = new Intent(getActivity(),
                        LoginConfirmacion.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
            }
        });

        return ll;
    }

    @Override
    public void onResume() {
        super.onResume();


        for(int i = 0 ; i < listaBusqueda.size() ; i++){
            if(!listaBusqueda.get(i).getEnfavoritos().equals("1")){
                listaBusqueda.remove(i);
            }
        }
        adapter.notifyDataSetChanged();
        tvResultados.setText(""+listaBusqueda.size());
    }

    private void getFavoritos() {
        ProgressSingleton.getInstance(getActivity()).pDialog.show();

        listaBusqueda.clear();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/getcontenidosoftfavoritos";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("id_suscripcion", SesionUsuario.getInstance(getActivity()).getIdCliente());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();

                        JSONArray result = new JSONArray();

                        try {
                            if(response.getInt("estado") == 1){
                                result = response.getJSONArray("datos");
                                Gson gson = new Gson();

                                for (int i = 0 ; i < result.length();i++){
                                    BusquedaSoftwareModel busquedaSoftwareModel = new BusquedaSoftwareModel();
                                    busquedaSoftwareModel = gson.fromJson(result.getJSONObject(i).toString(), BusquedaSoftwareModel.class);
                                    //Picasso.with(getActivity()).load(boletinModel.getNot_imagen()).fetch();

                                    busquedaSoftwareModel.setEnfavoritos("1");
                                    listaBusqueda.add(busquedaSoftwareModel);
                                }

                                for (int i = 0 ;  i < listaBusqueda.size();i++){
                                    listaBusqueda.get(i).setEnfavoritos("1");
                                }

                                tvResultados.setText(""+result.length());
                                adapter.notifyDataSetChanged();

                            }   else{
                                AlertSingleton.getInstance(getActivity(),getString(R.string.no_data)).dialog.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(getActivity()).pDialog.cancel();
                        AlertSingleton.getInstance(getActivity(),getString(R.string.error_servicios)).dialog.show();

                        Crashlytics.log(error.toString());

                        Log.d("FAVORITOS GET",error.toString());
                    }
                }
        );

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(authRequest);
    }

}
