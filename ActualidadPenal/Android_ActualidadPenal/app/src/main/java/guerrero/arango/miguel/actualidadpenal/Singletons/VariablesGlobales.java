package guerrero.arango.miguel.actualidadpenal.Singletons;

import android.app.FragmentManager;
import android.widget.ImageView;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import guerrero.arango.miguel.actualidadpenal.Activities.PrincipalActivity;
import guerrero.arango.miguel.actualidadpenal.Models.BoletinModel;
import guerrero.arango.miguel.actualidadpenal.Models.BusquedaSoftwareModel;

/**
 * Created by MiguelDev on 10/23/2015.
 */
public class VariablesGlobales {

    private static VariablesGlobales singleton;
    String url_base;
    String idCliente;
    String usuario;
    String password;
    String codigoDispositivo;
    ImageView ivImagen;
    BusquedaSoftwareModel busquedaSoftware;

    JSONObject joBoletin;
    PrincipalActivity mainActivity;
    BoletinModel boletinSeleccionado;

    FragmentManager fragmentManager;

    String Revista;

    public static VariablesGlobales getInstance() {
        if(singleton == null){
            singleton = new VariablesGlobales();
        }
        return singleton;
    }

    private VariablesGlobales() {
    }

    public JSONObject getJoBoletin() {
        return joBoletin;
    }

    public void setJoBoletin(JSONObject joBoletin) {
        this.joBoletin = joBoletin;
    }

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public PrincipalActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(PrincipalActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public BoletinModel getBoletinSeleccionado() {
        return boletinSeleccionado;
    }

    public void setBoletinSeleccionado(BoletinModel boletinSeleccionado) {
        this.boletinSeleccionado = boletinSeleccionado;
    }

    public ImageView getIvImagen() {
        return ivImagen;
    }

    public void setIvImagen(ImageView ivImagen) {
        this.ivImagen = ivImagen;
    }

    public String getCodigoDispositivo() {
        return codigoDispositivo;
    }

    public void setCodigoDispositivo(String codigoDispositivo) {
        this.codigoDispositivo = codigoDispositivo;
    }

    public String getFechaActual(){
        //DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        System.out.println(dateFormat.format(cal.getTime())); //2014/08/06 16:00:22


        return dateFormat.format(cal.getTime());
    }

    public String getUrl_base() {
        return url_base;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setUrl_base(String url_base) {
        this.url_base = url_base;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public BusquedaSoftwareModel getBusquedaSoftware() {
        if(busquedaSoftware != null){
            return busquedaSoftware;
        }   else{
            return null;
        }
    }

    public void setBusquedaSoftware(BusquedaSoftwareModel busquedaSoftware) {
        this.busquedaSoftware = busquedaSoftware;
    }

    public String getRevista() {
        return Revista;
    }

    public void setRevista(String revista) {
        Revista = revista;
    }
}
