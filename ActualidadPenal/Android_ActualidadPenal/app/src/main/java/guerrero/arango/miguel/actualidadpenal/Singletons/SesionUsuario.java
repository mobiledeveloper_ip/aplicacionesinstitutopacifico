package guerrero.arango.miguel.actualidadpenal.Singletons;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import guerrero.arango.miguel.actualidadpenal.ParseApplication;

/**
 * Created by MiguelDev on 10/23/2015.
 */
public class SesionUsuario {

    private static SesionUsuario singleton;

    private SharedPreferences prefs;

    public static SesionUsuario getInstance(Context context) {
        if(singleton == null){
            singleton = new SesionUsuario(context);
        }
        return singleton;
    }

    private SesionUsuario(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setNombre(String usuario){
        prefs.edit().putString("nombre",usuario).commit();
    }



    public String getNombre(){
        String usuario = prefs.getString("nombre", "");
        return usuario;
    }



    public void setLogeado(boolean logged){
        prefs.edit().putBoolean("logeado", logged).commit();
    }

    public boolean estaLogeado(){
        boolean logged = prefs.getBoolean("logeado", false);
        return logged;
    }


    public void setIdCliente(String idCliente){
        prefs.edit().putString("idCliente", idCliente).commit();
    }

    public String getIdCliente(){
        String idCliente = prefs.getString("idCliente", "");
        return idCliente;
    }









}
