package guerrero.arango.miguel.actualidadpenal.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import guerrero.arango.miguel.actualidadpenal.Custom.AuthRequest;
import guerrero.arango.miguel.actualidadpenal.Dialogs.Dialog_Confirmar;
import guerrero.arango.miguel.actualidadpenal.Models.InicioSesionModel;
import guerrero.arango.miguel.actualidadpenal.Singletons.AlertSingleton;
import guerrero.arango.miguel.actualidadpenal.Singletons.ProgressSingleton;
import guerrero.arango.miguel.actualidadpenal.R;
import guerrero.arango.miguel.actualidadpenal.Singletons.SesionUsuario;
import guerrero.arango.miguel.actualidadpenal.Singletons.VariablesGlobales;
import guerrero.arango.miguel.actualidadpenal.Singletons.VolleySingleton;

public class Login extends AppCompatActivity {

    private String TAG = Login.this.getClass().getName();
    Button bSubmit;
    EditText etCodigo1,etCodigo2,etCodigo3,etCodigo4;
    View focusView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_login);



        etCodigo1 = (EditText) findViewById(R.id.etCodigo1);
        etCodigo2 = (EditText) findViewById(R.id.etCodigo2);
        etCodigo3 = (EditText) findViewById(R.id.etCodigo3);
        etCodigo4 = (EditText) findViewById(R.id.etCodigo4);

        bSubmit = (Button) findViewById(R.id.bSubmit);
        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentarLogear();
                //Dialog_Confirmar dialog_confirmar = new Dialog_Confirmar();
                //dialog_confirmar.show(getFragmentManager(),null);
            }
        });


        etCodigo1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etCodigo1.length() == 4){
                    focusView = etCodigo2;
                    focusView.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etCodigo2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etCodigo2.length() == 4){
                    focusView = etCodigo3;
                    focusView.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etCodigo3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etCodigo3.length() == 2){
                    focusView = etCodigo4;
                    focusView.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etCodigo4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(etCodigo4.length() == 6){
                    focusView = bSubmit;
                    focusView.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        //Verificar si tiene sesion registrada
        if(SesionUsuario.getInstance(Login.this).estaLogeado()){

            Bundle extras = getIntent().getExtras();

            String bypass = "";
            if(getIntent().hasExtra("bypass")){
                bypass = extras.getString("bypass");
            }

            if(bypass.equals("1")){
                Intent intent = new Intent(Login.this,PrincipalActivity.class);
                intent.putExtras(getIntent().getExtras());
                startActivity(intent);
                finish();
            }   else{
                Intent intent = new Intent(Login.this,LoginConfirmacion.class);
                startActivity(intent);
            }
        }

    }

    void IntentarLogear(){
        LimpiarErrores();

        String codigo1 = etCodigo1.getText().toString();
        String codigo2 = etCodigo2.getText().toString();
        String codigo3 = etCodigo3.getText().toString();
        String codigo4 = etCodigo4.getText().toString();

        boolean cancelar = false;
        View focusView = null;


        if (codigo4.length() != 6){
            etCodigo4.setError(getString(R.string.ErrorField));
            focusView = etCodigo4;
            cancelar = true;
        }







        if (codigo4.isEmpty()){
            etCodigo4.setError(getString(R.string.EmptyField));
            focusView = etCodigo4;
            cancelar = true;
        }

        if (codigo3.length() != 2){
            etCodigo3.setError(getString(R.string.ErrorField));
            focusView = etCodigo3;
            cancelar = true;
        }



        if (codigo3.isEmpty()){
            etCodigo3.setError(getString(R.string.EmptyField));
            focusView = etCodigo3;
            cancelar = true;
        }


        if (codigo2.length() != 4){
            etCodigo2.setError(getString(R.string.ErrorField));
            focusView = etCodigo2;
            cancelar = true;
        }

        if (codigo2.isEmpty()){
            etCodigo2.setError(getString(R.string.EmptyField));
            focusView = etCodigo2;
            cancelar = true;
        }


        if (codigo1.length() != 4){
            etCodigo1.setError(getString(R.string.ErrorField));
            focusView = etCodigo1;
            cancelar = true;
        }

        if (codigo1.isEmpty()){
            etCodigo1.setError(getString(R.string.EmptyField));
            focusView = etCodigo1;
            cancelar = true;
        }

        if(cancelar){
            focusView.requestFocus();
        }   else{
            Logear( (codigo1 + codigo2 + codigo3 + codigo4 ));
        }
    }
    void LimpiarErrores(){
        etCodigo1.setError(null);
    }

    void Logear(String codigo){
        ProgressSingleton.getInstance(Login.this).pDialog.show();

        String URL_BASE = VariablesGlobales.getInstance().getUrl_base();
        String URL_COMPLEMENTO = "/suscripcioncliente";
        // Mapeo de los pares clave-valor

        JSONObject jo = new JSONObject();

        try {
            jo.put("Codigo", codigo);
            jo.put("Revista", VariablesGlobales.getInstance().getRevista());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(jo.toString());

        AuthRequest authRequest = new AuthRequest(
                Request.Method.POST,
                URL_BASE + URL_COMPLEMENTO,
                jo,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressSingleton.getInstance(Login.this).pDialog.cancel();

                        JSONArray InicioSesionResult = new JSONArray();
                        try {
                            if(response.getInt("estado") == 1){
                                InicioSesionResult = response.getJSONArray("datos");

                                if(InicioSesionResult.length() > 0){
                                    Gson gson = new Gson();
                                    InicioSesionModel inicioSesionModel = new InicioSesionModel();
                                    inicioSesionModel = gson.fromJson( InicioSesionResult.getJSONObject(0).toString(), InicioSesionModel.class);

                                    SesionUsuario.getInstance(Login.this).setNombre(inicioSesionModel.getNombres_razonsocial() + " " + inicioSesionModel.getApe_paterno());
                                    SesionUsuario.getInstance(Login.this).setIdCliente(inicioSesionModel.getIdsuscripcion());
                                    SesionUsuario.getInstance(Login.this).setLogeado(true);

                                    Bundle extras = getIntent().getExtras();

                                    String bypass = "";
                                    if(getIntent().hasExtra("bypass")){
                                        bypass = extras.getString("bypass");
                                    }

                                    if(bypass.equals("1")){
                                        Intent intent = new Intent(Login.this,PrincipalActivity.class);
                                        intent.putExtras(getIntent().getExtras());
                                        startActivity(intent);
                                        finish();
                                    }   else{
                                        Intent intent = new Intent(Login.this,LoginConfirmacion.class);
                                        startActivity(intent);
                                    }
                                /*
                                Dialog_Confirmar dialog_confirmar = new Dialog_Confirmar();
                                dialog_confirmar.show(getFragmentManager(),null);*/

                                }   else{
                                    AlertSingleton.getInstance(Login.this,getString(R.string.Fail_Login)).dialog.show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressSingleton.getInstance(Login.this).pDialog.cancel();
                        AlertSingleton.getInstance(Login.this,getString(R.string.error_servicios)).dialog.show();

                        Crashlytics.log(error.toString());

                        Log.d(TAG,error.toString());
                    }
             }
        );

        VolleySingleton.getInstance(Login.this).addToRequestQueue(authRequest);

    }
}
