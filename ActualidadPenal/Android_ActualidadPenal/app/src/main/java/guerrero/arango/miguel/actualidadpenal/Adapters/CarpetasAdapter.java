package guerrero.arango.miguel.actualidadpenal.Adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadpenal.Fragments.SoftwarePenalCarpetas;
import guerrero.arango.miguel.actualidadpenal.Models.Carpeta;
import guerrero.arango.miguel.actualidadpenal.R;

/**
 * Created by Miguel on 23/05/2016.
 */
public class CarpetasAdapter extends RecyclerView.Adapter<CarpetasAdapter.ViewHolder> {

    ArrayList<Carpeta> carpetas;

    AppCompatActivity activity;
    FragmentManager fManager;
    SoftwarePenalCarpetas fragment;

    public CarpetasAdapter(ArrayList<Carpeta> carpetas, AppCompatActivity activity, SoftwarePenalCarpetas fragment) {
        this.carpetas = carpetas;

        this.activity = activity;

        this.fragment = fragment;
        fManager = this.activity.getSupportFragmentManager();

    }

    @Override
    public CarpetasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_carpeta, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final CarpetasAdapter.ViewHolder holder, final int position) {

        holder.tvTitulo.setText(carpetas.get(position).getNombrecat());
        holder.tvCarpetas.setText(carpetas.get(position).getNCarpetas());
        holder.tvArchivos.setText(carpetas.get(position).getNArchivos());


        if(carpetas.get(position).getNCarpetas().equals("0")){
            holder.llSeparadorCarpetas.setVisibility(View.GONE);
            holder.llCarpetas.setVisibility(View.GONE);

        }   else{
            holder.llSeparadorCarpetas.setVisibility(View.VISIBLE);
            holder.llCarpetas.setVisibility(View.VISIBLE);

        }

        if(carpetas.get(position).getNArchivos().equals("0")){
            holder.llSeparadorArchivos.setVisibility(View.GONE);
            holder.llArchivos.setVisibility(View.GONE);

        }   else{
            holder.llSeparadorArchivos.setVisibility(View.VISIBLE);
            holder.llArchivos.setVisibility(View.VISIBLE);

        }
        /*
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.Buscar(carpetas.get(position).getIdsiip_soft_categoria());
            }
        });
*/

        holder.llCarpetas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SoftwarePenalCarpetas softwarePenalCarpetas = new SoftwarePenalCarpetas();

                Bundle bundle = new Bundle();
                bundle.putString("id",carpetas.get(position).getIdsiip_soft_categoria());
                softwarePenalCarpetas.setArguments(bundle);
                fManager.beginTransaction().replace(R.id.contenedor,  softwarePenalCarpetas).addToBackStack(null).commit();}
        });

        holder.llArchivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.Buscar(carpetas.get(position).getIdsiip_soft_categoria());

            }
        });
    }

    @Override
    public int getItemCount() {
        return carpetas.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView cv;

        TextView tvTitulo,tvCarpetas,tvArchivos;

        ImageView ivCarpetas,ivArchivos;

        LinearLayout llSeparadorCarpetas,llSeparadorArchivos,llCarpetas,llArchivos;


        public ViewHolder(View itemView) {
            super(itemView);

            cv = (CardView) itemView.findViewById(R.id.cv);
            tvCarpetas = (TextView) itemView.findViewById(R.id.tvCarpetas);
            tvTitulo = (TextView) itemView.findViewById(R.id.tvTitulo);

            tvArchivos = (TextView) itemView.findViewById(R.id.tvArchivos);
            ivCarpetas = (ImageView) itemView.findViewById(R.id.ivCarpetas);
            ivArchivos = (ImageView) itemView.findViewById(R.id.ivArchivos);

            llSeparadorArchivos = (LinearLayout) itemView.findViewById(R.id.llSeparadorArchivos);
            llSeparadorCarpetas = (LinearLayout) itemView.findViewById(R.id.llSeparadorCarpetas);

            llCarpetas = (LinearLayout) itemView.findViewById(R.id.llCarpetas);
            llArchivos = (LinearLayout) itemView.findViewById(R.id.llArchivos);
        }
    }
}
