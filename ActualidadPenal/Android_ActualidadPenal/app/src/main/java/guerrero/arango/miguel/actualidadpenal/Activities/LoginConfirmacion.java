package guerrero.arango.miguel.actualidadpenal.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import guerrero.arango.miguel.actualidadpenal.Adapters.CarpetasAdapter;
import guerrero.arango.miguel.actualidadpenal.Adapters.ServiciosAdapter;
import guerrero.arango.miguel.actualidadpenal.Models.Servicio;
import guerrero.arango.miguel.actualidadpenal.R;

/**
 * Created by Miguel on 05/08/2016.
 */
public class LoginConfirmacion extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;


    ArrayList<Servicio> servicios = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_confirmacion);


        Servicio servicio1 = new Servicio("Software Penal", ContextCompat.getDrawable(this, R.drawable.software)  );
        servicios.add(servicio1);
        Servicio servicio2 = new Servicio("Boletín Diario", ContextCompat.getDrawable(this, R.drawable.boletin1)  );
        servicios.add(servicio2);
        Servicio servicio3 = new Servicio("Boletín Semanal", ContextCompat.getDrawable(this, R.drawable.boletin2)  );
        servicios.add(servicio3);

        // Obtener el Recycler
        recycler = (RecyclerView) findViewById(R.id.rv);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new ServiciosAdapter(servicios,this);
        recycler.setAdapter(adapter);

    }

}
