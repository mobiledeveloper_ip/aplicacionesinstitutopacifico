package guerrero.arango.miguel.actualidadpenal.Models;

/**
 * Created by Miguel on 09/08/2016.
 */
public class Estado {

    String estado;

    public Estado(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String toString() {
        return (this.estado);
    }
}
